# Api Boilerplate Template

## Getting Started
These is a project template which is used in OpenSpend for creating ASP.NET Core API applications.

## Prerequisites
The project in this repository currently requires .NET Core SDK >= 2.2.300 to be installed. 
If you don't have the .NET SDK installed, you can find it from https://dot.net

## Usage
1. Installation/updating the template.

    `dotnet new -i SU.ApiBoilerplate --nuget-source https://nuget.salondev.net/`

2. Create project in current folder.

    `dotnet new apiboilerplate -n [Solution name]`

    E.g. if you are going to create a project with name `SalonUltimate.MySuperProject` just run:
    `dotnet new apiboilerplate -n SalonUltimate.MySuperProject`

3. To uninstall the template run:

    `dotnet new -u SU.ApiBoilerplate`
    
## Contribution

This project is created using new `dotnet/templating` engine. You can find documentation about how to modify this template project in dotnet/templating wiki. 
https://github.com/dotnet/templating/wiki

Changing the solution inside the teplate as easy as:
- just clone this repo, 
- open the solution from template subfolder, 
- change the solution as you wish,
- commit.

A new nuget package will be packed and available in several minutes on our corporate NuGet server. https://nuget.salondev.net

