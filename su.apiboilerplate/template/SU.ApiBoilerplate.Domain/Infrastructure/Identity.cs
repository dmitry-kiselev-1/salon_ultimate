﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SU.ApiBoilerplate.Domain.Infrastructure
{
	public class Identity<TKey> : IIdentity<TKey> where TKey : struct
	{
		public TKey Id { get; set; }
	}
}
