﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SU.ApiBoilerplate.Domain.Infrastructure
{
	public interface IIdentity<TKey> where TKey : struct
	{
		TKey Id { get; set; }
	}
}
