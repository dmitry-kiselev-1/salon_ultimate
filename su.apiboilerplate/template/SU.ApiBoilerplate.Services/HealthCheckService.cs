﻿using System.Composition;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalonUltimate.Foundation.Services.Interfaces;
using SU.ApiBoilerplate.ORM;

namespace SU.ApiBoilerplate.Services
{
	[Export(typeof(IHealthCheckService))]
	public class HealthCheckService : IHealthCheckService
	{
		private readonly ApplicationDbContext _applicationDbContext;

		public HealthCheckService(ApplicationDbContext applicationDbContext)
		{
			_applicationDbContext = applicationDbContext;
		}

		public async Task WarmUpDbContext()
		{
			await Task.Run(() => 0);
		}
	}
}
