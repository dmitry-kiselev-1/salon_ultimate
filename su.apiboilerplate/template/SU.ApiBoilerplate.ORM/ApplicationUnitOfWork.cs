﻿using System;
using System.Collections.Generic;
using System.Text;
using SalonUltimate.Foundation.Storage.UnitOfWork;

namespace SU.ApiBoilerplate.ORM
{
	public class ApplicationUnitOfWork : EfUnitOfWork<ApplicationDbContext>
	{
		/// <summary>
		/// Initializes a new instance of <see cref="ZenithCentralUnitOfWork"/>.
		/// </summary>
		/// <param name="dbContext"></param>
		public ApplicationUnitOfWork(ApplicationDbContext dbContext) : base(dbContext)
		{
		}
	}
}
