using SU.ApiBoilerplate.Tests.Infrastructure;
using System.Threading.Tasks;
using Xunit;

namespace SU.ApiBoilerplate.Tests
{
	public class UnitTest1 : DatabaseTestBase
	{
		[Fact]
		public void Assert_Test_Architecture_Works()
		{
			// This test includes in background:
			// - creation of IServiceProvider
			// - creation of DbContext
			// - Opening an SQLite connection
			Assert.Equal("Microsoft.EntityFrameworkCore.Sqlite", DbContext.Database.ProviderName);
		}

		protected override Task SeedAsync() => Task.CompletedTask;
	}
}
