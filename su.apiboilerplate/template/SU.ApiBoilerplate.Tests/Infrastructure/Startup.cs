﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Threading;
using AutoMapper;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SU.ApiBoilerplate.ORM.DI;
using SU.ApiBoilerplate.Services.DI;
using SU.ApiBoilerplate.Tests.Infrastructure.Fakes;
using SU.ApiBoilerplate.ORM;

namespace SU.ApiBoilerplate.Tests.Infrastructure
{
	public class Startup
	{
		private static Lazy<IConfiguration> _configuration = new Lazy<IConfiguration>(() => new ConfigurationBuilder()
			.AddJsonFile("appsettings.json")
			.AddJsonFile("appsettings.local.json", optional: true)
			.Build(), LazyThreadSafetyMode.ExecutionAndPublication);
		private static IConfiguration Configuration => _configuration.Value;

		public static void ConfigureServices(IServiceCollection services)
		{
			services.TryAddSingleton(Configuration);
			services.AddMemoryCache();
			services.AddAutoMapper(typeof(OrmMappingProfile), typeof(DtoMappingProfile));

			services.AddDbContext<ApplicationDbContext>(TestApplicationDbContextFactory.GetTestOptionsBuilderConfigurator(), ServiceLifetime.Scoped);

			// TODO: put here your services from API Startup as you need

			services.RegisterDependencies(new RepositoryDIConfigurator());
			services.RegisterDependencies(new ServicesDIConfiguration());

			OverrideDependencies(services);
		}


		private static void OverrideDependencies(IServiceCollection services)
		{
			// TODO: here you can unregister services if you need to replace them with fake ones
			// Example:
			// services.RemoveAll<IDateTimeService>();
			// services.AddScoped<IDateTimeService, TestDateTimeService>();
		}
	}
}
