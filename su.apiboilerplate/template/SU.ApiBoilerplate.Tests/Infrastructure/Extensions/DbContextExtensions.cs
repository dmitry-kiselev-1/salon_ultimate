﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SU.ApiBoilerplate.Tests.Infrastructure.Extensions
{
	public static class DbContextExtensions
	{
		public static async Task AddAndSaveAsync<T>(this DbContext dbContext, T entity) where T : class
		{
			await dbContext.Set<T>().AddAsync(entity);
			await dbContext.SaveChangesAsync();
		}

		public static async Task AddRangeAndSaveAsync<T>(this DbContext dbContext, IEnumerable<T> entities) where T : class
		{
			await dbContext.Set<T>().AddRangeAsync(entities);
			await dbContext.SaveChangesAsync();
		}
	}
}
