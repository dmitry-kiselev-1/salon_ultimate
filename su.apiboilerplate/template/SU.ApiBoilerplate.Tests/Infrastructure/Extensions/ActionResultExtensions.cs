﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using SalonUltimate.Foundation.Enums;
using Xunit;
using SalonUltimate.Foundation.Helpers.Paginator;
using SalonUltimate.Foundation.ViewModels;

namespace SU.ApiBoilerplate.Tests.Infrastructure.Extensions
{
	public static class ActionResultExtensions
	{
		public static TEnvelope ExtractResponseEnvelope<TEnvelope>(this ActionResult<TEnvelope> response) where TEnvelope : BaseResponseVM
		{
			Assert.NotNull(response);
			var okObjectResult = Assert.IsType<ObjectResult>(response.Result);
			var typedResponse = Assert.IsType<TEnvelope>(okObjectResult.Value);
			Assert.NotNull(typedResponse);
			return typedResponse;
		}

		public static TResponse ExtractResponse<TResponse>(this ActionResult<ResponseVM<TResponse>> response)
			where TResponse : class
		{
			Assert.NotNull(response);
			var okObjectResult = Assert.IsType<ObjectResult>(response.Result);
			var typedResponse = Assert.IsType<ResponseVM<TResponse>>(okObjectResult.Value);
			Assert.NotNull(typedResponse);
			Assert.Equal(ResponseTypes.Success, typedResponse.Result);
			Assert.NotNull(typedResponse.Response);

			return typedResponse.Response;
		}

		public static void ValidateBaseResponse(this ActionResult<BaseResponseVM> response, ResponseTypes expectedType = ResponseTypes.Success)
		{
			var okObjectResult = Assert.IsType<ObjectResult>(response.Result);
			var typedResponse = Assert.IsType<BaseResponseVM>(okObjectResult.Value);
			Assert.Equal(expectedType, typedResponse.Result);
		}

		public static IList<TResponse> ExtractBaseCollectionResponse<TResponse>(this ActionResult<CollectionResponseVM<TResponse>> response)
		{
			var okObjectResult = Assert.IsType<ObjectResult>(response.Result);
			var typedResponse = Assert.IsType<CollectionResponseVM<TResponse>>(okObjectResult.Value);
			Assert.NotNull(typedResponse.Response);
			Assert.Equal(ResponseTypes.Success, typedResponse.Result);
			return typedResponse.Response.ToList();
		}

		public static IList<TResponse> ExtractPagedCollectionResponse<TResponse>(this ActionResult<PagedCollectionResponseVm<TResponse>> response)
		{
			var okObjectResult = Assert.IsType<ObjectResult>(response.Result);
			var typedResponse = Assert.IsType<PagedCollectionResponseVm<TResponse>>(okObjectResult.Value);
			Assert.NotNull(typedResponse.Response);
			Assert.Equal(ResponseTypes.Success, typedResponse.Result);
			return typedResponse.Response.ToList();
		}
	}
}
