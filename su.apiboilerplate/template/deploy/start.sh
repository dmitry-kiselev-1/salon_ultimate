#!/bin/bash

export SU_DB_CONNECTION_STRING_MYSQL=`echo $SU_DB_CONNECTION_STRING_MYSQL_BASE64 | base64 --decode`

cat ./appsettings.json \
    | jq ".ConnectionStrings.DefaultConnection=\"$SU_DB_CONNECTION_STRING_MYSQL\"" \
    | jq ".ApplicationInsights.InstrumentationKey=\"$SU_APPINSIGHTS_IKEY\"" \
    | jq ".AppInfo.Environment=\"$SU_ENVIRONMENT_NAME\"" \
    > ./appsettings.json.tmp

cp ./appsettings.json.tmp ./appsettings.json
rm ./appsettings.json.tmp
cat ./appsettings.json

# peform database migration and application start:
dotnet SU.ApiBoilerplate.Api.dll --migrate=up && dotnet SU.ApiBoilerplate.Api.dll

COMPlus_PerfMapEnabled=$SU_USE_PROFILER dotnet SalonUltimate.CentralConfig.API.dll
