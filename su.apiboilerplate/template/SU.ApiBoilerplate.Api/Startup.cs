﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using SU.ApiBoilerplate.Middleware;
using SU.ApiBoilerplate.ORM;
using SU.ApiBoilerplate.ORM.DI;
using SU.ApiBoilerplate.Services.DI;
using SalonUltimate.Foundation.DTOs;
using SalonUltimate.Foundation.Extensions.ApplicationBuilder;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.Middleware.Filters.AuthorizationFilters;
using SalonUltimate.Foundation.Middleware.Filters.ExceptionFilters;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.Foundation.Services.Interfaces;
using SU.ApiBoilerplate.Services;

[assembly: ApiController]
namespace SU.ApiBoilerplate
{
	public class Startup
	{
		private readonly ILoggerFactory _loggerFactory;

		private IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
		{
			Configuration = configuration;
			_loggerFactory = loggerFactory;
			ZenithLogger.LoggerFactory = loggerFactory;
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.ConfigureAppInsights();

			services.AddCors();
			services.AddMemoryCache();

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.EnableSensitiveDataLogging();
				options.UsePostgreSql(Configuration.GetConnectionString("DefaultConnection"));

				if (Debugger.IsAttached)
					options.UseLoggerFactory(_loggerFactory);
			});

			services.AddMvcCore()
				.SetCompatibilityVersion(CompatibilityVersion.Latest);
			services.AddVersionedApiExplorer("'v'VVV", true, "VVV");

			services.AddMvc(options =>
			{
				options.Filters.Add<OperationCancelledFilter>();
				options.Filters.Add<EnableRewindAuthorizationFilter>();
				options.AllowEmptyInputInBodyModelBinding = true;
			})
			.AddControllersAsServices();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			services.ConfigureApiVersioning();

			services.AddTransient<IHealthCheckService, HealthCheckService>();

			services.AddAutoMapper();

			services.ConfigureSwaggerGen(new InfoApiVersionDTO
			{
				TitleTemplate = "SU.ApiBoilerplate API {0}",
				VersionTemplate = "{0}",
				Description = "SU.ApiBoilerplate server",
				ContactName = "Some Guy",
				ContactEmail = "some.guy@somewhere.com",
				TermsOfService = "Shareware",
				LicenseName = "MIT",
				LicenseUrl = "https://opensource.org/licenses/MIT",
				DescriptionPartWhenDepricated = " This API version has been deprecated.",
				XmlCommentsFilePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
					$"{PlatformServices.Default.Application.ApplicationName}.xml")
			});

			services.AddSuHttpCacheHeaders(new ExpirationModelOptions { NoStore = true });

			services.RegisterDependencies(new RepositoryDIConfigurator());
			services.RegisterDependencies(new ServicesDIConfiguration());
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env,
			ApiVersionDescriptionProviderWrapper provider, AutoMapper.IConfigurationProvider autoMapper)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSuHttpCacheHeaders(); // this must be the first middleware (the outer one), because it closes response stream.
			app.UseGlobalExceptionHandler();

			app.UseSwaggerWithUI(provider);
			app.UseCors(builder => builder
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				.AllowCredentials());

			app.UseMvc();

			autoMapper.AssertConfigurationIsValid();
		}
	}
}
