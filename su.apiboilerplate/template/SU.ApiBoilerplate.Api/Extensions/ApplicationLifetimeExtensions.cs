﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SU.ApiBoilerplate.ORM;

namespace SU.ApiBoilerplate.Api.Extensions
{
	public static class ApplicationLifetimeExtensions
	{
		/// <summary>
		/// Used for applying migration before application start in deploy/start.sh
		/// </summary>
		/// <param name="appLifetime"></param>
		/// <param name="configuration"></param>
		public static void RegisterMigrateArgumentHandler(this IApplicationLifetime appLifetime, IConfiguration configuration)
		{
			appLifetime.ApplicationStarted.Register(async () =>
			{
				var migrateArg = configuration["migrate"];

				if (migrateArg == "up")
				{
					// Apply Migrations:
					var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
					var connectionString = configuration.GetConnectionString("DefaultConnection");
					builder.UsePostgreSql(connectionString);
					using (var dbContext = new ApplicationDbContext(builder.Options))
					{
						// With this settings Devart driver will create database if not exists (https://forums.devart.com/viewtopic.php?t=34590):
						//var config = Devart.Data.PostgreSql.Entity.Configuration.PgSqlEntityProviderConfig.Instance;
						//config.DatabaseScript.Schema.DeleteDatabaseBehaviour = Devart.Data.PostgreSql.Entity.Configuration.DeleteDatabaseBehaviour.Database;

						await dbContext.Database.MigrateAsync();
					}
					appLifetime.StopApplication();
				}
				else if (migrateArg != null)
				{
					Console.WriteLine("\t\nInvalid value for 'migrate' argument. Valid values: up.\n");
				}
			});
		}
	}
}
