﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using SalonUltimate.Foundation.Controllers;
using SalonUltimate.Foundation.ViewModels;

namespace SU.ApiBoilerplate.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    public class ValuesController : BaseController
    {
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            return Resp(new ResponseVM<string[]>(new string[] { "value1", "value2" }));
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Resp(new ResponseVM<string>("value"));
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
