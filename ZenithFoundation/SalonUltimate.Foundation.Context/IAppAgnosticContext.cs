﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Context
{
	/// <summary>
	/// A marker interface that represents any context which can be implemented in each system (NW's StoreContext, BookedBy's IExecutionContext etc).
	/// Should be used in Foundation in the places the real system context should be passed to.
	/// </summary>
	public interface IAppAgnosticContext
	{
	}
}
