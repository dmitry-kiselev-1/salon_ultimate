﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;


namespace SalonUltimate.Foundation.BasicTypes
{
	public static class MoneyExtensions
	{
		#region Sum extensions
		public static Money Sum(this IEnumerable<Money> source)
		{
			return source.Aggregate((x, y) => x + y);
		}

		public static Money Sum<T>(this IEnumerable<T> source, Func<T, Money> selector)
		{
			return source.Select(selector).Aggregate((x, y) => x + y);
		}
		#endregion
	}

	public struct Money : IComparable
	{
		private static NumberFormatInfo noSymbolFormat;
		static Money()
		{
			noSymbolFormat = (System.Globalization.NumberFormatInfo)(CultureInfo.CurrentCulture.NumberFormat).Clone();
			noSymbolFormat.CurrencySymbol = "";
		}

		private decimal _value;

		public decimal Value
		{
			get { return _value; }
		}

		public enum Currencies
		{
			USD
		}

		private Currencies _currency;
		public Currencies Currency
		{
			get { return _currency; }
		}

		public enum RoundingTypes
		{
			Null = -1,
			Drop = 1,
			Round = 2,
			RoundUp = 3,
			NoRounding = 4
		}

		public Money(decimal value)
		{
			_value = value;
			_currency = Currencies.USD;
		}

		public Money(decimal value, Currencies currency)
		{
			_value = value;
			_currency = currency;
		}

		public static Money Zero
		{
			get
			{
				return new Money(0m);
			}
		}

		public static Money Abs(Money input)
		{
			return (input < Money.Zero) ? -input : input;
		}

		public static Money operator +(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException("All binary Money operations currenlty require both operands to use the same currency.");
			return new Money(m1._value + m2._value);
		}

		public static Money operator -(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException("All binary Money operations currenlty require both operands to use the same currency.");
			return new Money(m1._value - m2._value);
		}

		public static Money operator -(Money m1)
		{
			return new Money(-m1._value);
		}

		public static bool operator ==(Money m1, Money m2)
		{
			return (m1._value == m2._value && m1._currency == m2._currency);
		}

		public static bool operator !=(Money m1, Money m2)
		{
			return (m1._value != m2._value || m1._currency != m2._currency);
		}

		public static bool operator >(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException();
			return m1.Value > m2.Value;
		}

		public static bool operator <(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException();
			return m1.Value < m2.Value;
		}

		public static bool operator >=(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException();
			return m1.Value >= m2.Value;
		}

		public static bool operator <=(Money m1, Money m2)
		{
			if (m1.Currency != m2.Currency)
				throw new NotSupportedException();
			return m1.Value <= m2.Value;
		}

		public bool Equals(Money target)
		{
			return _value == target._value && _currency == target._currency;
		}

		public override bool Equals(object obj)
		{
			if (obj is Money)
				return Equals((Money)obj);
			return false;
		}

		public override int GetHashCode()
		{
			int hash = 17;

			hash = hash * 23 + _value.GetHashCode();
			hash = hash * 23 + _currency.GetHashCode();

			return hash;
		}

		public static Money Min(Money x, Money y)
		{
			return new Money(Math.Min(x.Value, y.Value));
		}

		public static Money Multiply(Money m1, decimal factor, RoundingTypes roundingType)
		{
			return new Money(RoundByRules(m1._value * factor, roundingType));
		}

		public static Money[] SplitEvenly(Money originalAmount, int numSplits)
		{
			if (numSplits == 1)
			{
				return new Money[] { originalAmount };
			}
			else if (numSplits <= 0)
			{
				throw new ArgumentException("numSplits should be >= 1");
			}

			var splitAmounts = new decimal[numSplits];
			for (int i = 0; i < numSplits; i++)
			{
				splitAmounts[i] = 1.0m / (decimal)numSplits;
			}

			return Split(originalAmount, splitAmounts, 0);
		}

		public static Money[] Split(Money originalAmount, decimal[] splitAmounts, int? remainderIndex = null)
		{
			if (remainderIndex != null && (remainderIndex.Value >= splitAmounts.Count() || remainderIndex.Value < 0))
				throw new ArgumentException("remainderIndex is inconsistent with number of splitAmounts");

			if (remainderIndex == null)
			{
				var maxSplit = decimal.MinValue;

				int maxIndex = -1;
				for (var i = 0; i < splitAmounts.Count(); i++)
				{
					if (splitAmounts[i] >= maxSplit)
					{
						maxSplit = splitAmounts[i];
						maxIndex = i;
					}
				}

				remainderIndex = maxIndex;
			}

			Money total = new Money(0m);
			Money[] ret = new Money[splitAmounts.Count()];

			Money remainderBaseAmount = Money.Zero;

			for (int i = 0; i < splitAmounts.Count(); i++)
			{
				if (i == remainderIndex)
				{
					remainderBaseAmount = Multiply(originalAmount, splitAmounts[i], RoundingTypes.Drop);
					continue;
				}
				ret[i] = Multiply(originalAmount, splitAmounts[i], RoundingTypes.Drop);
				total += ret[i];
			}

			var remainder = originalAmount - (total + remainderBaseAmount);

			if (remainder > new Money(0.03m) && remainderBaseAmount != Money.Zero)
			{
				ret[remainderIndex.Value] = remainderBaseAmount;

				var penny = new Money(0.01m);
				while (remainder > Money.Zero)
				{
					for (var i = 0; i < splitAmounts.Count(); i++)
					{
						ret[i] += penny;
						remainder -= penny;

						if (remainder <= Money.Zero)
						{
							break;
						}
					}
				}
			}
			else
			{
				ret[remainderIndex.Value] = originalAmount - total;
			}


			return ret;
		}

		public static Money RoundByRules(Money originalAmount, RoundingTypes roundingType)
		{
			return new Money(RoundByRules(originalAmount._value, roundingType));
		}

		/// <summary>
		/// Rounds a given amount based on a given rounding rule.
		/// </summary>
		/// <param name="originalAmount">The amount to round.</param>
		/// <param name="roundingType">The rounding rule.</param>
		/// <returns>The originmalAmount with the rounding rule applied.</returns>
		private static decimal RoundByRules(decimal originalAmount, RoundingTypes roundingType)
		{
			try
			{
				decimal bigAmount;
				var epsilon = 0.00000000001m;
				if (originalAmount < 0m)
				{
					if (roundingType == RoundingTypes.Drop)
						roundingType = RoundingTypes.RoundUp;
					else if (roundingType == RoundingTypes.RoundUp)
						roundingType = RoundingTypes.Drop;
				}
				switch (roundingType)
				{
					case RoundingTypes.Drop:
						if (originalAmount > 0m)
							originalAmount += epsilon;
						else if (originalAmount < 0m)
							originalAmount -= epsilon;
						bigAmount = originalAmount * 100.0m;
						return Math.Floor(Math.Round(bigAmount, 8, MidpointRounding.AwayFromZero)) / 100.0m;
					case RoundingTypes.RoundUp:
						if (originalAmount > 0m)
							originalAmount += epsilon;
						else if (originalAmount < 0m)
							originalAmount -= epsilon;
						bigAmount = originalAmount * 100.0m;
						return Math.Ceiling(Math.Round(bigAmount, 8, MidpointRounding.AwayFromZero)) / 100.0m;
					case RoundingTypes.Round:
						return Math.Round(originalAmount, 2, MidpointRounding.AwayFromZero);
					case RoundingTypes.NoRounding:
						return originalAmount;
					default:
						throw new NotSupportedException("Rounding type must be set");
				}
			}
			catch (OverflowException)
			{
				return originalAmount;
			}
		}

		public override string ToString()
		{
			return ToString(true);
		}

		public string ToString(bool showCurrency)
		{
			if (showCurrency)
				return Value.ToString("c");
			else
				return string.Format(noSymbolFormat, "{0:c}", Value);
		}

		#region IComparable Members

		public int CompareTo(object obj)
		{
			if (!(obj is Money))
				throw new NotSupportedException();
			if (obj == null)
				throw new NotSupportedException();
			if (((Money)obj).Currency != Currency)
				throw new NotSupportedException();
			return Value.CompareTo(((Money)obj).Value);
		}

		#endregion

		public Money DecimalPart
		{
			get
			{
				return this - this.WholePart;
			}
		}

		public Money WholePart
		{
			get
			{
				return (this >= Money.Zero) ? new Money(Math.Floor(this.Value)) : new Money(Math.Ceiling(this.Value));
			}
		}
	}
}
