﻿
namespace SalonUltimate.Foundation.BasicTypes
{
	public class FirstTimeStatus
	{
		public bool IsFirstTimeClient { get { return IsFirstTimeLocal || IsFirstTimeGlobal; } }
		public bool IsFirstTimeLocal { get; set; }
		public bool IsFirstTimeGlobal { get; set; }
	}
}
