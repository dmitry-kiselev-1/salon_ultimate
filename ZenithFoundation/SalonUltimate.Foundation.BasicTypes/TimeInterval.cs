﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SalonUltimate.Foundation.BasicTypes
{
	public class TimeInterval : IEquatable<TimeInterval>
	{
		public static readonly TimeSpan DefaultPeriod = TimeSpan.FromMinutes(5);

		public TimeSpan Start { get; set; }
		public TimeSpan End { get; set; }

		public TimeSpan Duration => End - Start;

		public bool InsideOf(TimeInterval target) => Start >= target.Start && End <= target.End;

		/// <summary>
		/// Calculates whether this <see cref="TimeInterval"/> overlaps with the target <see cref="TimeInterval"/>
		/// </summary>
		/// <param name="target"></param>
		/// <returns></returns>
		public bool Overlaps(TimeInterval target)
		{
			return Start > target.Start && Start < target.End || End > target.Start && End < target.End ||
						 target.Start > Start && target.Start < End || target.End > Start && target.End < End ||
						 Start == target.Start && End == target.End;
		}

		public TimeSpan OverlapTime(TimeInterval target)
		{
			if (!Overlaps(target))
				return new TimeSpan(0, 0, 0);
			else
			{
				var maxStart = (this.Start > target.Start) ? this.Start : target.Start;
				var minEnd = (this.End < target.End) ? this.End : target.End;

				return minEnd - maxStart;
			}
		}

		public bool InsideOf(IEnumerable<TimeInterval> target)
		{
			foreach (var item in target)
			{
				if (this.InsideOf(item))
					return true;
			}
			return false;
		}

		public static List<TimeInterval> Intersect(IEnumerable<TimeInterval> what, IEnumerable<TimeInterval> withWhat)
		{
			List<TimeInterval> result = new List<TimeInterval>();

			if (what.Any() && withWhat.Any())
			{
				foreach (var interval in what)
				{
					var intersected = interval.Intersect(withWhat);
					if (intersected != null)
						result.Add(intersected);
				}

				foreach (var interval in withWhat)
				{
					var intersected = interval.Intersect(what);
					if (intersected != null)
						result.Add(intersected);
				}

				result = result.Distinct().OrderBy(x => x.Start).ToList();
			}

			return result;
		}

		public TimeInterval Intersect(IEnumerable<TimeInterval> intervals)
		{
			var result = new TimeInterval
			{
				Start = Start,
				End = End
			};      

			foreach(var interval in intervals)
			{
				var doesOverlap = interval.Overlaps(result);

				if (doesOverlap && interval.Start > result.Start)
					result.Start = interval.Start;
				if (doesOverlap && interval.End < result.End)
					result.End = interval.End;
			}

			return result;
		}

		public bool Equals(TimeInterval other)
		{
			if (other == null)
				return false;

			return Start == other.Start && End == other.End;
		}

		public override bool Equals(object obj)
		{
			return Equals(obj as TimeInterval);
		}

		public override int GetHashCode()
		{
			return $"{Start} - {End}".GetHashCode();
		}
	}
}
