﻿using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Redis
{
	[Obsolete("This service is obsolete and will be removed (or replaced) in a future version.")]
	public class RedisService : IRedisService
	{
		private readonly RedisOptions _redisOptions;

		public RedisService(IOptions<RedisOptions> redisOptionsAccessor)
		{
			_redisOptions = redisOptionsAccessor.Value ?? throw new InvalidOperationException("RedisOptions are not configured");
		}

		public async Task<(bool success, string message)> CheckRedisConnection()
		{
			try
			{
				using (var conn = await ConnectionMultiplexer.ConnectAsync(_redisOptions.ConnectionString))
				{
					if (conn != null && conn.GetEndPoints().Any())
					{
						return (true, $"RedisCheck({conn.ClientName}): Healthy");
					}
					return (false, $"RedisCheck({conn.ClientName}): Unhealthy");
				}
			}
			catch (Exception ex)
			{
				return (false, $"RedisCheck: Exception during check: {ex.GetType().FullName}");
			}
		}
	}
}
