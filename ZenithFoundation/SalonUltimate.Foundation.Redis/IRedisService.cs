﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Redis
{
	[Obsolete("This service is obsolete and will be removed (or replaced) in a future version.")]
	public interface IRedisService
	{
		Task<(bool success, string message)> CheckRedisConnection();
	}
}
