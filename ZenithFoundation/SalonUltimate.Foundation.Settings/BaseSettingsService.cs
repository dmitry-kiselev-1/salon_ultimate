﻿using System;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Settings
{
	public abstract class BaseSettingsService : SettingInfoService, IBaseSettingsService
    {
		#region Getters

		public virtual int? GetIntSetting(SettingKey settingKey) => GetSettingValue<int?>(settingKey);
		public virtual string GetStringSetting(SettingKey settingKey) => GetSettingValue<string>(settingKey);
		public virtual string GetTextSetting(SettingKey settingKey) => GetSettingValue<string>(settingKey);
		public virtual decimal? GetDecimalSetting(SettingKey settingKey) => GetSettingValue<decimal?>(settingKey);
		public virtual decimal? GetMoneySetting(SettingKey settingKey) => GetSettingValue<decimal?>(settingKey);
		public virtual bool? GetBoolSetting(SettingKey settingKey) => GetSettingValue<bool?>(settingKey);

		public virtual TEnum? GetEnumSetting<TEnum>(SettingKey settingKey) where TEnum : struct
		{
			var result = GetSettingValue<int?>(settingKey);

			return result.HasValue ? (TEnum)Enum.ToObject(typeof(TEnum), result.Value) : default(TEnum?);
		}

		protected abstract T GetSettingValue<T>(SettingKey settingKey, T defaultValue = default(T));

		#endregion

		#region Async Getters

		public virtual async Task<int?> GetIntSettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<int?>(settingKey));

		public virtual async Task<string> GetStringSettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<string>(settingKey));

		public virtual async Task<string> GetTextSettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<string>(settingKey));

		public virtual async Task<decimal?> GetDecimalSettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<decimal?>(settingKey));

		public virtual async Task<decimal?> GetMoneySettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<decimal?>(settingKey));

		public virtual async Task<bool?> GetBoolSettingAsync(SettingKey settingKey) => (await GetSettingValueAsync<bool?>(settingKey));

		public virtual async Task<TEnum?> GetEnumSettingAsync<TEnum>(SettingKey settingKey) where TEnum : struct
		{
			var result = await GetSettingValueAsync<int?>(settingKey);

			return result.HasValue ? (TEnum)Enum.ToObject(typeof(TEnum), result.Value) : default(TEnum?);
		}

		protected abstract Task<T> GetSettingValueAsync<T>(SettingKey settingKey, T defaultValue = default(T));

		#endregion
	}
}
