﻿using System.Collections.Generic;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Extensions;

namespace SalonUltimate.Foundation.Settings
{
	/// <summary>
	/// Base class to hold shared setting info and methods.
	/// </summary>
	public class SettingInfoService
	{
		protected static Dictionary<SettingKey, SettingInfo> SettingKeysMap = new Dictionary<SettingKey, SettingInfo>
		{
			[SettingKey.TrackClientAge] = new SettingInfo { SettingName = "TrackClientAge" },
			[SettingKey.CancellationPolicy] = new SettingInfo { SettingName = "CancellationPolicy" },
			[SettingKey.EnableNoShowCharge] = new SettingInfo { SettingName = "EnableNoShowCharge" },
			[SettingKey.SendEmpModifyBookingMsg] = new SettingInfo { SettingName = "SendEmpModifyBookingMsg" },
			[SettingKey.SendModifyBookingMsg] = new SettingInfo { SettingName = "SendModifyBookingMsg" },
			[SettingKey.SendBookingMsg] = new SettingInfo { SettingName = "SendBookingMsg" },
			[SettingKey.SendEmpBookingMsg] = new SettingInfo { SettingName = "SendEmpBookingMsg" },
			[SettingKey.DefaultContactType] = new SettingInfo { SettingName = "DefaultContactType" },
			[SettingKey.ConfEmailsStrictKey] = new SettingInfo { SettingName = "ConfEmailsStrict" },
			[SettingKey.TwilioPhone] = new SettingInfo { SettingName = "TwilioPhone" },
			[SettingKey.TwilioToken] = new SettingInfo { SettingName = "TwilioToken" },
			[SettingKey.TwilioSid] = new SettingInfo { SettingName = "TwilioSid" },
			[SettingKey.SmsConfEnabled] = new SettingInfo { SettingName = "SmsConfEnabled" },
			[SettingKey.ShowOnlyClockedInEmployeesInClientQueue] = new SettingInfo { SettingName = "ShowOnlyClockedInEmployeesInClientQueue" },
			[SettingKey.IsSportClips] = new SettingInfo { SettingName = "IsSportClips" },
			[SettingKey.AutoWalkInServiceId] = new SettingInfo { SettingName = "AutoWalkInServiceId" },
			[SettingKey.AutoWalkInServiceKey] = new SettingInfo { SettingName = "AutoWalkInServiceKey" },
			[SettingKey.MaxChildAge] = new SettingInfo { SettingName = "MaxChildAge" },
			[SettingKey.MaxAdultAge] = new SettingInfo { SettingName = "MaxAdultAge" },
			[SettingKey.ApptBookSales] = new SettingInfo { SettingName = "ApptBookSales", IsLocal = true },
			[SettingKey.ForceClientReferralQuestion] = new SettingInfo { SettingName = "ForceClientReferralQuestion" },
			[SettingKey.ForceMissingQueueEmail] = new SettingInfo { SettingName = "ForceMissingQueueEmail" },
			[SettingKey.ForceMissingEmail] = new SettingInfo { SettingName = "ForceMissingEmail" },
			[SettingKey.RequireClientGender] = new SettingInfo { SettingName = "RequireClientGender" },
			[SettingKey.ForceAgeInput] = new SettingInfo { SettingName = "ForceAgeInput" },
			[SettingKey.UpdateClientAge] = new SettingInfo { SettingName = "UpdateClientAge" },
			[SettingKey.ForceChildBirthday] = new SettingInfo { SettingName = "ForceChildBirthday" },
			[SettingKey.ForceAdultBirthday] = new SettingInfo { SettingName = "ForceAdultBirthday" },
			[SettingKey.AllowEnablingFirstAvailable] = new SettingInfo { SettingName = "AllowEnablingFirstAvailable" },
			[SettingKey.UseFirstAvailable] = new SettingInfo { SettingName = "UseFirstAvailable" },
			[SettingKey.TimeBlockSize] = new SettingInfo { SettingName = "TimeBlockSize" },
			[SettingKey.AllowCashedOutApptModify] = new SettingInfo { SettingName = "AllowCashedOutApptModify" },
			[SettingKey.TrackQueueRemoval] = new SettingInfo { SettingName = "TrackQueueRemoval" },
			[SettingKey.AppointToQueueTime] = new SettingInfo { SettingName = "AppointToQueueTime" },
			[SettingKey.TestSetting] = new SettingInfo { SettingName = "TestSetting" },
			[SettingKey.DefaultBreakDurationInMinutes] = new SettingInfo { SettingName = "DefaultBreakDurationInMinutes" },
			[SettingKey.UpwardsPriceChanges] = new SettingInfo { SettingName = "UpwardsPriceChanges" },
			[SettingKey.PreventActivity] = new SettingInfo { SettingName = "PreventActivity" },
			[SettingKey.AutoAssignFirstAvailableProvider] = new SettingInfo { SettingName = "AutoAssignFirstAvailableProvider" },
			[SettingKey.EmailMessagingAvailable] = new SettingInfo { SettingName = "EmailMessagingAvailable" },
			[SettingKey.UseConflictsWebsocket] = new SettingInfo { SettingName = "UseConflictsWebsocket" },
			[SettingKey.ZenithNewWsAccessToken] = new SettingInfo { SettingName = "ZenithNewWsAccessToken" },
			[SettingKey.NbbCheckInMaxWaitTimeInMinutes] = new SettingInfo { SettingName = "NbbCheckInMaxWaitTimeInMinutes" },
			[SettingKey.NbbTimeSlotsFeedCount] = new SettingInfo { SettingName = "NbbTimeSlotsFeedCount" },
			[SettingKey.EnableAppointmentRestrictions] = new SettingInfo { SettingName = "EnableAppointmentRestrictions" },
			[SettingKey.AppointmentRestrictionsDays] = new SettingInfo { SettingName = "AppointmentRestrictionsDays" },
			[SettingKey.WalkInMode] = new SettingInfo { SettingName = "WalkInMode" },
			[SettingKey.OABMode] = new SettingInfo { SettingName = "OABMode" },
			[SettingKey.AllowServiceProviderToPerformServicesOnMultipleClientsSimultaneously] = new SettingInfo { SettingName = "AllowServiceProviderToPerformServicesOnMultipleClientsSimultaneously" },
			[SettingKey.MissingAddressMode] = new SettingInfo { SettingName = "MissingAddressMode" },
			[SettingKey.OABSkipRequest] = new SettingInfo { SettingName = "OABSkipRequest" },
			[SettingKey.UseNewAppointmentBook] = new SettingInfo { SettingName = "UseNewAppointmentBook" },
			[SettingKey.HaveNewWebSystem] = new SettingInfo { SettingName = "HaveNewWebSystem" },
			[SettingKey.HaveNewWebSystem_ApptBook] = new SettingInfo { SettingName = "HaveNewWebSystem_ApptBook" },
			[SettingKey.HaveNewWebSystem_Queue] = new SettingInfo { SettingName = "HaveNewWebSystem_Queue" },
			[SettingKey.HaveNewWebSystem_ClientMaintenance] = new SettingInfo { SettingName = "HaveNewWebSystem_ClientMaintenance" },
			[SettingKey.OABAllowCancel] = new SettingInfo { SettingName = "OABAllowCancel" },
			[SettingKey.OABAllowCancelHours] = new SettingInfo { SettingName = "OABAllowCancelHours" },
			[SettingKey.BBContactMeMessageTemplate] = new SettingInfo { SettingName = "BBContactMeMessageTemplate" },
			[SettingKey.BBContactMeAddress] = new SettingInfo { SettingName = "BBContactMeAddress" },
			[SettingKey.DefGiftExp] = new SettingInfo { SettingName = "DefGiftExp" },
			[SettingKey.MailFee] = new SettingInfo { SettingName = "MailFee" },
			[SettingKey.WebGiftDiscount] = new SettingInfo { SettingName = "WebGiftDiscount" },
			[SettingKey.Website] = new SettingInfo { SettingName = "Website" },
			[SettingKey.Facebook] = new SettingInfo { SettingName = "Facebook" },
			[SettingKey.Twitter] = new SettingInfo { SettingName = "Twitter" },
			[SettingKey.Yelp] = new SettingInfo { SettingName = "Yelp" },
			[SettingKey.GooglePlus] = new SettingInfo { SettingName = "GooglePlus" },
			[SettingKey.Instagram] = new SettingInfo { SettingName = "Instagram" }, // TODO: make sure it is really named "Instagram" or really in use
			[SettingKey.MercuryWebAccount] = new SettingInfo { SettingName = "MercuryWebMid" },
			[SettingKey.MercuryWebPassword] = new SettingInfo { SettingName = "MercuryWebPassword" },
			[SettingKey.MercuryWebUrl] = new SettingInfo { SettingName = "MercuryWebUrl" },
			[SettingKey.Disclaimer] = new SettingInfo { SettingName = "Disclaimer" },
			[SettingKey.AllowPurgeOnlineWaitList] = new SettingInfo { SettingName = "AllowPurgeOnlineWaitList" },
			[SettingKey.AutoWalkInNewServiceId] = new SettingInfo { SettingName = "AutoWalkInNewServiceId" },
			[SettingKey.AutoWalkInNewServiceKey] = new SettingInfo { SettingName = "AutoWalkInNewServiceKey" },
			[SettingKey.TryHandleNwResponseError] = new SettingInfo { SettingName = "TryHandleNwResponseError" },
			[SettingKey.ApptCardType] = new SettingInfo { SettingName = "ApptCardType" },
			[SettingKey.Email] = new SettingInfo { SettingName = "Email" },
			[SettingKey.Phone] = new SettingInfo { SettingName = "Phone" },
			[SettingKey.Street1] = new SettingInfo { SettingName = "Street1" },
			[SettingKey.Street2] = new SettingInfo { SettingName = "Street2" },
			[SettingKey.City] = new SettingInfo { SettingName = "City" },
			[SettingKey.Zip] = new SettingInfo { SettingName = "Zip" },
			[SettingKey.State] = new SettingInfo { SettingName = "State" },
			[SettingKey.SalonName] = new SettingInfo { SettingName = "SalonName" },
			[SettingKey.Latitude] = new SettingInfo { SettingName = "ForceGeoLat" },
			[SettingKey.Longitude] = new SettingInfo { SettingName = "ForceGeoLon" },
			[SettingKey.AllowCheckoutWithoutSale] = new SettingInfo { SettingName = "AllowCheckoutWithoutSale" },
			[SettingKey.IgnoreWaitTimeOnCheckIn] = new SettingInfo { SettingName = "IgnoreWaitTimeOnCheckIn" },
			[SettingKey.OnlineCheckinTrackArrival] = new SettingInfo { SettingName = "OnlineCheckinTrackArrival" },
			[SettingKey.RemoveClientsFromQueueAllowed] = new SettingInfo { SettingName = "RemoveClientsFromQueueAllowed" },
			[SettingKey.DisableQueueGrouping] = new SettingInfo { SettingName = "DisableQueueGrouping" },
			[SettingKey.DisableFormulas] = new SettingInfo { SettingName = "DisableFormulas" },
			[SettingKey.UseQueueLength] = new SettingInfo { SettingName = "UseQueueLength" },
			[SettingKey.DisableClientsMerge] = new SettingInfo { SettingName = "DisableClientsMerge" },
			[SettingKey.DisableTimeClock] = new SettingInfo { SettingName = "DisableTimeClock" },
			[SettingKey.DisableRebook] = new SettingInfo { SettingName = "DisableRebook" },
			[SettingKey.PromptWorkTickets] = new SettingInfo { SettingName = "PromptWorkTickets" },
			[SettingKey.ShouldExpandMidDayScheduleForWaitTime] = new SettingInfo { SettingName = "ShouldExpandMidDayScheduleForWaitTime" },
			[SettingKey.MidDayScheduleExpandDurationMins] = new SettingInfo { SettingName = "MidDayScheduleExpandDurationMins" },
			[SettingKey.AutoBookAppointmentOnCheckin] = new SettingInfo { SettingName = "AutoBookAppointmentOnCheckin" },
			[SettingKey.StorePictureUrl] = new SettingInfo { SettingName = "StorePictureUrl" },
			[SettingKey.StorePicturePreviewUrl] = new SettingInfo { SettingName = "StorePicturePreviewUrl" }
		};

		protected virtual string GetSettingName(SettingKey settingKey)
		{
			return GetSettingInfo(settingKey)?.SettingName;
		}

		protected virtual SettingInfo GetSettingInfo(SettingKey settingKey)
		{
			return SettingKeysMap.Get(settingKey) ?? throw new ZenithValidationException($"Setting name for key: {settingKey}  doesn't exist");
		}
	}
}
