﻿using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Settings
{
	public interface IBaseSettingsService
    {
		#region Getters

		int? GetIntSetting(SettingKey settingKey);
		string GetStringSetting(SettingKey settingKey);
		string GetTextSetting(SettingKey settingKey);
		decimal? GetDecimalSetting(SettingKey settingKey);
		decimal? GetMoneySetting(SettingKey settingKey);
		bool? GetBoolSetting(SettingKey settingKey);

		#endregion

		#region Async Getters

		Task<int?> GetIntSettingAsync(SettingKey settingKey);
		Task<string> GetStringSettingAsync(SettingKey settingKey);
		Task<string> GetTextSettingAsync(SettingKey settingKey);
		Task<decimal?> GetDecimalSettingAsync(SettingKey settingKey);
		Task<decimal?> GetMoneySettingAsync(SettingKey settingKey);
		Task<bool?> GetBoolSettingAsync(SettingKey settingKey);

		#endregion
	}
}
