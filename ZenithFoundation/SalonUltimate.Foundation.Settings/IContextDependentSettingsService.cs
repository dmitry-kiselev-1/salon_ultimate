﻿using System.Threading.Tasks;
using SalonUltimate.Foundation.Context;

namespace SalonUltimate.Foundation.Settings
{
	public interface IContextDependentSettingsService<TContext> where TContext : IAppAgnosticContext
	{
		#region Getters

		int? GetIntSetting(TContext context, SettingKey settingKey);
		string GetStringSetting(TContext context, SettingKey settingKey);
		string GetTextSetting(TContext context, SettingKey settingKey);
		decimal? GetDecimalSetting(TContext context, SettingKey settingKey);
		decimal? GetMoneySetting(TContext context, SettingKey settingKey);
		bool? GetBoolSetting(TContext context, SettingKey settingKey);

		#endregion

		#region Async Getters

		Task<int?> GetIntSettingAsync(TContext context, SettingKey settingKey);
		Task<string> GetStringSettingAsync(TContext context, SettingKey settingKey);
		Task<string> GetTextSettingAsync(TContext context, SettingKey settingKey);
		Task<decimal?> GetDecimalSettingAsync(TContext context, SettingKey settingKey);
		Task<decimal?> GetMoneySettingAsync(TContext context, SettingKey settingKey);
		Task<bool?> GetBoolSettingAsync(TContext context, SettingKey settingKey);

		#endregion
	}
}
