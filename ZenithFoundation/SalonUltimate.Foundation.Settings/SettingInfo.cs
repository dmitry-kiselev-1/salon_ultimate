﻿namespace SalonUltimate.Foundation.Settings
{
	public class SettingInfo
	{
		public string SettingName { get; set; }
		public bool IsLocal { get; set; }
	}
}
