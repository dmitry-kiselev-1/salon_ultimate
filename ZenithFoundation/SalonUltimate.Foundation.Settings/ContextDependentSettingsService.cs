﻿using System;
using System.Threading.Tasks;
using SalonUltimate.Foundation.Context;

namespace SalonUltimate.Foundation.Settings
{
	/// <summary>
	/// A base class for any settings service which is dependent from any kind of a system context.
	/// </summary>
	/// <typeparam name="TContext">An implementation of system context which is inherited from marker IAppAgnosticContext interface.</typeparam>
	public abstract class ContextDependentSettingsService<TContext> : SettingInfoService, IContextDependentSettingsService<TContext> where TContext : IAppAgnosticContext
	{
		#region Context-Dependent Getters

		public virtual int? GetIntSetting(TContext context, SettingKey settingKey) => GetSettingValue<int?>(context, settingKey);
		public virtual string GetStringSetting(TContext context, SettingKey settingKey) => GetSettingValue<string>(context, settingKey);
		public virtual string GetTextSetting(TContext context, SettingKey settingKey) => GetSettingValue<string>(context, settingKey);
		public virtual decimal? GetDecimalSetting(TContext context, SettingKey settingKey) => GetSettingValue<decimal?>(context, settingKey);
		public virtual decimal? GetMoneySetting(TContext context, SettingKey settingKey) => GetSettingValue<decimal?>(context, settingKey);
		public virtual bool? GetBoolSetting(TContext context, SettingKey settingKey) => GetSettingValue<bool?>(context, settingKey);

		public virtual TEnum? GetEnumSetting<TEnum>(TContext context, SettingKey settingKey) where TEnum: struct
		{
			var result = GetSettingValue<int?>(context, settingKey);

			return result.HasValue ? (TEnum)Enum.ToObject(typeof(TEnum), result.Value) : default(TEnum?);
		}

		protected abstract T GetSettingValue<T>(TContext context, SettingKey settingKey, T defaultValue = default);

		#endregion

		#region

		public virtual async Task<int?> GetIntSettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<int?>(context, settingKey));

		public virtual async Task<string> GetStringSettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<string>(context, settingKey));

		public virtual async Task<string> GetTextSettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<string>(context, settingKey));

		public virtual async Task<decimal?> GetDecimalSettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<decimal?>(context, settingKey));

		public virtual async Task<decimal?> GetMoneySettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<decimal?>(context, settingKey));

		public virtual async Task<bool?> GetBoolSettingAsync(TContext context, SettingKey settingKey) => (await GetSettingValueAsync<bool?>(context, settingKey));

		public virtual async Task<TEnum?> GetEnumSettingAsync<TEnum>(TContext context, SettingKey settingKey) where TEnum : struct
		{
			var result = await GetSettingValueAsync<int?>(context, settingKey);

			return result.HasValue ? (TEnum)Enum.ToObject(typeof(TEnum), result.Value) : default(TEnum?);
		}

		protected abstract Task<T> GetSettingValueAsync<T>(TContext context, SettingKey settingKey, T defaultValue = default(T));

		#endregion

	}
}
