﻿namespace SalonUltimate.Foundation.Settings
{
	public enum SettingKey
	{
		TrackClientAge = 0,
		CancellationPolicy = 1,
		EnableNoShowCharge = 2,
		SendEmpModifyBookingMsg = 3,
		SendModifyBookingMsg = 4,
		SendBookingMsg = 5,
		SendEmpBookingMsg = 6,
		DefaultContactType = 7,
		ConfEmailsStrictKey = 8,
		TwilioPhone = 9,
		TwilioToken = 10,
		TwilioSid = 11,
		SmsConfEnabled = 12,
		ShowOnlyClockedInEmployeesInClientQueue = 13,
		IsSportClips = 14,
		/// <summary> Represents Id in MSSQL DB </summary>
		AutoWalkInServiceId = 15,
		MaxChildAge = 16,
		MaxAdultAge = 17,
		ApptBookSales = 18,
		ForceClientReferralQuestion = 19,
		ForceMissingQueueEmail = 20,
		ForceMissingEmail = 21,
		RequireClientGender = 22,
		ForceAgeInput = 23,
		UpdateClientAge = 24,
		ForceChildBirthday = 25,
		ForceAdultBirthday = 26,
		AllowEnablingFirstAvailable = 27,
		UseFirstAvailable = 28,
		TimeBlockSize = 29,
		AllowCashedOutApptModify = 31,
		TrackQueueRemoval = 32,
		AppointToQueueTime = 33,
		DefaultBreakDurationInMinutes = 34,
		UpwardsPriceChanges = 35,
		/// <summary> Represents Id in MySQL DB </summary>
		AutoWalkInServiceKey = 36,
		PreventActivity = 37,
		AutoAssignFirstAvailableProvider = 38,
		EmailMessagingAvailable = 39,
		UseConflictsWebsocket = 40,
		ZenithNewWsAccessToken = 41,
		NbbCheckInMaxWaitTimeInMinutes = 42,
		NbbTimeSlotsFeedCount = 43,
		EnableAppointmentRestrictions = 44,
		AppointmentRestrictionsDays = 45,

		/// <summary> Doesn't represent any Id, just increments previous </summary>
		WalkInMode = 46,
		OABMode = 47,

		AllowServiceProviderToPerformServicesOnMultipleClientsSimultaneously = 48,
		MissingAddressMode = 49,
		UseNewAppointmentBook = 50,
		HaveNewWebSystem = 51,
		HaveNewWebSystem_ApptBook = 52,
		HaveNewWebSystem_Queue = 53,
		HaveNewWebSystem_ClientMaintenance = 54,

		OABSkipRequest = 55,
		/// <summary>Boolean. Allows appointment cancellations not later than amount of hours based on setting OABAllowCancelHours before the first appointment in StoreVisit</summary>
		OABAllowCancel = 56,
		/// <summary>Integer. Allows appointment cancellations not later than this amount of hours before the first appointment in StoreVisit</summary>
		OABAllowCancelHours = 57,

		BBContactMeMessageTemplate = 58,
		BBContactMeAddress = 59,

		DefGiftExp = 60,
		MailFee = 61,
		WebGiftDiscount = 62,

		// Salon social network links
		Website = 63,
		Facebook = 64,
		Twitter = 65,
		Yelp = 66,
		GooglePlus = 67,
		Instagram = 68,

		MercuryWebAccount = 69,
		MercuryWebPassword = 70,
		MercuryWebUrl = 71,
		Disclaimer = 72,

		AllowPurgeOnlineWaitList = 73,

		AutoWalkInNewServiceId = 74,
		AutoWalkInNewServiceKey = 75,

		TryHandleNwResponseError = 76,

		ApptCardType = 77,

		// Store contacts
		Email = 78,
		Phone = 79,
		Street1 = 80,
		Street2 = 81,
		City = 82,
		Zip = 83,
		State = 84,
		SalonName = 85,

		// Store coordinates
		Latitude = 86,
		Longitude = 87,

		// Victra-specific
		AllowCheckoutWithoutSale = 88,
		IgnoreWaitTimeOnCheckIn = 89,
		OnlineCheckinTrackArrival = 90,

		RemoveClientsFromQueueAllowed = 91,

		DisableQueueGrouping = 92,
		DisableFormulas = 93,
		UseQueueLength = 94,
		DisableClientsMerge = 95,
		DisableTimeClock = 96,
		DisableRebook = 97,
		PromptWorkTickets = 98,

		ShouldExpandMidDayScheduleForWaitTime = 99,
		MidDayScheduleExpandDurationMins = 100,
		AutoBookAppointmentOnCheckin = 101,

		StorePictureUrl = 102,
		StorePicturePreviewUrl = 103,

		// Add all settings above this, use proper enum value, increment this value
		TestSetting = 104
	}
}
