﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SalonUltimate.Foundation.Models.DbConflict
{
	public class EntityChangeWrapper
	{
		public EntityChangeWrapper(EntityEntry entityEntry, string rowVersionProperty = "UpdateStamp")
		{
			EntityTypeName = entityEntry.Entity.GetType().Name;
			PropertiesChanged = PropertyChangeWrapper.GetChanges(entityEntry).ToList();
			RowVersionProperty = rowVersionProperty;
		}

		public string EntityTypeName { get; set; }
		public List<PropertyChangeWrapper> PropertiesChanged { get; set; }

		public string RowVersionProperty { get; private set; }
		public object DbRowVersion => PropertiesChanged.FirstOrDefault(x => x.PropertyName == RowVersionProperty)?.DatabaseValue;
		public object CacheRowVersion => PropertiesChanged.FirstOrDefault(x => x.PropertyName == RowVersionProperty)?.ProposedValue;

		public override string ToString()
		{
			var result = $"==={Environment.NewLine}{EntityTypeName}:{Environment.NewLine}==={Environment.NewLine}";
			foreach (var property in PropertiesChanged)
				result += property.ToString() + Environment.NewLine;

			return result;
		}
	}
}
