﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SalonUltimate.Foundation.Models.DbConflict
{
	public class DbConcurrencyExceptionWrapper
	{
		private List<EntityChangeWrapper> _entitiesChanged = new List<EntityChangeWrapper>();

		public DbConcurrencyExceptionWrapper(DbUpdateConcurrencyException ex)
		{
			foreach (var entry in ex.Entries)
			{
				_entitiesChanged.Add(new EntityChangeWrapper(entry));
			}
		}

		public IReadOnlyList<EntityChangeWrapper> EntitiesChanged => _entitiesChanged;

		public IEnumerable<string> RowVersions =>
			EntitiesChanged.Select(x => $"{x.EntityTypeName} (db: {x.DbRowVersion}, ef: {x.CacheRowVersion})");

		public override string ToString()
		{
			var result = string.Empty;
			foreach (var entityChanged in _entitiesChanged)
				result += entityChanged.ToString() + Environment.NewLine;
			return result;
		}
	}
}
