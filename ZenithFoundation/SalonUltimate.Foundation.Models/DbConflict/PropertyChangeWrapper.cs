﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Models.DbConflict
{
	public class PropertyChangeWrapper
	{
		protected PropertyChangeWrapper() { }

		public static IEnumerable<PropertyChangeWrapper> GetChanges(EntityEntry entityEntry)
		{
			var dbValues = entityEntry.GetDatabaseValues();

			foreach (var property in entityEntry.CurrentValues.Properties)
			{
				yield return new PropertyChangeWrapper
				{
					PropertyName = property.Name,
					ProposedValue = entityEntry.CurrentValues[property],
					DatabaseValue = dbValues[property]
				};
			}
		}

		public object ProposedValue { get; set; }
		public object DatabaseValue { get; set; }
		public string PropertyName { get; set; }

		public override string ToString()
		{
			return $"{PropertyName}: {ProposedValue} (ef|db) {DatabaseValue}";
		}
	}
}
