﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Models
{
    public enum JobStatus
    {
        InProgress = 1,
        Completed = 2,
        Failed = 3,
        NotFound = 4
    }
}
