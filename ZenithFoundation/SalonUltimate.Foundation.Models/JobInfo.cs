﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Models
{
    public class JobInfo<T>
    {
        public JobStatus Status { get; set; }
        public T Result { get; set; }
    }
}
