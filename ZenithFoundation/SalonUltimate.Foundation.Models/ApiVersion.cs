﻿using Newtonsoft.Json;

namespace SalonUltimate.Foundation.Models
{
	public class ApiVersion
	{
		public string AssemblyVersion { get; set; }
		[JsonProperty(PropertyName = "commit")]
		public string CommitVersion { get; set; }
		[JsonProperty(PropertyName = "built_at")]
		public string BuiltAt { get; set; }
		[JsonProperty(PropertyName = "api_version")]
		public string ControllersVersion { get; set; } = "v2";
		[JsonProperty(PropertyName = "deployment")]
		public DeploymentInfo Deployment { get; set; }
	}
}
