﻿using Newtonsoft.Json;

namespace SalonUltimate.Foundation.Models.V1
{
	public class ApiVersion
	{
		public string AssemblyVersion { get; set; }
		[JsonProperty(PropertyName = "commit")]
		public string CommitVersion { get; set; }
		[JsonProperty(PropertyName = "built_at")]
		public string BuiltAt { get; set; }
	}
}
