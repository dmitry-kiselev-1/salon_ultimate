﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Models
{
	public class DeploymentInfo
	{
		public string Application { get; set; }
		public string Environment { get; set; }
		public string Branch { get; set; }
		public string BuildDate { get; set; }
		public string PipelineId { get; set; }
		public string JobId { get; set; }

		public DeploymentInfo() { }

		public DeploymentInfo(AppInsightsMetrics metrics)
		{
			const string NA = "n/a";

			Application = metrics.Application;
			Environment = metrics.Environment;
			Branch = metrics.AdditionalMetrics?.ContainsKey(nameof(Branch)) == true ? metrics.AdditionalMetrics[nameof(Branch)] : NA;
			BuildDate = metrics.AdditionalMetrics?.ContainsKey(nameof(BuildDate)) == true ? metrics.AdditionalMetrics[nameof(BuildDate)] : NA;
			PipelineId = metrics.AdditionalMetrics?.ContainsKey(nameof(PipelineId)) == true ? metrics.AdditionalMetrics[nameof(PipelineId)] : NA;
			JobId = metrics.AdditionalMetrics?.ContainsKey(nameof(JobId)) == true ? metrics.AdditionalMetrics[nameof(JobId)] : NA;
		}
	}
}
