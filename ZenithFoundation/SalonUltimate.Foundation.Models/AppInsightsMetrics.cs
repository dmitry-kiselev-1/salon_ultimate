﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Models
{
	public class AppInsightsMetrics
	{
		public string Application { get; set; }
		public string Version { get; set; }
		public string Environment { get; set; }

		public IDictionary<string, string> AdditionalMetrics { get; set; }
	}
}
