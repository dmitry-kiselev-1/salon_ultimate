﻿namespace SalonUltimate.Foundation.Extensions
{
		public static class StringExtensions
		{
			public static string Ellipsize(this string text, int maxLength)
			{
				return (text.Length > maxLength) ? text.Substring(0, maxLength - 2) + ".." : text;
			}
		}
}
