﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Exceptions.Validation;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionApiBehaviourExtensions
	{
		public static IServiceCollection ConfigureZenithApiBehaviour(this IServiceCollection services)
		{
			services.Configure<ApiBehaviorOptions>(o =>
			{
				o.InvalidModelStateResponseFactory = actionContext =>
				{
					var modelValidationResult = new ValidationResultExceptionInfo();
					foreach (var modelIssue in actionContext.ModelState)
					{
						foreach (var modelError in modelIssue.Value.Errors)
						{
							modelValidationResult.AddError(modelError.ErrorMessage);
						}
					}

					throw new ZenithValidationException(modelValidationResult);
				};
			});

			return services;
		}
	}
}
