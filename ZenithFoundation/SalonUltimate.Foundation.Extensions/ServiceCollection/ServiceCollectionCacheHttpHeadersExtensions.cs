﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Marvin.Cache.Headers;
using Marvin.Cache.Headers.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	// That code uses this library:
	// https://github.com/KevinDockx/HttpCacheHeaders
	// Its source code contains descriptions for config parameters.

	public static class ServiceCollectionCacheHttpHeadersExtensions
	{
		/// <summary>
		/// Adds cache-control headers to the response. See https://github.com/KevinDockx/HttpCacheHeaders
		/// </summary>
		/// <param name="services"></param>
		/// <param name="expirationModelOptions">Pass deserialized options from the configuration</param>
		/// <param name="validationModelOptions">Pass deserialized options from the configuration</param>
		/// <param name="dateParserFunc"></param>
		/// <param name="storeKeyGeneratorFunc"></param>
		/// <param name="validatorValueStoreFunc"></param>
		/// <param name="eTagGeneratorFunc"></param>
		/// <returns></returns>
		public static IServiceCollection AddSuHttpCacheHeaders(this IServiceCollection services,
			ExpirationModelOptions expirationModelOptions = null,
			ValidationModelOptions validationModelOptions = null,
			Func<IServiceProvider, IDateParser> dateParserFunc = null,
			Func<IServiceProvider, IStoreKeyGenerator> storeKeyGeneratorFunc = null,
			Func<IServiceProvider, IValidatorValueStore> validatorValueStoreFunc = null,
			Func<IServiceProvider, IETagGenerator> eTagGeneratorFunc = null
			)
		{
			var mapper = new MapperConfiguration(mce =>
			{
				mce.CreateMap<ExpirationModelOptions, ExpirationModelOptions>();
				mce.CreateMap<ValidationModelOptions, ValidationModelOptions>();
			}).CreateMapper();

			// Here we can override default options for http caching with our own common options for SU projects.

			services.AddHttpCacheHeaders(
				expirationModelOptionsAction:	emo => mapper.Map(expirationModelOptions ?? DefaultExpirationModelOptions, emo),
				validationModelOptionsAction:	vmo => mapper.Map(validationModelOptions ?? DefaultValidationModelOptions, vmo),
				dateParserFunc:					dateParserFunc,
				validatorValueStoreFunc:		validatorValueStoreFunc,
				storeKeyGeneratorFunc:			storeKeyGeneratorFunc,
				eTagGeneratorFunc:				eTagGeneratorFunc
			);

			return services;
		}

		public static void UseSuHttpCacheHeaders(this IApplicationBuilder app) // it is named "Su" to prevent conflicts with the original lib
		{
			app.UseHttpCacheHeaders();
		}

		private static ExpirationModelOptions DefaultExpirationModelOptions => new ExpirationModelOptions
		{
			CacheLocation = CacheLocation.Private
		};

		private static ValidationModelOptions DefaultValidationModelOptions
		{
			get
			{
				var result = new ValidationModelOptions();

				var vary = new List<string>();
				vary.AddRange(result.Vary);
				vary.Add("X-BookedBy-Context");
				vary.Add("Authorization");

				result.Vary = vary;
				return result;
			}
		}
	}
}
