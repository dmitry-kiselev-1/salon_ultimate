﻿using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Healthchecks;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionHealthCheckExtensions
	{
		public static IHealthChecksBuilder ConfigureHealthCheck(this IServiceCollection services, bool registerRedis = false)
		{
			var healthCheckBuilder = services.AddHealthChecks();
			if (registerRedis)
			{
				healthCheckBuilder.AddCheck<RedisHealthCheck>("redis_healch_check");
			}

			return healthCheckBuilder;
		}
	}
}
