﻿using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionAppInsightsExtensions
	{
		public static void ConfigureAppInsights(this IServiceCollection services)
		{
			var config = services.BuildServiceProvider().GetRequiredService<IConfiguration>();

			var instrumentationKey = config["ApplicationInsights:InstrumentationKey"];
			// Required metrics
			var metrics = new AppInsightsMetrics
			{
				Application = config["AppInfo:AppName"],
				Environment = config["AppInfo:Environment"],
				Version = Assembly.GetEntryAssembly().GetName().Version.ToString()
			};

			// Additional metrics
			var section = config.GetSection("BuildInfo");
			if (section != null)
			{
				metrics.AdditionalMetrics = section.AsEnumerable()
					.ToDictionary(
						x => x.Key.Split(':').Last(),
						x => x.Value);
			}

			services.ConfigureApplicationInsights(instrumentationKey, metrics);
		}
	}
}
