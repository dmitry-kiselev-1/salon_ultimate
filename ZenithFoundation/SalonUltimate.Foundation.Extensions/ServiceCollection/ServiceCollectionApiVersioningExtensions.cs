﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Mvc.Versioning.Conventions;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
    public static class ServiceCollectionApiVersioningExtensions
	{
		public static void ConfigureApiVersioning(this IServiceCollection services,
			ApiVersion defaultApiVersion = null, bool versioningByNamespace = false)
		{
			services.AddApiVersioning(options =>
			{
				if (defaultApiVersion != null)
				{
					options.DefaultApiVersion = defaultApiVersion;
				}

				options.ReportApiVersions = true;
				options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
				options.AssumeDefaultVersionWhenUnspecified = true;

				if (versioningByNamespace)
				{
					options.Conventions.Add(new VersionByNamespaceConvention());
				}
			});

			var apiVersionDescriptionProvider = services.BuildServiceProvider().GetService<IApiVersionDescriptionProvider>();
			var wrapper = new ApiVersionDescriptionProviderWrapper
			{
				ApiVersionDescriptionProvider = apiVersionDescriptionProvider
			};

			services.AddSingleton(wrapper);
		}

		public static IServiceCollection AddVersionedApiExplorer(this IServiceCollection serviceCollection,
			string groupNameFormat, bool substituteApiVersionInUrl, string substitutionFormat, ApiVersion defaultApiVersion = null)
		{
			return serviceCollection.AddVersionedApiExplorer(options =>
			{
				options.GroupNameFormat = groupNameFormat;

				// note: this option is only necessary when versioning by url segment. the SubstitutionFormat
				// can also be used to control the format of the API version in route templates
				options.SubstituteApiVersionInUrl = substituteApiVersionInUrl;
				options.SubstitutionFormat = substitutionFormat;

				if (defaultApiVersion != null)
				{
					options.DefaultApiVersion = defaultApiVersion;
					options.AssumeDefaultVersionWhenUnspecified = true;
				}
			});
		}
	}
}
