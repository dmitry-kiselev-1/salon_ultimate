﻿using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
    public static class ServiceCollectionApplicationInsihgtsExtensions
    {
	    public static void ConfigureApplicationInsights(this IServiceCollection services, 
		    string instrumentationKey, AppInsightsMetrics appInsightsMetrics)
		{
			services.AddApplicationInsightsTelemetry(instrumentationKey);
			services.AddSingleton<ITelemetryInitializer, TelemetryInitializer>(x => new TelemetryInitializer(appInsightsMetrics));
			services.AddSingleton(appInsightsMetrics);
		}
	}
}
