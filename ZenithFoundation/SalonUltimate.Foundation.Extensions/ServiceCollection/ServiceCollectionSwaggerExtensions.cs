﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using SalonUltimate.Foundation.DTOs;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionSwaggerExtensions
	{
		public static void ConfigureSwaggerGen(this IServiceCollection services, 
			InfoApiVersionDTO infoApiVersion, bool jwtToken = false, Action<SwaggerGenOptions> additionalSetup = null)
		{
			services.AddSwaggerGen(options =>
			{
				// resolve the IApiVersionDescriptionProvider service
				// note: that we have to build a temporary service provider here because one has not been created yet
				var provider = services.BuildServiceProvider().GetService<IApiVersionDescriptionProvider>();

				if (provider?.ApiVersionDescriptions != null)
				{
					// add a swagger document for each discovered API version
					// note: you might choose to skip or document deprecated API versions differently
					foreach (var description in provider.ApiVersionDescriptions)
					{
						var info = new Info
						{
							Title = string.Format(infoApiVersion.TitleTemplate, description.ApiVersion.ToString()),
							Version = string.Format(infoApiVersion.VersionTemplate, description.ApiVersion.ToString()),
							Description = infoApiVersion.Description,
							Contact = new Contact { Name = infoApiVersion.ContactName, Email = infoApiVersion.ContactEmail },
							TermsOfService = infoApiVersion.TermsOfService,
							License = new License { Name = infoApiVersion.LicenseName, Url = infoApiVersion.LicenseUrl }
						};

						if (description.IsDeprecated)
						{
							info.Description += infoApiVersion.DescriptionPartWhenDepricated;
						}

						options.SwaggerDoc(description.GroupName, info);
					}
				}

				options.DocumentFilter<SwaggerAddEnumDescriptions>();

				options.IncludeXmlComments(infoApiVersion.XmlCommentsFilePath);
				//options.DescribeAllEnumsAsStrings();
				//options.DescribeAllParametersInCamelCase();
				//options.DescribeStringEnumsInCamelCase();

				if (jwtToken)
				{
					options.AddSecurityDefinition("Bearer", new ApiKeyScheme { In = "header", Description = "Please enter JWT with Bearer into field", Name = "Authorization", Type = "apiKey" });
					options.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> {
						{ "Bearer", Enumerable.Empty<string>() },
					});
				}
				
				additionalSetup?.Invoke(options);
			});
		}
	}
}
