﻿using Hangfire;
using Hangfire.Console;
using Hangfire.MemoryStorage;
using Hangfire.Redis;
using Microsoft.Extensions.DependencyInjection;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionHangfireExtensions
	{
		public static void ConfigureHangfireWithMemoryStorage(this IServiceCollection services)
		{
			services.AddHangfire(c =>
			{
				c.UseMemoryStorage();
				c.UseConsole();
			});
		}

		public static void ConfigureHangfireWithMemoryStorage(this IServiceCollection services, MemoryStorageOptions memoryStorageOptions)
		{
			services.AddHangfire(c =>
			{
				c.UseMemoryStorage(memoryStorageOptions);
				c.UseConsole();
			});
		}

		public static void ConfigureHangfireWithRedisStorage(this IServiceCollection services, string redisConnectionString, string redisPrefix = null)
		{
			services.AddHangfire(c =>
			{
				var redisStorageOptions = new RedisStorageOptions();
				redisStorageOptions.Prefix = RedisStorageOptions.DefaultPrefix + redisPrefix;
				c.UseRedisStorage(redisConnectionString, redisStorageOptions);
				c.UseConsole();
			});
		}
	}
}
