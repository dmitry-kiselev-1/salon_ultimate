﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace SalonUltimate.Foundation.Extensions.ServiceCollection
{
	public static class ServiceCollectionRegisterDependenciesExtensions
	{
		public static void RegisterDependencies<T>(this IServiceCollection services, T configurator) where T : IDependencyInjectionConfigurator
		{
			configurator.RegisterServices(SimpleRegister);

			void SimpleRegister(Type serviceType, Type implementationType, ServiceLifetime serviceLifetime)
			{
				services.Add(new ServiceDescriptor(serviceType, implementationType, serviceLifetime));
			}
		}
	}
	public interface IDependencyInjectionConfigurator
	{
		void RegisterServices(Action<Type, Type, ServiceLifetime> action);
	}

}
