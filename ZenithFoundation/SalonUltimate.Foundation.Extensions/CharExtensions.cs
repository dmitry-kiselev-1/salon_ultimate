﻿namespace SalonUltimate.Foundation.Extensions
{
	public static class CharExtensions
	{
		public static string GetTimes(this char symbol, int times)
		{
			string result = string.Empty;
			for (int i = 0; i < times; i++)
				result += symbol;
			return result;
		}
	}
}
