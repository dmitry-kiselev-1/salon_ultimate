﻿using System.Threading.Tasks;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Validation;

namespace SalonUltimate.Foundation.Extensions
{
	public static class ValidationResultExtensions
	{
		public static void ThrowIfFailed(this ValidationResult validationResult)
		{
			if (validationResult?.IsValid == false)
				throw new ZenithValidationException(validationResult.ToString());
		}

		public static Task ThrowIfFailed(this Task<ValidationResult> validationResultTask)
		{
			return validationResultTask.ContinueWith(t => t.Result.ThrowIfFailed());
		}
	}
}
