﻿using System;
using System.Runtime.CompilerServices;

namespace SalonUltimate.Foundation.Extensions
{
	public static class DateTimeExtensions
	{
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static DateTime TruncateTime(this DateTime dateTime) => dateTime.Date;

		public static int MinValidYear = 1801; // or 1753

		/// <summary>
		/// Indicates if this value is at least from 19th century.
		/// </summary>
		public static bool IsMeaningful(this DateTime val) => val.Year > MinValidYear;

		/// <summary>
		/// Returns this value if it is not too small, or null otherwise
		/// </summary>
		public static DateTime? GetMeaningfulOrNull(this DateTime val) =>
			val.IsMeaningful()
				? val 
				: default(DateTime?);
	}
}
