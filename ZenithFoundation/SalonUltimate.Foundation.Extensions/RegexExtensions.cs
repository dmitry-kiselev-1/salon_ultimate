﻿using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SalonUltimate.Foundation.Extensions
{
	public static class RegexExtensions
	{
		public static string ReplaceGroup(this Regex regex, string input, char replacement)
		{
			return regex.Replace(
				input,
				m =>
				{
					var s = input;
					if(m.Groups.Count > 1)
					{
						for(var i = 1; i < m.Groups.Count; i++)
						{
							s = ReplaceGroup(m, i, s, replacement);
						}
					}
					return s;
				});
		}

		private static string ReplaceGroup(Match m, int groupKey, string input, char replacement)
		{
			var group = m.Groups[groupKey];
			if (group != null)
			{
				var sb = new StringBuilder();
				var previousCaptureEnd = 0;
				foreach (var capture in group.Captures.Cast<Capture>())
				{
					var currentCaptureEnd = capture.Index + capture.Length - m.Index;
					var currentCaptureLength = capture.Index - m.Index - previousCaptureEnd;
					sb.Append(input.Substring(previousCaptureEnd, currentCaptureLength));
					sb.Append(replacement.GetTimes(capture.Length));
					previousCaptureEnd = currentCaptureEnd;
				}
				sb.Append(input.Substring(previousCaptureEnd));

				return sb.ToString();
			}
			return input;
		}
	}
}
