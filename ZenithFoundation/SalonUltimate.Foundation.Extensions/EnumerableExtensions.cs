﻿using System.Collections.Generic;
using System.Linq;

namespace SalonUltimate.Foundation.Extensions
{
	public static class EnumerableExtensions
	{
		public static IEnumerable<IEnumerable<T>> DifferentCombinations<T>(this IEnumerable<T> elements, int k)
		{
			return k == 0 ? new[] { new T[0] } :
				elements.SelectMany((e, i) =>
					Enumerable.Select<IEnumerable<T>, IEnumerable<T>>(elements.Skip(i + 1).DifferentCombinations(k - 1), c => (new[] { e }).Concat(c)));
		}

		public static T[] ToNotNullArray<T>(this IEnumerable<T> collection)
		{
			if (collection == null)
				return new T[] { };
			return collection.ToArray();
		}
	}
}
