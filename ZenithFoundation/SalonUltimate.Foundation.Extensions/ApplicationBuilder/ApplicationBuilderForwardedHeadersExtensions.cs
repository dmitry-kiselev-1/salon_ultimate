﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpOverrides;

namespace SalonUltimate.Foundation.Extensions.ApplicationBuilder
{
    public static class ApplicationBuilderForwardedHeadersExtensions
    {
	    public static IApplicationBuilder EnableForwardedHeaders(this IApplicationBuilder app)
	    {
		    var forwardingOptions = new ForwardedHeadersOptions
		    {
			    ForwardedHeaders = ForwardedHeaders.XForwardedFor
			                       | ForwardedHeaders.XForwardedProto
			                       | ForwardedHeaders.XForwardedHost
		    };
		    forwardingOptions.KnownNetworks.Clear();
		    forwardingOptions.KnownProxies.Clear();

		    return app.UseForwardedHeaders(forwardingOptions);
	    }
    }
}
