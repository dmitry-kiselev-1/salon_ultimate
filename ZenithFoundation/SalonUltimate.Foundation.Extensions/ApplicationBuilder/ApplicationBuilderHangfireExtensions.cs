﻿using Hangfire;
using Microsoft.AspNetCore.Builder;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.Foundation.Extensions.ApplicationBuilder
{
	public static class ApplicationBuilderHangfireExtensions
	{
		public static void UseHangfireDashboard(this IApplicationBuilder app)
		{
			app.UseHangfireDashboard(options: new DashboardOptions
			{
				Authorization = new[] { new HangfireAuthorizationFilter() }
			});
		}

		public static void UseHangfireServer(this IApplicationBuilder app)
		{
			HangfireApplicationBuilderExtensions.UseHangfireServer(app);
		}
	}
}
