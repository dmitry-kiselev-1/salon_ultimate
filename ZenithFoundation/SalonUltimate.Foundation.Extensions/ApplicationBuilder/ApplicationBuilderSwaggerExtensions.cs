﻿using System.Linq;
using Microsoft.AspNetCore.Builder;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.Foundation.Extensions.ApplicationBuilder
{
	public static class ApplicationBuilderSwaggerExtensions
	{
		public static void UseSwaggerWithUI(this IApplicationBuilder app, ApiVersionDescriptionProviderWrapper wrapper = null)
		{
			app.UseSwagger();

			if (wrapper?.ApiVersionDescriptionProvider != null)
			{
				app.UseSwaggerUI(options =>
				{
					// build a swagger endpoint for each discovered API version
					foreach (var description in wrapper.ApiVersionDescriptionProvider.ApiVersionDescriptions
						// first version should be the selected one
						.OrderByDescending(x => x.ApiVersion == wrapper.DefaultApiVersion)
						// then order by versions from the latest to the oldest
						.ThenByDescending(x => x.ApiVersion.MajorVersion.GetValueOrDefault()))
					{
						options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
							description.GroupName.ToUpperInvariant());
					}
				});
			}
			else
			{
				app.UseSwaggerUI();
			}
		}
	}
}
