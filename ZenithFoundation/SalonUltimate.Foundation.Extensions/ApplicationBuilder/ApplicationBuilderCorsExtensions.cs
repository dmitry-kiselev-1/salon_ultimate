﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Extensions.ApplicationBuilder
{
	public static class ApplicationBuilderCorsExtensions
	{
		/// <summary>
		/// Read and apply CORS settings from configuration (appsettings.json)
		/// </summary>
		public static void UseCors(this IApplicationBuilder app, IConfiguration configuration, string configurationKey = "Cors:Origins")
		{
			var origins = GetOrigins(configuration, configurationKey);

			app.UseCors(builder => builder
				.SetIsOriginAllowedToAllowWildcardSubdomains()
				.WithOrigins(origins)
				.AllowAnyMethod()
				.AllowAnyHeader()
				.AllowCredentials());
		}

		/// <summary>
		/// Extract origins from configuration as array
		/// </summary>
		/// <exception cref="ArgumentNullException">Will throw if CORS section missed or empty</exception>
		public static string[] GetOrigins(IConfiguration configuration, string configurationKey)
		{
			var section = configuration.GetSection(configurationKey);

			if (!section.Exists())
			{
				var errorMessage = $"CORS error: missed or empty section {configurationKey} from appsettings.json";
				Console.WriteLine(errorMessage);
				throw new FoundationCorsConfigurationMissedOrEmptyException(userMessage: errorMessage);
			}

			// will be null if section value is not array of string:
			var origins = section.Get<string[]>();

			if (origins is null) 
			{
				var errorMessage = $"CORS error: wrong section value (must be array of string) {configurationKey} from appsettings.json";
				Console.WriteLine(errorMessage);
				throw new FoundationCorsConfigurationIncorrectException(userMessage: errorMessage);
			}

			return origins;
		}
	}
}
