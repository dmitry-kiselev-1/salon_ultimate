﻿using System.Collections.Generic;

namespace SalonUltimate.Foundation.Extensions
{
	public static class CollectionExtensions
	{
		public static TV Get<TK, TV>(this Dictionary<TK, TV> dic, TK key, TV defaultValue = default(TV))
		{
			return dic.TryGetValue(key, out TV value) ? value : defaultValue;
		}
	}
}
