﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Extensions
{
	public static class TaskExtensions
	{
		public static T WaitOrThrow<T>(this Task<T> task)
		{
			try
			{
				task.Wait();
				return task.Result;
			}
			catch (AggregateException ae)
			{
				ExceptionDispatchInfo.Capture(ae.InnerException).Throw();
				return default(T); // will not be called because of exception,
								   // but makes the compiler to think that all code paths return a value
			}
		}

		public static T GetResultOrDefault<T>(this Task<T> task, T defaultValue = default(T))
		{
			try
			{
				var result = task.GetAwaiter().GetResult();
				return result;
			}
			catch
			{
				return defaultValue;
			}
		}
	}
}
