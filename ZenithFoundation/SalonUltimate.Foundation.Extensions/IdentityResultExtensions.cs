﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Extensions
{
	public static class IdentityResultExtensions
	{
		public static void ThrowIfErrors(this IdentityResult result)
		{
			if (!result.Succeeded)
			{
				var errorMessage = result.Errors.Select(e => e.Description).FirstOrDefault();	// TODO: HACK: fix to return list of error. waiting for UI team

				throw new ZenithValidationException(errorMessage);
			}
		}
	}
}
