﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Helpers
{
	/// <summary>
	/// Provides methods to make get/post requests to remote servers.
	/// </summary>
	public interface IRestApiHelper
	{
		/// <summary>
		/// Gets an object if server returns JSON.
		/// </summary>
		/// <typeparam name="T">Type of response object.</typeparam>
		/// <param name="url">Url with all parameters.</param>
		/// <param name="cookies">Cookies KVP</param>
		/// <returns>Deserialized object</returns>
		Task<T> GetAsync<T>(string url, Dictionary<string, string> cookies = null) where T : class;
		/// <summary>
		/// Gets a string from remote server.
		/// </summary>
		/// <param name="url">Url with all parameters.</param>
		/// <returns>String</returns>
		Task<string> GetStringAsync(string url);
		/// <summary>
		/// Gets byte response from remote server.
		/// </summary>
		/// <param name="url">Url with all parameters.</param>
		/// <returns></returns>
		Task<byte[]> GetBytesAsync(string url);

		/// <summary>
		/// Posts an object as JSON data to given endpoint.
		/// </summary>
		/// <typeparam name="T">Type of response object.</typeparam>
		/// <param name="url">Url</param>
		/// <param name="payload">Payload object - will be serialized to JSON</param>
		/// <returns>Deserialized object.</returns>
		Task<T> PostJsonAsync<T>(string url, object payload = null, Dictionary<string, string> cookies = null) where T : class;
		/// <summary>
		/// Posts an object as JSON data and returns just a string from the server.
		/// </summary>
		/// <param name="url">Url</param>
		/// <param name="payload">Payload object - will be serialized to JSON</param>
		/// <returns>Deserialized object</returns>
		Task<string> PostJsonAsync(string url, object payload = null, Dictionary<string, string> cookies = null);
		/// <summary>
		/// Posts x-form-data-urlencoded and returns object
		/// </summary>
		/// <typeparam name="T">Type of response object.</typeparam>
		/// <param name="url">Url</param>
		/// <param name="payload">Payload object</param>
		/// <param name="includeNullValues">If false, will exclude null strings from form data request.</param>
		/// <returns></returns>
		Task<T> PostFormDataAsync<T>(string url, object payload = null, bool includeNullValues = true) where T : class;
		/// <summary>
		/// Posts x-form-data-urlencoded and returns string
		/// </summary>
		/// <param name="url">Url</param>
		/// <param name="payload">Payload object</param>
		/// <param name="includeNullValues">If false, will exclude null strings from form data request.</param>
		/// <returns></returns>
		Task<string> PostFormDataAsync(string url, object payload = null, bool includeNullValues = true);
	}
}
