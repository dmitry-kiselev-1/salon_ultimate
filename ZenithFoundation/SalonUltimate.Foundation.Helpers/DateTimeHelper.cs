﻿using System;
using SalonUltimate.Foundation.Time;

namespace SalonUltimate.Foundation.Helpers
{
	public class DateTimeHelper
	{
		/// <summary>
		/// Gets the current ticks in unix format.
		/// </summary>
		public static Int64 GetCurrentTicksInUnixFormat()
		{
			// Convert ticks to microseconds since unix epoch
			return (ClockProvider.UtcNow.Ticks - DateTime.UnixEpoch.Ticks) / 10 + 1;

		}
	}
}
