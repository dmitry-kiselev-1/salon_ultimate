﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace SalonUltimate.Foundation.Helpers.Loggers
{
	/// <summary>
	/// Provides a logger which logs specified general log messages to Application Insights. 
	/// This is useful to track events logged by the code we don't have access to. 
	/// E.g. this allows to track EF Core warnings which are generally logged only to ILogger.
	/// Taken from BookedBy project.
	/// </summary>
	public class ConditionalAppInsightsLoggerProvider : ILoggerProvider
	{
		protected readonly ISet<LogLevel> _levelFilter;
		protected readonly ISet<string> _eventNameFilter;

		public ConditionalAppInsightsLoggerProvider(ISet<LogLevel> supportedLevelFilters,
			ISet<string> supportedEventNames)
		{
			_levelFilter = supportedLevelFilters ?? new HashSet<LogLevel>();
			_eventNameFilter = supportedEventNames ?? new HashSet<string>();
		}

		public ILogger CreateLogger(string categoryName) 
			=> new ConditionalAppInsightsLogger(_levelFilter, _eventNameFilter);

		public void Dispose() { }
	}
}
