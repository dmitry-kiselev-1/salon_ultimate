﻿using Microsoft.ApplicationInsights;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;

namespace SalonUltimate.Foundation.Helpers.Loggers
{
	/// <summary>
	/// Logs specified general log messages to Application Insights. This is useful to track events logged by the code we don't have
	/// access to. E.g. this allows to track EF Core warnings which are generally logged only to ILogger.
	/// Taken from BookedBy project.
	/// </summary>
	public class ConditionalAppInsightsLogger : ILogger, IDisposable
	{
		protected readonly Lazy<TelemetryClient> _telemetryClient = new Lazy<TelemetryClient>(() => new TelemetryClient());
		protected readonly ISet<LogLevel> _levelFilter;
		protected readonly ISet<string> _eventNameFilter;

		public ConditionalAppInsightsLogger(ISet<LogLevel> levelFilter, ISet<string> eventNameFilter)
		{
			_levelFilter = levelFilter;
			_eventNameFilter = eventNameFilter;
		}

		public IDisposable BeginScope<TState>(TState state) 
			=> new DisposableAction(null);

		public void Dispose()
		{
			if (_telemetryClient.IsValueCreated)
				_telemetryClient.Value.Flush();
		}

		public bool IsEnabled(LogLevel logLevel) 
			=> _levelFilter.Contains(logLevel);

		public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
		{
			if (NeedTrackEvent(logLevel, eventId))
			{
				_telemetryClient.Value.TrackEvent(eventId.Name, new Dictionary<string, string>
				{
					["EventId"] = eventId.Id.ToString(),
					["State"] = state.ToString()
				});
			}
		}

		protected bool NeedTrackEvent(LogLevel logLevel, EventId eventId) 
			=> IsEnabled(logLevel) && _eventNameFilter.Contains(eventId.Name);
	}
}
