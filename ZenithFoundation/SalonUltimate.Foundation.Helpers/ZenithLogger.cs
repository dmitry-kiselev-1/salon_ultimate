﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Helpers
{
	/// <summary>
	/// Shared logger
	/// </summary>
    public static class ZenithLogger
    {
		public static ILoggerFactory LoggerFactory { get; set; }
		public static ILogger CreateLogger() => LoggerFactory?.CreateLogger(typeof(ZenithLogger)) ?? throw new Exception("LoggerFactory is not initialized.");
	}
}
