﻿using System;

namespace SalonUltimate.Foundation.Helpers
{
	/// <summary>
	/// Allows to execute arbitrary code after this object gets disposed.
	/// Example:
	/// using(_ = new DisposableAction(() => DoSomething())) { ... }
	/// </summary>
	public class DisposableAction : IDisposable
	{
		private readonly Action _action;

		public DisposableAction(Action action)
		{
			_action = action;
		}

		public void Dispose()
		{
			_action?.Invoke();
		}
	}
}
