﻿using System.Text.RegularExpressions;
using SalonUltimate.Foundation.Validation;

namespace SalonUltimate.Foundation.Helpers
{
	public class PersonNameHelper
	{
		private readonly Regex _clientNameRegex = new Regex(@"^[a-zA-Z0-9]+((['-]|\s)[a-zA-Z0-9]+)*$", RegexOptions.Compiled);

		public void ValidateNameField(ValidationResult validationResult, string name, string fieldName = "Name")
		{
			if(!string.IsNullOrEmpty(name) && !_clientNameRegex.IsMatch(name))
			{
				validationResult.AddError($"{fieldName} is incorrect", conflictData: name);
			}
		}

		public string NormalizePersonName(string input)
		{
			if(!string.IsNullOrEmpty(input))
			{
				input = input.Trim();
				input = Regex.Replace(input, @"^[a-zA-Z]|[\s|\-|'][a-zA-Z]", m => m.Value.ToUpper());
			}

			return input;
		}
	}
}
