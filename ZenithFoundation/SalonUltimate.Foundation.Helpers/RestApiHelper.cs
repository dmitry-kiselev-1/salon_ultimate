﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Helpers
{
	public class RestApiHelper : IRestApiHelper, IDisposable
	{
		protected static readonly HttpClientHandler _clientHandler;
		protected static readonly HttpClient _client;

		protected virtual HttpClientHandler ClientHandler => _clientHandler;
		protected virtual HttpClient Client => _client;

		public RestApiHelper() { }

		static RestApiHelper() // MSDN highly recommends to do so: https://docs.microsoft.com/ru-ru/dotnet/api/system.net.http.httpclient.-ctor?view=netframework-4.7.2
		{
			_clientHandler = new HttpClientHandler();
			_client = new HttpClient(_clientHandler);
		}

		public virtual void Dispose()
		{
		}

		#region Get

		/// <inheritdoc />
		public async Task<T> GetAsync<T>(string url, Dictionary<string, string> cookies = null) where T : class
		{
			var baseAddress = new Uri(url);
			SetCookies(cookies, baseAddress);

			var result = await Client.GetAsync(url);
			EnsureSuccessStatusCode(result);
			var deserializedResult = await DeserializeAsync<T>(result);
			return deserializedResult;
		}

		/// <inheritdoc />
		public async Task<string> GetStringAsync(string url)
		{
			var result = await Client.GetAsync(url);
			EnsureSuccessStatusCode(result);
			var str = await result.Content.ReadAsStringAsync();
			return str;
		}

		/// <inheritdoc />
		public async Task<byte[]> GetBytesAsync(string url)
		{
			var result = await Client.GetAsync(url);
			EnsureSuccessStatusCode(result);
			var bytes = await result.Content.ReadAsByteArrayAsync();
			return bytes;
		}

		#endregion

		#region Post

		/// <inheritdoc />
		public async Task<T> PostAsync<T>(string url, object payload = null, Dictionary<string, string> cookies = null) where T : class
		{
			var baseAddress = new Uri(url);
			SetCookies(cookies, baseAddress);

			var result = await Client.PostAsync(url, new JsonContent(payload));
			var deserializedResult = await DeserializeAsync<T>(result);
			return deserializedResult;
		}

		public async Task<T> PostJsonAsync<T>(string url, object payload = null, Dictionary<string, string> cookies = null) where T : class
		{
			var baseAddress = new Uri(url);
			SetCookies(cookies, baseAddress);

			var result = await Client.PostAsync(url, new JsonContent(payload));
			var deserializedResult = await DeserializeAsync<T>(result);
			return deserializedResult;
		}

		/// <inheritdoc />
		public async Task<string> PostJsonAsync(string url, object payload = null, Dictionary<string, string> cookies = null)
		{
			var baseAddress = new Uri(url);
			SetCookies(cookies, baseAddress);

			var result = await Client.PostAsync(url, new JsonContent(payload));
			EnsureSuccessStatusCode(result);
			var str = await result.Content.ReadAsStringAsync();
			return str;
		}

		/// <inheritdoc />
		public async Task<T> PostFormDataAsync<T>(string url, object payload = null, bool includeNullValues = true) where T : class
		{
			var keyValuePairs = GetKeyValuePairs(payload);
			var formContent = new FormUrlEncodedContent(keyValuePairs);
			var result = await Client.PostAsync(url, formContent);
			EnsureSuccessStatusCode(result);
			var deserializedResult = await DeserializeAsync<T>(result);
			return deserializedResult;
		}

		/// <inheritdoc />
		public async Task<string> PostFormDataAsync(string url, object payload = null, bool includeNullValues = true)
		{
			var keyValuePairs = GetKeyValuePairs(payload);
			var formContent = new FormUrlEncodedContent(keyValuePairs);
			var result = await Client.PostAsync(url, formContent);
			EnsureSuccessStatusCode(result);
			var str = await result.Content.ReadAsStringAsync();
			return str;
		}

		#endregion

		private void SetCookies(Dictionary<string, string> cookies, Uri baseUri)
		{
			// TODO: Rewrite to support Singleton HttpClient
			return;
			
			var cookieContainer = new CookieContainer();
			ClientHandler.CookieContainer = cookieContainer;
			if (cookies != null)
			{
				foreach (var cookie in cookies)
					cookieContainer.Add(baseUri, new Cookie(cookie.Key, cookie.Value));
			}
		}

		/// <summary>
		/// Converts an object to key-value pairs of strings using reflection.
		/// </summary>
		/// <param name="payload">An object to convert.</param>
		/// <param name="includeNullProperties">If false, pairs with null value strings will be excluded from result collection.</param>
		/// <returns></returns>
		protected IEnumerable<KeyValuePair<string, string>> GetKeyValuePairs(object payload, bool includeNullProperties = true)
		{
			var result = payload?.GetType()
				.GetProperties()
				.Select(x => new KeyValuePair<string, string>($"{x.Name.First().ToString().ToLowerInvariant()}{x.Name.Substring(1)}", x.GetValue(payload)?.ToString()))
				.Where(x => includeNullProperties || x.Value != null)
				.ToArray();

			return result;
		}

		/// <summary>
		/// Extracts JSON from the response if it is success and casts to type T. Throws ZenithExternalResourceException if response has unsuccess code.
		/// </summary>
		/// <typeparam name="T">A type of server response object.</typeparam>
		/// <param name="response">HttpResponseMessage object</param>
		/// <returns>Deserialized object</returns>
		protected async Task<T> DeserializeAsync<T>(HttpResponseMessage response) where T : class
		{
			EnsureSuccessStatusCode(response);

			var stringContent = await response.Content.ReadAsStringAsync();
			var deserialized = JsonConvert.DeserializeObject(stringContent, typeof(T)) as T;

			return deserialized;
		}

		/// <summary>
		/// If status code of response is not success, throws ZenithExternalResourceException with request uri.
		/// </summary>
		/// <param name="response">HttpResponseMessage object.</param>
		/// <returns>True or throws exception.</returns>
		protected bool EnsureSuccessStatusCode(HttpResponseMessage response)
		{
			if (!response.IsSuccessStatusCode)
			{
				string responseStatus = $"{(int)response.StatusCode} {response.ReasonPhrase}";
				string responseHeaders = response.Headers?.ToString() ?? "Request failed with no response and ho headers";
				
				// circular links solution (Extensions <-> Helpers): RestApiHelper -> EnsureSuccessStatusCode changed
				string responseContent = "Response body is empty or cannot be read";
				try
				{
					string result = response.Content?.ReadAsStringAsync().GetAwaiter().GetResult();
					responseContent = result;
				}
				finally
				{
					throw new ZenithExternalResourceException("External data request failed", responseStatus, new
					{
						Headers = responseHeaders,
						Content = responseContent
					});
				}
			}
			else
				return true;
		}

		/// <summary>
		/// Internal class to easily convert object to json request payload.
		/// </summary>
		protected class JsonContent : StringContent
		{
			public JsonContent(object obj) :
				base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
			{ }
		}
	}

	public class TestRestApiHelper : RestApiHelper
	{
		protected readonly HttpClientHandler _overridenClientHandler;
		protected readonly HttpClient _overridenClient;

		public TestRestApiHelper(HttpClientHandler clientHandler, HttpClient client)
		{
			_overridenClient = client;
			_overridenClientHandler = clientHandler;
		}

		protected override HttpClientHandler ClientHandler => _overridenClientHandler ?? _clientHandler;
		protected override HttpClient Client => _overridenClient ?? _client;

		public override void Dispose()
		{
			_overridenClient?.Dispose();
			_overridenClientHandler?.Dispose();
		}
	}
}
