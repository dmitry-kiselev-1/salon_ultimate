﻿using System.Collections.Generic;
using System.Net;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Helpers
{
	public class StatusCodeHelper
	{
		private static readonly Dictionary<ResponseTypes, HttpStatusCode> _statusMap = new Dictionary<ResponseTypes, HttpStatusCode>()
		{
			{ ResponseTypes.NotImplemented, HttpStatusCode.NotImplemented },
			{ ResponseTypes.Success, HttpStatusCode.OK },
			{ ResponseTypes.NotAuthenticated, HttpStatusCode.Unauthorized },
			{ ResponseTypes.NoPermission, HttpStatusCode.Forbidden },
			{ ResponseTypes.FailedValidation, HttpStatusCode.BadRequest },
			{ ResponseTypes.Exception, HttpStatusCode.InternalServerError },
			{ ResponseTypes.NotFound, HttpStatusCode.NotFound },
			{ ResponseTypes.BadData, HttpStatusCode.InternalServerError },
			{ ResponseTypes.Conflict, HttpStatusCode.Conflict },
			{ ResponseTypes.InvalidOperation, HttpStatusCode.Forbidden }
		};

		public static HttpStatusCode GetStatusCodeForResponse(BaseResponseVM response)
		{
			return _statusMap[response.Result];
		}

		public static int GetIntStatusCodeForResponse(BaseResponseVM response)
		{
			return (int)_statusMap[response.Result];
		}
	}
}
