﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;

namespace SalonUltimate.Foundation.Helpers
{
	public static class ReflectionHelper
	{
		public static bool IsNullable(Type t) =>
			!t.IsValueType || t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>);

		public static ParameterInfo GetBodyVmParameter(MethodInfo methodInfo)
		{
			return methodInfo.GetParameters()
				.SingleOrDefault(p => !p.ParameterType.IsValueType
															&& p.CustomAttributes.Any(a => a.AttributeType == typeof(FromBodyAttribute)));
		}
	}
}
