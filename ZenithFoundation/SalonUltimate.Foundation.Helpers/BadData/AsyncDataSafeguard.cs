﻿using System;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Helpers.BadData
{
	public class AsyncDataSafeguard<TReturnResult> : DataSafeguard<Task<TReturnResult>>
	{
		protected AsyncDataSafeguard(string operationName) : base(operationName)
		{
		}

		public static new IDataSafeguard<Task<TReturnResult>> ForOperation(string operationName)
		{
			return new AsyncDataSafeguard<TReturnResult>(operationName);
		}

		public override async Task<TReturnResult> Get()
		{
			try
			{
				return await _correctDataProcessor();
			}
			catch (Exception ex)
			{
				if (!ShouldCatch(ex))
					throw;

				// else catch and report issue to the appinsights
				LogDataIssue(ex);
				if (_shouldThrow)
					ThrowBadDataException(ex);

				return await _badDataProcessor();
			}
		}
	}
}
