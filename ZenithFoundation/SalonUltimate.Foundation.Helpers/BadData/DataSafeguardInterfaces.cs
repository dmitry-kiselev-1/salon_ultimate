﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Helpers.BadData
{
	public interface IDataSafeguard<TReturnResult>
	{
		IDataSafeguardCorrectDataHandlerDefined<TReturnResult> Try(Func<TReturnResult> correctDataDelegate);
	}

	public interface IDataSafeguardCorrectDataHandlerDefined<TReturnResult>
	{
		IDataSafeguardBadDataHandlerDefined<TReturnResult> ForBadData(Func<TReturnResult> badDataDelegate);
		IDataSafeguardBadDataHandlerDefined<TReturnResult> ForBadDataThrow();
	}

	public interface IDataSafeguardBadDataHandlerDefined<TReturnResult>
	{
		IDataSafeguardReady<TReturnResult> Catch<TException>() where TException : Exception;
		IDataSafeguardReady<TReturnResult> Catch(params Type[] exceptionTypes);
	}

	public interface IDataSafeguardReady<TReturnResult> : IDataSafeguardBadDataHandlerDefined<TReturnResult>
	{
		TReturnResult Get();
	}
}
