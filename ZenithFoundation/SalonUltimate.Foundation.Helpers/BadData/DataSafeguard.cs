﻿using Microsoft.ApplicationInsights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Helpers.BadData
{
	public class DataSafeguard<TReturnResult> : IDataSafeguard<TReturnResult>,
												IDataSafeguardCorrectDataHandlerDefined<TReturnResult>,
												IDataSafeguardBadDataHandlerDefined<TReturnResult>,
												IDataSafeguardReady<TReturnResult>
	{
		protected readonly Lazy<TelemetryClient> _telemetryClient = new Lazy<TelemetryClient>(() => new TelemetryClient());

		protected string _operationName = null;
		protected Func<TReturnResult> _correctDataProcessor = () => default;
		protected Func<TReturnResult> _badDataProcessor = () => default;
		protected readonly List<Type> _catchingException = new List<Type>();
		protected bool _shouldThrow = false;

		protected DataSafeguard(string operationName)
		{
			_operationName = operationName;
		}

		public static IDataSafeguard<TReturnResult> ForOperation(string operationName)
		{
			return new DataSafeguard<TReturnResult>(operationName);
		}

		public IDataSafeguardCorrectDataHandlerDefined<TReturnResult> Try(Func<TReturnResult> correctDataDelegate)
		{
			_correctDataProcessor = correctDataDelegate;
			return this;
		}

		public IDataSafeguardBadDataHandlerDefined<TReturnResult> ForBadData(Func<TReturnResult> badDataDelegate)
		{
			_badDataProcessor = badDataDelegate;
			return this;
		}

		public IDataSafeguardBadDataHandlerDefined<TReturnResult> ForBadDataThrow()
		{
			_shouldThrow = true;
			return this;
		}

		public IDataSafeguardReady<TReturnResult> Catch<TException>() where TException : Exception
		{
			_catchingException.Add(typeof(TException));
			return this;
		}

		public IDataSafeguardReady<TReturnResult> Catch(params Type[] exceptionTypes)
		{
			if (!exceptionTypes.All(x => typeof(Exception).IsAssignableFrom(x)))
				throw new ArgumentException("You can only use exception types there");

			_catchingException.AddRange(exceptionTypes);
			return this;
		}

		public virtual TReturnResult Get()
		{
			try
			{
				return _correctDataProcessor();
			}
			catch(Exception ex)
			{
				if(!ShouldCatch(ex))
					throw;

				// else catch and report issue to the appinsights
				LogDataIssue(ex);
				if (_shouldThrow)
					ThrowBadDataException(ex);

				return _badDataProcessor();
			}
		}

		protected void LogDataIssue(Exception ex)
		{
			_telemetryClient.Value.TrackEvent("ZenithBadDataSafeguard", new Dictionary<string, string>
			{
				{ "DataOperationName", _operationName },
				{ "UnderlyingExceptionMessage", ex.Message },
				{ "UnderlyingExceptionType", ex.GetType().Name }
			});
		}

		protected bool ShouldCatch(Exception ex)
		{
			var exceptionType = ex.GetType();
			return _catchingException.Any(x => x.IsAssignableFrom(exceptionType));
		}

		protected void ThrowBadDataException(Exception sourceException)
		{
			throw new ZenithBadDataException(_operationName, sourceException);
		}
	}
}
