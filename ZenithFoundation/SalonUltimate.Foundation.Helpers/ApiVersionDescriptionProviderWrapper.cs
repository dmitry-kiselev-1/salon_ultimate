﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;

namespace SalonUltimate.Foundation.Helpers
{
    public class ApiVersionDescriptionProviderWrapper
	{
		public IApiVersionDescriptionProvider ApiVersionDescriptionProvider { get; set; }
		public ApiVersion DefaultApiVersion { get; set; }
    }
}
