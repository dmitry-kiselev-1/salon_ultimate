﻿namespace SalonUltimate.Foundation.Helpers.Paginator
{
	public enum SortOrders
	{
		Asc = 1,
		Desc = 2,
	}
}
