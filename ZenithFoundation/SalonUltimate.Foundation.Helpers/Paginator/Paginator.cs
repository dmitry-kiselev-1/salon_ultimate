﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Helpers.Paginator
{
    public class Paginator
	{
		#region Constants

		protected const int DefaultSkip = 0;
		protected const int DefaultMaxCount = 25;
		protected const SortOrders DefaultSortOrder = SortOrders.Asc;
		protected const string DefaultSortField = "Id";

		#endregion

		#region Pagination params

		public int? Skip { get; set; }
		public int? MaxCount { get; set; }
		public bool? DisableTotal { get; set; }
		public SortOrders? SortOrder { get; set; }
		public string SortField { get; set; }

		#endregion
		
		protected int? total;

		#region Getters of Pagination params

		public int? GetTotal() => total; // Swagger shows public property, even if it is 'protected set', as a required parameter
		public int GetSkip() => Skip ?? DefaultSkip;
		public int GetMaxCount() => MaxCount ?? DefaultMaxCount;
		public SortOrders GetSortOrder() => SortOrder ?? DefaultSortOrder;
		public string GetSortField() => GetSortFields().First();
		public string[] GetSortFields()
		{
			var result = SortField?.Split(',', ' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
			return result ?? new string[] { };
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Applies paging to query. Has side-effect! (on updating this.total which is total number of rows that query can return without paging)
		/// </summary>
		public async Task<IQueryable<TModel>> ApplyPagination<TModel>(IQueryable<TModel> query, CancellationToken ct = default(CancellationToken))
		{
			SortOrders sortOrder = GetSortOrder();
			if (DisableTotal != true)
			{
				total = await query.CountAsync(ct);
			}

			var orderByCommand = default(string);
			if (query.Expression.Type == typeof(IOrderedQueryable<TModel>))
			{
				orderByCommand = sortOrder == SortOrders.Asc ? "ThenBy" : "ThenByDescending";
			}
			else
			{
				orderByCommand = sortOrder == SortOrders.Asc ? "OrderBy" : "OrderByDescending";
			}

			var fields = GetSortFields();
			var type = typeof(TModel);

			// If sort fields collection is empty - use DefaultSortField if it exists in model.
			if (!fields.Any())
			{
				if (type.GetProperty(DefaultSortField) != null)
				{
					fields = new[] {DefaultSortField};
				}
			}

			var orderedQuery = query;
			foreach (var field in fields)
			{
				if (ct.IsCancellationRequested)
					return query;

				var property = type.GetProperty(field);
				if (property == null)
				{
					throw new ZenithInvalidOperationException($"Sorting field with name: '{field}' not found.");
				}
				var parameter = Expression.Parameter(type, "p");
				var propertyAccess = Expression.MakeMemberAccess(parameter, property);
				var orderByExpression = Expression.Lambda(propertyAccess, parameter);
				var resultExpression = Expression.Call(typeof(Queryable), orderByCommand, new Type[] { type, property.PropertyType },
					orderedQuery.Expression, Expression.Quote(orderByExpression));

				orderByCommand = sortOrder == SortOrders.Asc ? "ThenBy" : "ThenByDescending"; // second and all further orderings must be ThenBy

				orderedQuery = orderedQuery.Provider.CreateQuery<TModel>(resultExpression);
			}

			// Skip and Take must be called after order by expression, otherwise it won't work.
			var skip = GetSkip();
			var maxCount = GetMaxCount();
			orderedQuery = orderedQuery.Skip(skip).Take(maxCount);

			return orderedQuery;
		}

		protected MethodCallExpression OrderQueryable<TModel>(IQueryable<TModel> query, SortOrders sortOrder, LambdaExpression selectorExpression)
		{
			MethodCallExpression thenByCallExpression = Expression.Call(
				typeof(Queryable),
				sortOrder == SortOrders.Asc ? "ThenBy" : "ThenByDescending",
				new Type[] { query.ElementType, selectorExpression.ReturnType },
				query.Expression,
				selectorExpression
			);
			return thenByCallExpression;
		}

		protected IEnumerable<LambdaExpression> GetSortFieldSelectors<TModel>()
		{
			var fields = GetSortFields();
			foreach (var field in fields)
			{
				yield return GetSortFieldSelector<TModel>(field);
			}
		}

		protected LambdaExpression GetSortFieldSelector<TModel>(string fieldName)
		{
			var parameterExpression = Expression.Parameter(typeof(TModel));
			MemberExpression property;
			try
			{
				property = Expression.Property(parameterExpression, fieldName);
			}
			catch
			{
				property = Expression.Property(parameterExpression, DefaultSortField);
			}
			return Expression.Lambda(property, parameterExpression);
		}

		#endregion
	}
}
