﻿using System;
using System.Linq;
using System.Linq.Expressions;
using SalonUltimate.Foundation.Enums;
//using SalonUltimate.Foundation.Extensions;

namespace SalonUltimate.Foundation.Helpers.Paginator
{
	public class StringFilter : FilterVM<string>
	{
		#region Public methods

		public static StringFilter Empty => new StringFilter {FilterRule = FilterRules.None};
		public static StringFilter EmptyWithMaxPageSize => new StringFilter {FilterRule = FilterRules.None, MaxCount = int.MaxValue};

		public override string GetFilterValue() => FilterValue ?? string.Empty;

		public override Expression<Func<TModel, bool>> GetPredicate<TModel>(Expression<Func<TModel, string>> fieldSelector)
		{
			var pe = Expression.Parameter(typeof(TModel));

			if (GetFilterRule() == FilterRules.None)
				return GetTrueExpression<TModel>(pe);

			var filterExpression = GetComparisionExpression(pe, GetFilterValue(), fieldSelector);
			return Expression.Lambda<Func<TModel, bool>>(filterExpression, pe);
		}

		public override Expression<Func<TModel, bool>> GetPredicate<TModel>(BinaryOperation combinationRule, params Expression<Func<TModel, string>>[] fieldSelectors)
		{
			var pe = Expression.Parameter(typeof(TModel));

			if (GetFilterRule() == FilterRules.None)
				return GetTrueExpression<TModel>(pe);

			var expressions = fieldSelectors.Select<Expression<Func<TModel, string>>, Expression>(x => GetComparisionExpression(pe, GetFilterValue(), x));
			var result = CombineExpressions(combinationRule, expressions);

			return Expression.Lambda<Func<TModel, bool>>(result, pe);
		}

		public override Expression<Func<TModel, bool>> GetSplittedPredicate<TModel>(params Expression<Func<TModel, string>>[] fieldSelectors)
		{
			var pe = Expression.Parameter(typeof(TModel));

			if (GetFilterRule() == FilterRules.None)
				return GetTrueExpression<TModel>(pe);

			var filterValues = GetFilterValue()
				.Split(' ')
				.Where(x => !string.IsNullOrWhiteSpace(x))
				.Select(x => x.Trim())
				.ToList();

			var expressionsToAnd = filterValues.Select(filterValue =>
			{
				var expressionsToOr = fieldSelectors
					.Select(fieldSelector => GetComparisionExpression(pe, filterValue, fieldSelector))
					.ToList();

				return CombineExpressions(BinaryOperation.Or, expressionsToOr);
			}).ToList();

			var finalExpression = CombineExpressions(BinaryOperation.And, expressionsToAnd);

			return Expression.Lambda<Func<TModel, bool>>(finalExpression, pe);
		}

		#endregion

		#region Protected methods

		protected Expression GetComparisionExpression<TModel>(ParameterExpression pe, string filterString, Expression<Func<TModel, string>> fieldSelector)
		{
			var filterValue = Expression.Constant(filterString.ToLower());
			// Use just bare comparison instead of adding COALESCE/TOLOWER which kills indexes. Speedup up to 2x times
			var compValueGetter = Expression.Invoke(fieldSelector, pe); //GetFilterGetterExpression(pe, fieldSelector);
			return GetFilterAppliedToString(compValueGetter, filterValue);
		}

		protected MethodCallExpression GetFilterGetterExpression<TModel>(ParameterExpression pe, Expression<Func<TModel, string>> fieldSelector)
		{
			var compValueGetter = Expression.Call(Expression.Coalesce(Expression.Invoke(fieldSelector, pe), Expression.Constant("")), typeof(string).GetMethod("ToLower", Type.EmptyTypes));
			return compValueGetter;
		}

		protected Expression GetFilterAppliedToString(Expression compValueGetter, Expression filterValue)
		{
			switch (GetFilterRule())
			{
				case FilterRules.Equals:
					return Expression.Equal(filterValue, compValueGetter);
				case FilterRules.StartsWith:
					return Expression.Call(compValueGetter, typeof(string).GetMethod("StartsWith", new[] { typeof(string) }), filterValue);
				case FilterRules.EndsWith:
					return Expression.Call(compValueGetter, typeof(string).GetMethod("EndsWith", new[] { typeof(string) }), filterValue);
				case FilterRules.Contains:
					return Expression.Call(compValueGetter, typeof(string).GetMethod("Contains", new[] { typeof(string) }), filterValue);
				case FilterRules.None:
				default:
					return Expression.Constant(true);
			}
		}

		#endregion

	}
}
