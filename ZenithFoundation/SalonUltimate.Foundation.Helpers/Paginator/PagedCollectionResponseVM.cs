﻿using System;
using System.Collections.Generic;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Helpers.Paginator
{
	public class PagedCollectionResponseVm<T> : CollectionResponseVM<T>
	{
		public PagedCollectionResponseVm() : base() { }
		public PagedCollectionResponseVm(ResponseTypes result, string userMessage) : base(result, userMessage) { }
		public PagedCollectionResponseVm(Exception exception, string userMessage = null) : base(exception, userMessage) { }
		public PagedCollectionResponseVm(IEnumerable<T> data) : base(data) { }

		public PagedCollectionResponseVm(IEnumerable<T> data, Paginator paginator)
			: this(data)
		{
			Total = paginator.GetTotal();
			Skip = paginator.GetSkip();
			PageSize = paginator.GetMaxCount();
		}

		public override int? Total { get; set; }
		public int Skip { get; set; }
		public int PageSize { get; set; }
	}
}
