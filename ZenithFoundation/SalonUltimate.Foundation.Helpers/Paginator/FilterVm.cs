﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.Helpers.Paginator
{
	public abstract class FilterVM<T> : Paginator
	{
		#region Filter Parameters

		public FilterRules? FilterRule { get; set; }
		public virtual T FilterValue { get; set; }

		#endregion

		#region Filter Parameter Getters
		// Swagger shows all properties, even private, as required parameters, so we need methods here

		public FilterRules GetFilterRule() => FilterRule ?? FilterRules.None;
		public virtual T GetFilterValue() => FilterValue;

		#endregion

		#region Filtration Logic

		public abstract Expression<Func<TModel, bool>> GetPredicate<TModel>(Expression<Func<TModel, T>> fieldSelector);
		public abstract Expression<Func<TModel, bool>> GetPredicate<TModel>(BinaryOperation combinationRule, params Expression<Func<TModel, T>>[] fieldSelectors);
		public abstract Expression<Func<TModel, bool>> GetSplittedPredicate<TModel>(params Expression<Func<TModel, T>>[] fieldSelectors);

		#region Protected methods

		protected Expression GetSeed(BinaryOperation combinationRule)
		{
			switch(combinationRule)
			{
				case BinaryOperation.And:
					return Expression.Constant(true);
				default:
					return Expression.Constant(false);
			}
		}

		protected Expression CombineExpressions(BinaryOperation combinationRule, Expression left, Expression right)
		{
			switch(combinationRule)
			{
				case BinaryOperation.And:
					return Expression.AndAlso(left, right);
				default:
					return Expression.OrElse(left, right);
			}
		}

		protected Expression CombineExpressions(BinaryOperation operation, IEnumerable<Expression> expressions)
		{
			Expression result = GetSeed(operation);
			foreach (var ex in expressions)
				result = CombineExpressions(operation, result, ex);
			return result;
		}

		protected Expression<Func<TModel, bool>> GetTrueExpression<TModel>(ParameterExpression pe)
		{
			return Expression.Lambda<Func<TModel, bool>>(Expression.Constant(true), pe);
		}

		#endregion

		#endregion
	}
}
