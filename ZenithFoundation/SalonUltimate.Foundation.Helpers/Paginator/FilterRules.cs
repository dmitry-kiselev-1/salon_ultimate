﻿namespace SalonUltimate.Foundation.Helpers.Paginator
{
	public enum FilterRules
	{
		None = -1,
		Equals = 1,
		Contains,
		StartsWith,
		EndsWith
	}
}
