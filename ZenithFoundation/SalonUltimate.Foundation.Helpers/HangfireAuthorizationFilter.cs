﻿using Hangfire.Dashboard;

namespace SalonUltimate.Foundation.Helpers
{
	public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
	{
		private readonly string[] _roles;

		public HangfireAuthorizationFilter(params string[] roles)
		{
			_roles = roles;
		}

		public bool Authorize(DashboardContext context)
		{
			//Leaving this here for future usage of ASP.NET Core Identity.For now it's a dummy auth...

			//var httpContext = ((AspNetCoreDashboardContext)context).HttpContext;
			//var result = _roles.Aggregate(false, (current, role) => current || httpContext.User.IsInRole(role));

			//return result;
			return true;
		}
	}
}