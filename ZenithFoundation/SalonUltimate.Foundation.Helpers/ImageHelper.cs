﻿using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.Helpers
{
	public class ImageHelper
	{
		public const int DefaultUserpicSize = 100;
		public const int EmSize = 35;
		public const int TextPadding = 5;

		public Font DefaultFont => new Font(FontFamily.GenericSansSerif, EmSize);
		public Brush DefaultBrush => Brushes.White;
		public Brush DefaultBackgroundBrush => Brushes.Gray;

		public Image GenerateTwoLettersPhoto(string firstName, string lastName)
		{
			var bitmap = new Bitmap(DefaultUserpicSize, DefaultUserpicSize);
			var letters = (firstName?.TrimStart().First().ToString() ?? string.Empty) + (lastName?.TrimStart().First().ToString() ?? string.Empty);
			if (string.IsNullOrEmpty(letters))
				throw new ZenithValidationException("Cannot create userpic from empty input");

			using (var gr = Graphics.FromImage(bitmap))
			{
				StringFormat format = new StringFormat
				{
					LineAlignment = StringAlignment.Center,
					Alignment = StringAlignment.Center
				};
				gr.FillRectangle(DefaultBackgroundBrush, new Rectangle(0, 0, DefaultUserpicSize, DefaultUserpicSize));
				gr.DrawString(letters, DefaultFont, DefaultBrush, new RectangleF(TextPadding, TextPadding, DefaultUserpicSize - 2*TextPadding, DefaultUserpicSize - 2*TextPadding), format);
			}

			return bitmap;
		}

		public async Task<Image> GenerateTwoLettersPhotoAsync(string firstName, string lastName)
		{
			return await Task.Run(() => GenerateTwoLettersPhoto(firstName, lastName));
		}

		public async Task<Image> GetExternalImage(string imagePath, string imageName)
		{
			if (string.IsNullOrEmpty(imagePath) || string.IsNullOrEmpty(imageName))
				return null;

			try
			{
				using (var httpClient = new HttpClient())
				{
					var image = await httpClient.GetAsync($"{imagePath}/{imageName}");
					if (image.IsSuccessStatusCode)
					{
						using (var ms = new MemoryStream())
						{
							await image.Content.CopyToAsync(ms);
							return Image.FromStream(ms);
						}
					}
					return null;
				}
			}
			catch
			{
				return null;
			}
		}
	}
}

