﻿using System.Linq.Expressions;
using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.Helpers
{
	/// <summary>
	///   ExpressionHelper helps to join 2 Expressions with AND or OR operation
	///   for example if you have 2 lambdas: 
	///     first = x => x.Id > 100 
	///     second = x => x.Name == "John Doe"
	///   and you want to join them to one lambda, then you can not just make first.And(second)
	///   because you need to make both of them to the same parameter (x) 
	///   so you need walk recursively through the first (or second) expression and swap its param by param from another one expression
	/// </summary>
	public static class ExpressionHelper
	{
		/// <summary> Join 2 lambdas to just one by AND or OR operation </summary>
		/// <typeparam name="T"> expression type </typeparam>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <param name="binaryOperation"></param>
		/// <returns> Expression which contains both <paramref name="left"/> and <paramref name="right"/> Expressions </returns>
		public static Expression<T> Join<T>(Expression<T> left, Expression<T> right, BinaryOperation binaryOperation)
		{
			var swapper = new ParametersSwapper(left.Parameters[0], right.Parameters[0]);

			var binaryExp = Expression.OrElse(swapper.SwapParameters(left.Body), right.Body);
			if (binaryOperation == BinaryOperation.And)
				binaryExp = Expression.AndAlso(swapper.SwapParameters(left.Body), right.Body);

			return Expression.Lambda<T>(binaryExp, right.Parameters);
		}
	}

	/// <summary> Walks through the Expression tree and makes 'right' parameter to work on 'left' node (or otherwise) </summary>
	public class ParametersSwapper : ExpressionVisitor
	{
		private readonly Expression _leftParameter, _rightParameter;
		public ParametersSwapper(Expression leftParameter, Expression rightParameter)
		{
			_leftParameter = leftParameter;
			_rightParameter = rightParameter;
		}

		public Expression SwapParameters(Expression node)
		{
			return Visit(node);
		}

		public override Expression Visit(Expression node)
		{
			var newExpression = node == _leftParameter ? _rightParameter : base.Visit(node);
			return newExpression;
		}
	}
}
