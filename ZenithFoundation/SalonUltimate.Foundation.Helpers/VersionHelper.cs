﻿using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Helpers
{
	public class VersionHelper
	{
		private const string CommitVrsnFileName = "commit.txt";
		private const string BuiltAtFileName = "built_at.txt";

		private readonly string _commitVrsnFilePath;
		private readonly string _builtAtFilePath;
		private readonly AppInsightsMetrics _appInsightsMetrics;

		public VersionHelper(IHostingEnvironment hostingEnvironment, AppInsightsMetrics appInsightsMetrics)
		{
			_commitVrsnFilePath = hostingEnvironment.WebRootPath != null 
				? Path.Combine(hostingEnvironment.WebRootPath, CommitVrsnFileName) 
				: null;
			_builtAtFilePath = hostingEnvironment.WebRootPath != null
				? Path.Combine(hostingEnvironment.WebRootPath, BuiltAtFileName)
				: null;
			_appInsightsMetrics = appInsightsMetrics;
		}

		public string GetAssemblyVersion()
		{
			var runtimeVersion = typeof(VersionHelper)
					.GetTypeInfo()
					.Assembly
					.GetCustomAttribute<AssemblyFileVersionAttribute>();

			return runtimeVersion.Version;
		}

		public async Task<string> GetCommitVersion()
		{
			if (_commitVrsnFilePath != null && File.Exists(_commitVrsnFilePath))
			{
				var version = await File.ReadAllTextAsync(_commitVrsnFilePath);
				return version.Trim();
			}
			return null;
		}

		public async Task<string> GetBuiltAt()
		{
			if (_builtAtFilePath != null && File.Exists(_builtAtFilePath))
			{
				var version = await File.ReadAllTextAsync(_builtAtFilePath);
				return version.Trim();
			}
			return null;
		}

		public DeploymentInfo GetDeploymentInfo()
			=> new DeploymentInfo(_appInsightsMetrics);

		public async Task<Models.V1.ApiVersion> GetVersions()
		{
			return new Models.V1.ApiVersion
			{
				AssemblyVersion = GetAssemblyVersion(),
				CommitVersion = await GetCommitVersion(),
				BuiltAt = await GetBuiltAt()
			};
		}

		public async Task<ApiVersion> GetVersionsWithApiVersion()
		{
			return new ApiVersion
			{
				AssemblyVersion = GetAssemblyVersion(),
				CommitVersion = await GetCommitVersion(),
				BuiltAt = await GetBuiltAt(),
				Deployment = GetDeploymentInfo()
			};
		}
	}
}
