﻿using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Helpers
{
	public class TelemetryInitializer : ITelemetryInitializer
	{
		private readonly AppInsightsMetrics _appInsightsMetrics;

		public TelemetryInitializer(AppInsightsMetrics appInsightsMetrics)
		{
			_appInsightsMetrics = appInsightsMetrics;
		}

		/// <summary>
		/// Initializes properties of the specified 
		/// <see cref="T:Microsoft.ApplicationInsights.Channel.ITelemetry" /> object.
		/// </summary>
		/// <param name="telemetry">the telemetry channel</param>
		public void Initialize(ITelemetry telemetry)
		{
			var propTelemetry = (ISupportProperties)telemetry;

			propTelemetry.Properties["Application"] = _appInsightsMetrics.Application;
			propTelemetry.Properties["Version"] = _appInsightsMetrics.Version;
			propTelemetry.Properties["Environment"] = _appInsightsMetrics.Environment;

			if (_appInsightsMetrics.AdditionalMetrics != null)
			{
				foreach (var item in _appInsightsMetrics.AdditionalMetrics)
				{
					propTelemetry.Properties[item.Key] = item.Value;
				}
			}
		}
	}
}
