﻿using System;
using System.Threading.Tasks;
using Hangfire;

namespace SalonUltimate.Foundation.Hangfire
{
    public static class HangfireAttrs
    {
	    [AutomaticRetry(Attempts = 0)]
	    public static async Task<T> InvokeWithZeroRetries<T>(Func<Task<T>> func) => await func();

	    [PreventConcurrentExecution]
		[AutomaticRetry(Attempts = 0)]
	    public static void InvokeWithZeroRetriesAndDisableMultipleQueuedItems(Action func) => func();
    }
}
