﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.Storage;

namespace SalonUltimate.Foundation.Hangfire
{
	public class PreventConcurrentExecutionAttribute : JobFilterAttribute, IClientFilter, IServerFilter
	{
		private static readonly TimeSpan LockTimeout = TimeSpan.FromSeconds(5);
		private static readonly TimeSpan FingerprintTimeout = TimeSpan.FromMinutes(5);

		public void OnCreating(CreatingContext filterContext)
		{
			if (!AddFingerprintIfNotExists(filterContext.Connection, filterContext.Job))
			{
				filterContext.Canceled = true;
			}
		}

		public void OnPerformed(PerformedContext filterContext)
		{
			RemoveFingerprint(filterContext.Connection, filterContext.BackgroundJob.Job);
		}

		private static bool AddFingerprintIfNotExists(IStorageConnection connection, Job job)
		{
			try
			{
				using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
				{
					var fingerprint = connection.GetAllEntriesFromHash(GetFingerprintKey(job));

					if (fingerprint != null &&
						fingerprint.ContainsKey("Timestamp") &&
						DateTimeOffset.TryParse(fingerprint["Timestamp"], null, DateTimeStyles.RoundtripKind,
							out var timestamp) &&
						DateTimeOffset.UtcNow <= timestamp.Add(FingerprintTimeout))
					{
						// Actual fingerprint found, returning.
						return false;
					}

					// Fingerprint does not exist, it is invalid (no `Timestamp` key),
					// or it is not actual (timeout expired).
					connection.SetRangeInHash(GetFingerprintKey(job), new Dictionary<string, string>
					{
						{"Timestamp", DateTimeOffset.UtcNow.ToString("o")}
					});

					return true;
				}
			}
			catch
			{
				return false;
			}
		}

		private static void RemoveFingerprint(IStorageConnection connection, Job job)
		{
			using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
			{
				using (var transaction = connection.CreateWriteTransaction())
				{
					transaction.RemoveHash(GetFingerprintKey(job));
					transaction.Commit();
				}
			}
		}

		private static string GetFingerprintLockKey(Job job)
		{
			return string.Format("{0}:lock", GetFingerprintKey(job));
		}

		private static string GetFingerprintKey(Job job)
		{
			return string.Format("fingerprint:{0}", GetFingerprint(job));
		}

		private static string GetFingerprint(Job job)
		{
			var parameters = string.Empty;
			if (job.Args != null)
			{
				parameters = string.Join(".", job.Args);
			}
			if (job.Type == null || job.Method == null)
			{
				return string.Empty;
			}
			var fingerprint = String.Format(
				"{0}.{1}.{2}",
				job.Type.FullName,
				job.Method.Name,
				parameters);

			// We can use hash and reduce fingerprint length (important when we use DB storage)
			//var hash = SHA256.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes(payload));
			//var fingerprint = Convert.ToBase64String(hash);

			return fingerprint;
		}

		void IClientFilter.OnCreated(CreatedContext filterContext)
		{
		}

		void IServerFilter.OnPerforming(PerformingContext filterContext)
		{
		}
	}
}
