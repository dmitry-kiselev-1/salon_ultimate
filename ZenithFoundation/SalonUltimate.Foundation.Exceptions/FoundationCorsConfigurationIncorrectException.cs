﻿using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.Exceptions
{
	public class FoundationCorsConfigurationIncorrectException : ZenithBaseException
	{
		public FoundationCorsConfigurationIncorrectException(string userMessage) : base(userMessage) { }
		public FoundationCorsConfigurationIncorrectException(string userMessage, object debugInfo) : base(userMessage, debugInfo) { }
		public FoundationCorsConfigurationIncorrectException(string userMessage, string systemMessage, object debugInfo) : base(userMessage, systemMessage, debugInfo) { }

		public override ResponseTypes ResponseType => ResponseTypes.FailedValidation;
	}
}