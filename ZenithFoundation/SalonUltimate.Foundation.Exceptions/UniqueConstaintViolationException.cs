﻿using System;
using System.Runtime.Serialization;

namespace SalonUltimate.Foundation.Exceptions
{
	public class UniqueConstaintViolationException : Exception
	{
		protected UniqueConstaintViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		public UniqueConstaintViolationException(string message) : base(message)
		{
		}

		public UniqueConstaintViolationException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
