﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.Models.DbConflict;
using SalonUltimate.Foundation.Exceptions.Validation;

namespace SalonUltimate.Foundation.Exceptions
{
	public abstract class ZenithBaseException : Exception
	{
		public ZenithBaseException(string userMessage) : base(userMessage) { }

		public ZenithBaseException(string userMessage, Exception innerException) : base(userMessage, innerException) { }

		public ZenithBaseException(string userMessage, object debugInfo) : base(userMessage)
		{
			ZenithDebugInfo = debugInfo;
		}

		public ZenithBaseException(string userMessage, string systemMessage) : base(userMessage)
		{
			SystemMessage = systemMessage;
		}

		public ZenithBaseException(string userMessage, string systemMessage, object debugInfo) : this(userMessage, systemMessage)
		{
			ZenithDebugInfo = debugInfo;
		}

		public abstract ResponseTypes ResponseType { get; }
		public object ZenithDebugInfo { get; protected set; }
		public string SystemMessage { get; protected set; }
	}

	public class ZenithNotAuthenticatedException : ZenithBaseException
	{
		public ZenithNotAuthenticatedException(string userMessage) : base(userMessage) { }
		public override ResponseTypes ResponseType => ResponseTypes.NotAuthenticated;
	}

	public class ZenithAccessDeniedException : ZenithBaseException
	{
		public Dictionary<string, string> RequiredTasks { get; set; }

		public ZenithAccessDeniedException(string userMessage) : base(userMessage) { }
		public override ResponseTypes ResponseType => ResponseTypes.NoPermission;
	}

	public class ZenithNoPermissionException : ZenithBaseException
	{
		public ZenithNoPermissionException(string userMessage) : base(userMessage) { }
		public override ResponseTypes ResponseType => ResponseTypes.NoPermission;
	}

	public class ZenithValidationException : ZenithBaseException
	{
		public ZenithValidationException(string userMessage) : base(userMessage) { }
		public ZenithValidationException(string userMessage, object debugInfo) : base(userMessage, debugInfo) { }
		public ZenithValidationException(string userMessage, string systemMessage, object debugInfo) : base(userMessage, systemMessage, debugInfo) { }
		
		public ZenithValidationException(ValidationResultExceptionInfo validationResult) 
			: base(validationResult.AllIssuesToString, validationResult.AllSystemMessagesToString, validationResult.AllIssuesDebugInfo) { }

		public override ResponseTypes ResponseType => ResponseTypes.FailedValidation;
	}

	public class ZenithNotFoundException : ZenithBaseException
	{
		public ZenithNotFoundException(string userMessage) : base(userMessage) { }
		public override ResponseTypes ResponseType => ResponseTypes.NotFound;
	}

	public class ZenithExternalResourceException : ZenithBaseException
	{
		public ZenithExternalResourceException(string userMessage) : base(userMessage) { }
		public ZenithExternalResourceException(string userMessage, string systemMessage) : base(userMessage, systemMessage) { }
		public ZenithExternalResourceException(string userMessage, string systemMessage, object responseDebugInfo) : base(userMessage, systemMessage, responseDebugInfo) { }
		public ZenithExternalResourceException(string userMessage, string systemMessage, object responseDebugInfo, HttpRequestMessage requestData) : base(userMessage, responseDebugInfo)
		{
			SystemMessage = systemMessage;
			RequestData = requestData;
		}
		public override ResponseTypes ResponseType => ResponseTypes.Exception;

		public HttpRequestMessage RequestData { get; private set; }
	}

	public class ZenithDisabledFeatureException : ZenithBaseException
	{
		public ZenithDisabledFeatureException(string featureName) : base($"{featureName} feature is switched off.") { }
		public override ResponseTypes ResponseType => ResponseTypes.Exception;
	}

	public class ZenithInvalidOperationException : ZenithBaseException
	{
		public ZenithInvalidOperationException(string userMessage) : base(userMessage) { }
		public override ResponseTypes ResponseType => ResponseTypes.InvalidOperation;
	}

	public class ZenithBadDataException : ZenithBaseException
	{
		public ZenithBadDataException(string operationName, Exception sourceException) 
			: base("Bad data has been detected in the database", sourceException)
		{
			OperationName = operationName;
		}

		public string OperationName { get; protected set; }
		public override ResponseTypes ResponseType => ResponseTypes.BadData;
	}

	public class ZenithConcurrencyWritingException : ZenithBaseException
	{
		private readonly DbConcurrencyExceptionWrapper _exceptionWrapper;

		public ZenithConcurrencyWritingException(DbConcurrencyExceptionWrapper exceptionWrapper) : this("A conflict occured while trying to modify data.", exceptionWrapper)
		{
		}

		public ZenithConcurrencyWritingException(string userMessage, DbConcurrencyExceptionWrapper exceptionWrapper) : base(userMessage)
		{
			_exceptionWrapper = exceptionWrapper;
			SystemMessage = string.Join(", ", _exceptionWrapper.RowVersions);
		}

		public override ResponseTypes ResponseType => ResponseTypes.Conflict;
	}

}
