﻿using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.Exceptions
{
	public class FoundationCorsConfigurationMissedOrEmptyException : ZenithBaseException
	{
		public FoundationCorsConfigurationMissedOrEmptyException(string userMessage) : base(userMessage) { }
		public FoundationCorsConfigurationMissedOrEmptyException(string userMessage, object debugInfo) : base(userMessage, debugInfo) { }
		public FoundationCorsConfigurationMissedOrEmptyException(string userMessage, string systemMessage, object debugInfo) : base(userMessage, systemMessage, debugInfo) { }

		public override ResponseTypes ResponseType => ResponseTypes.FailedValidation;
	}
}