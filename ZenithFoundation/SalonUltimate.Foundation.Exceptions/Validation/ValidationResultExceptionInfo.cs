﻿using System;
using System.Collections.Generic;
using System.Linq;
using SalonUltimate.Foundation.Interfaces.Validation;

namespace SalonUltimate.Foundation.Exceptions.Validation
{
	public class ValidationResultExceptionInfo
	{

		public List<ValidationIssueExceptionInfo> Issues { get => _issues ?? (_issues = new List<ValidationIssueExceptionInfo>()); set => _issues = value; }
		public bool IsValid => !Errors.Any();

		#region Protected properties

		protected IEnumerable<ValidationIssueExceptionInfo> Errors => Issues?.Where(x => !x.CanBeSkipped) ?? Enumerable.Empty<ValidationIssueExceptionInfo>();
		protected IEnumerable<ValidationIssueExceptionInfo> Warnings => Issues?.Where(x => x.CanBeSkipped) ?? Enumerable.Empty<ValidationIssueExceptionInfo>();
		protected List<ValidationIssueExceptionInfo> _issues;

		#endregion

		#region Public properties

		public string ErrorsToString => Errors.Any()
			? $"Errors:{Environment.NewLine}{string.Join(Environment.NewLine, Errors.Select(x => x.Description))}"
			: string.Empty;

		public string WarningsToStrings => Warnings.Any()
			? $"Warnings:{Environment.NewLine}{string.Join(Environment.NewLine, Warnings.Select(x => x.Description))}"
			: string.Empty;

		public string AllIssuesToString => (ErrorsToString + Environment.NewLine + WarningsToStrings).Trim();



		public string SystemErrorsToString => Errors.Any()
			? $"Errors:{Environment.NewLine}{string.Join(Environment.NewLine, Errors.Select(x => x.SystemMessage))}"
			: string.Empty;

		public string SystemWarningsToStrings => Warnings.Any()
			? $"Warnings:{Environment.NewLine}{string.Join(Environment.NewLine, Warnings.Select(x => x.SystemMessage))}"
			: string.Empty;

		public string AllSystemMessagesToString => (SystemErrorsToString + Environment.NewLine + SystemWarningsToStrings).Trim();



		public List<ValidationSystemInfoExceptionInfo> ErrorsDebugInfo => Errors.Any()
			? Errors.Select(x => new ValidationSystemInfoExceptionInfo(x.Description, x.SystemInfo)).ToList()
			: null;

		public List<ValidationSystemInfoExceptionInfo> WarningsDebugInfo => Warnings.Any()
			? Warnings.Select(x => new ValidationSystemInfoExceptionInfo(x.Description, x.SystemInfo)).ToList()
			: null;

		public List<ValidationSystemInfoExceptionInfo> AllIssuesDebugInfo => 
			Issues.Select(x => new ValidationSystemInfoExceptionInfo(x.Description, x.SystemInfo)).ToList();

		#endregion

		#region Constructors

		public ValidationResultExceptionInfo(string message, bool canBeSkipped, object conflictData = null, object systemInfo = null) : this(message, message, canBeSkipped, conflictData, systemInfo)
		{
		}

		public ValidationResultExceptionInfo(string message, string systemMessage, bool canBeSkipped, object conflictData = null, object systemInfo = null) : base()
		{
			AddIssue(message, systemMessage, canBeSkipped, conflictData, systemInfo);
		}

		public ValidationResultExceptionInfo()
		{
			Issues = new List<ValidationIssueExceptionInfo>();
		}

		#endregion

		#region Public methods

		public void AddResult(ValidationResultExceptionInfo result)
		{
			if (result.Issues == null)
				return;

			if (Issues == null)
				Issues = result.Issues;
			else
				Issues.AddRange(result.Issues);
		}

		public void AddError(string errorMessage, object conflictData = null, object systemInfo = null)
		{
			AddError(errorMessage, errorMessage, conflictData, systemInfo);
		}

		public void AddError(string errorMessage, string systemMessage, object conflictData = null, object systemInfo = null)
		{
			AddIssue(errorMessage, systemMessage, false, conflictData, systemInfo);
		}

		public void AddWarning(string warningMessage, object conflictData = null, object systemInfo = null)
		{
			AddWarning(warningMessage, warningMessage, conflictData, systemInfo);
		}

		public void AddWarning(string warningMessage, string systemMessage, object conflictData = null, object systemInfo = null)
		{
			AddIssue(warningMessage, systemMessage, true, conflictData, systemInfo);
		}

		public void ThrowIfNotValid()
		{
			if (!this.IsValid)
				throw new ZenithValidationException(this);
		}

		#endregion

		protected void AddIssue(string issueMessage, string systemMessage, bool canBeSkipped, object conflictData = null, object systemInfo = null)
		{
			if (Issues == null)
				Issues = new List<ValidationIssueExceptionInfo>();

			if (systemInfo == null && conflictData != null)
				systemInfo = conflictData;

				Issues.Add(new ValidationIssueExceptionInfo(issueMessage, systemMessage, canBeSkipped, conflictData as IConflictData, systemInfo));
		}
	}
}
