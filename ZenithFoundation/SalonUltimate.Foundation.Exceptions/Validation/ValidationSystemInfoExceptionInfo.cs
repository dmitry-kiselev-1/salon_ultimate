﻿namespace SalonUltimate.Foundation.Exceptions.Validation
{
	public class ValidationSystemInfoExceptionInfo
	{
		public ValidationSystemInfoExceptionInfo(string message, object info)
		{
			UserMessage = message;
			SystemInfo = info;
		}

		public string UserMessage { get; set; }
		public object SystemInfo { get; set; }

		public override string ToString()
		{
			return UserMessage;
		}
	}
}
