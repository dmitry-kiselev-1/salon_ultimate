﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.Services.Interfaces;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Controllers
{
	[ApiController]
	[ApiVersionNeutral]
	[Route("api/v{version:apiVersion}/[controller]")]
	public class HealthCheckController : BaseController
	{
		private readonly IHealthCheckService _healthCheckService;

		public HealthCheckController(IHealthCheckService healthCheckService)
		{
			_healthCheckService = healthCheckService ?? throw new InvalidOperationException("HealthCheckService is not implemented");
		}

		[HttpGet("warm-up-db")]
		public async Task<ActionResult<BaseResponseVM>> WarmUpDbContext()
		{
			await _healthCheckService.WarmUpDbContext();

			return ResponseTyped(new BaseResponseVM(ResponseTypes.Success, "Warmed Up"));
		}

		[HttpGet("is-up")]
		public ActionResult<BaseResponseVM> IsUp() => ResponseTyped(new BaseResponseVM(ResponseTypes.Success, "API Up"));
	}
}
