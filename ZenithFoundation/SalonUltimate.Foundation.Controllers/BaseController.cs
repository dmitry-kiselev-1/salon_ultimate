﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Controllers
{
	public abstract class BaseController : Controller
	{
		protected void ValidateIds(params int[] ids)
		{
			foreach (var id in ids)
			{
				if (id <= 0)
					throw new ZenithValidationException("Cannot process empty request");
			}
		}

		protected void ValidatePayload(object payload)
		{
			if (payload == null)
				throw new ZenithValidationException("Cannot process empty request");
		}

		protected IActionResult Resp(MediaFileVM mediaFile)
		{
			return File(mediaFile.Content, mediaFile.ContentType);
		}

		[Obsolete("Please use instead ResponseTyped with generic ActionResult")]
		protected IActionResult Resp(BaseResponseVM response) => RespInner(response);

		[Obsolete("Please use instead RespInnerTyped with generic ActionResult")]
		protected IActionResult RespInner(BaseResponseVM responseVM)
		{
			var logger = ZenithLogger.CreateLogger();
			if (logger.IsEnabled(LogLevel.Debug))
			{
				var json = JsonConvert.SerializeObject(responseVM);
				logger.LogDebug(json);
			}

			return StatusCode(StatusCodeHelper.GetIntStatusCodeForResponse(responseVM), responseVM);
		}

		protected ActionResult<T> ResponseTyped<T>(T response) where T : BaseResponseVM
		{
			return this.RespInnerTyped(response);
		}

		protected ActionResult<T> RespInnerTyped<T>(T responseVM) where T : BaseResponseVM
		{
			var logger = ZenithLogger.CreateLogger();
			if (logger.IsEnabled(LogLevel.Debug))
			{
				string message = JsonConvert.SerializeObject((object)responseVM);
				logger.LogDebug(message);
			}
			return this.StatusCode(StatusCodeHelper.GetIntStatusCodeForResponse(responseVM), (object)responseVM);
		}
	}
}
