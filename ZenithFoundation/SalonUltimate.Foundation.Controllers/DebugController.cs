﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Controllers
{
	[ApiVersionNeutral]
	[ApiController]
	[Route("api/v{version:apiVersion}/[controller]/[action]")]
	public class DebugController : BaseController
	{
		private readonly IConfiguration _cfg;

		public DebugController(IConfiguration cfg)
		{
			_cfg = cfg;
		}

		[HttpGet]
		public IActionResult Headers() => Json(Request.Headers);

		/// <summary>
		/// Returns all appsettings*.json and IConfiguration content based on AllowViewApplicationConfigs config in format:
		/// {
		///		appsettingsFiles: { "appsettings.json": { ... }, "appsettings.local.json": { ... } },
		///		iConfiguration: [ {"key" : "", "value": ""} ]
		/// }
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetConfig()
		{
			if (!_cfg.GetValue<bool>("AllowViewApplicationConfigs"))
				return NotFound();

			var res = new Dictionary<string, object>();
			foreach (var f in Directory.GetFiles(Directory.GetCurrentDirectory(), "appsettings*.json"))
				res.Add(f, JsonConvert.DeserializeObject(System.IO.File.ReadAllText(f)));

			return Ok(new { AppsettingsFiles = res, IConfiguration = _cfg.AsEnumerable() });
		}
	}
}
