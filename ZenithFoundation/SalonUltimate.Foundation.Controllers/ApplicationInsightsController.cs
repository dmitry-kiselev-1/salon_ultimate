﻿using Microsoft.AspNetCore.Mvc;
using SalonUltimate.Foundation.Services.Interfaces;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Controllers
{
	[ApiController]
	[ApiVersionNeutral]
	[Route("api/v{version:apiVersion}/[controller]/[action]")]
	public class ApplicationInsightsController : BaseController
	{
		private readonly IApplicationInsightsService _applicationInsightsService;

		public ApplicationInsightsController(IApplicationInsightsService applicationInsightsService)
		{
			_applicationInsightsService = applicationInsightsService;
		}
		
		[HttpPost]
		public ActionResult<BaseResponseVM> Event([FromBody] ApplicationInsightsCustomEventVM model)
		{
			_applicationInsightsService.TrackEvent(model);

			return ResponseTyped(new BaseResponseVM());
		}
		
		[HttpPost]
		public ActionResult<BaseResponseVM> Flush()
		{
			_applicationInsightsService.Flush();

			return ResponseTyped(new BaseResponseVM());
		}
	}
}
