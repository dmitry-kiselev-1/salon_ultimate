﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Options;
using SalonUltimate.Foundation.FileSystem.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.FileSystem.Storages
{
	/// <summary>
	/// Amazon S3 file storage implementation.
	/// </summary>
	public class S3FileStorage : IFileStorage
	{
		private const string FileUrlTemplate = "https://{0}.s3.{1}.amazonaws.com/{2}";

		private readonly IAmazonS3 _awsClient;
		private readonly S3FileStorageOptions _options;

		/// <summary>
		/// Initializes a new instance of <see cref="S3FileStorage"/>.
		/// </summary>
		/// <param name="optionsAccessor">S3 file storage options.</param>
		public S3FileStorage(IOptions<S3FileStorageOptions> optionsAccessor)
		{
			_options = optionsAccessor.Value;
			var awsCredentials = new BasicAWSCredentials(_options.AwsKey, _options.AwsToken);
			var awsRegion = RegionEndpoint.GetBySystemName(_options.AwsRegion);
			_awsClient = new AmazonS3Client(awsCredentials, awsRegion);
		}

		/// <inheritdoc />
		public async Task UploadAsync(Stream stream, string subpath,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var uploadStreamBufferSize = 1024 * 16;
			BufferedStream bufferedStream = new BufferedStream(stream, uploadStreamBufferSize);

			var putObjectRequest = new PutObjectRequest
			{
				BucketName = _options.BucketName,
				InputStream = bufferedStream,
				Key = subpath,
				CannedACL = S3CannedACL.PublicRead
			};

			await _awsClient.PutObjectAsync(putObjectRequest, cancellationToken);
		}

		/// <inheritdoc />
		public async Task<Stream> CreateReadStreamAsync(string subpath,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var downloadStreamBufferSize = 1024 * 16;
			var objectResponse = await _awsClient.GetObjectAsync(_options.BucketName, subpath, cancellationToken);
			BufferedStream bufferStream = new BufferedStream(objectResponse.ResponseStream, downloadStreamBufferSize);
			return bufferStream;
		}

		public async Task DeleteAsync(string subpath, CancellationToken cancellationToken = default(CancellationToken))
		{
			await _awsClient.DeleteObjectAsync(_options.BucketName, subpath, cancellationToken);
		}

		public string BuildFileUrl(string subpath)
		{
			return string.Format(FileUrlTemplate, _options.BucketName, _options.AwsRegion, subpath);
		}
	}
}
