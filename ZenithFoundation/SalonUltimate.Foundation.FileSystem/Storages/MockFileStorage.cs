﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.FileSystem.Storages
{
	public class MockFileStorage : IFileStorage
	{
		public string BuildFileUrl(string subpath)
		{
			throw new NotImplementedException("Please configure the storage");
		}

		public Task<Stream> CreateReadStreamAsync(string subpath, CancellationToken cancellationToken = default(CancellationToken))
		{
			throw new NotImplementedException("Please configure the storage");
		}

		public Task DeleteAsync(string subpath, CancellationToken cancellationToken = default(CancellationToken))
		{
			throw new NotImplementedException("Please configure the storage");
		}

		public Task UploadAsync(Stream stream, string subpath, CancellationToken cancellationToken = default(CancellationToken))
		{
			throw new NotImplementedException("Please configure the storage");
		}
	}
}
