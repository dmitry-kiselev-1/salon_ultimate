﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SalonUltimate.Foundation.FileSystem
{
	/// <summary>
	/// Represents a file storage.
	/// </summary>
	public interface IFileStorage
	{
		/// <summary>
		/// Uploads the file to the storage.
		/// </summary>
		/// <param name="stream">Stream for upload, content of the file will be read from this stream.</param>
		/// <param name="subpath">Virtual FS relative file path.</param>
		/// <param name="cancellationToken"> A <see cref="CancellationToken"/> to propagate a notification
		/// that asynchronous operation should be cancelled. </param>
		/// <returns>A task that represents the asynchronous operation.</returns>
		Task UploadAsync(Stream stream, string subpath,
			CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Creates <see cref="Stream"/> for reading the file.
		/// </summary>
		/// <param name="subpath">Virtual FS relative file path.</param>
		/// <param name="cancellationToken"> A <see cref="CancellationToken"/> to propagate a notification
		/// that asynchronous operation should be cancelled. </param>
		/// <returns>A <see cref="Task"/> that on completion provides the created stream./></returns>
		Task<Stream> CreateReadStreamAsync(string subpath,
			CancellationToken cancellationToken = default(CancellationToken));

		Task DeleteAsync(string subpath, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Builds a full-qualified URI of the file to be accessed outside.
		/// Can be a direct link to AWS, BB, NW or whatever. This address must return the file.
		/// </summary>
		/// <param name="subpath"></param>
		/// <returns></returns>
		string BuildFileUrl(string subpath);
	}
}
