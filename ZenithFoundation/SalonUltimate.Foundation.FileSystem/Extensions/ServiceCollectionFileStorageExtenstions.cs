﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.FileSystem.Options;
using SalonUltimate.Foundation.FileSystem.Storages;

namespace SalonUltimate.Foundation.FileSystem.Extensions
{
	public static class ServiceCollectionFileStorageExtenstions
	{
		/// <summary>
		/// Configures dependencies and options for file storage, based on FileStorage and AWSS3 setting sections
		/// </summary>
		public static IServiceCollection AddFileStorage(this IServiceCollection services, IConfiguration configuration)
		{
			services.Configure<S3FileStorageOptions>(configuration.GetSection("AWSS3"));

			if (Enum.TryParse<FileStorageTypes>(configuration["FileStorage"], out var fileStorageType))
			{
				switch (fileStorageType)
				{
					case FileStorageTypes.Local:
						services.AddSingleton<IFileStorage, MockFileStorage>(); // to be replaced with future LocalStorage class
						break;
					case FileStorageTypes.AWSS3:
						services.AddSingleton<IFileStorage, S3FileStorage>();
						break;
					case FileStorageTypes.Disabled:
					default:
						services.AddSingleton<IFileStorage, MockFileStorage>();
						break;
				}

				return services;
			}
			else
				throw new InvalidOperationException("FileStorage configuration option must be configured");
		}
	}
}
