﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.FileSystem.Options
{
	public enum FileStorageTypes
	{
		Disabled,
		Local,
		AWSS3
	}
}
