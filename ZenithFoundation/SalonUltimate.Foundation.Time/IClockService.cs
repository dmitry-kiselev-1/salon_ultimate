﻿using System;

namespace SalonUltimate.Foundation.Time
{
	//TODO: it looks not used (and ClockProvider also)
	public interface IClockService
	{
		DateTime UtcNow { get; }
	}
}
