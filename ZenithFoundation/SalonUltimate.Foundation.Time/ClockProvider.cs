﻿using System;

namespace SalonUltimate.Foundation.Time
{
	/// <summary>
	/// The ClockProvider must always be used instead of using DateTime.UtcNow, DateTime.Now, and DateTime.Today
	/// The UtcNow property can be accessed like ClockProvider.UtcNow, while the other two either require passing a TimeZoneInfo to GetNow() or GetToday()
	/// or instantiating a ClockProvider instance with the desired conversion time zone. This is because the local system timezone is almost always useless.
	/// </summary>
	public class ClockProvider
	{
		private TimeZoneInfo _timeZone;
		public ClockProvider(TimeZoneInfo timeZone)
		{
			_timeZone = timeZone;
		}

		/// <summary>
		/// Gets a DateTime that is set to the current date and time on this
		/// computer, expressed in the time zone with which this ClockProvider was constructed.
		/// </summary>
		public DateTime Now => GetNow(_timeZone);


		/// <summary>
		/// Gets a DateTime that is set to the current date on this
		/// computer, expressed in the time zone with which this ClockProvider was constructed.
		/// </summary>
		public DateTime Today => GetToday(_timeZone);

		/// <summary>
		/// Gets a TimeSpan that represents the current time-of-day on this computer,
		/// expressed in the time zone with which this ClockProvider was constructed.
		/// </summary>
		public TimeSpan TimeOfDay => GetTimeOfDay(_timeZone);

		private static IClockService _clockService = null;

		/// <summary>
		/// Gets a DateTime that is set to the current date and time on this
		/// computer, expressed as the Coordinated Universal Time (UTC).
		/// </summary>
		public static DateTime UtcNow
		{
			get
			{
#if DEBUG
				return (_clockService != null) ? _clockService.UtcNow : DateTime.UtcNow;
#else
				return DateTime.UtcNow;
#endif
			}
		}

		/// <summary>
		/// Gets a DateTime that is set to the current date and time on this
		/// computer, expressed in the time zone that is passed to this method.
		/// </summary>
		/// <param name="timeZone">The time zone to which the UTC time will be converted.</param>
		public static DateTime GetNow(TimeZoneInfo timeZone) => TimeZoneInfo.ConvertTimeFromUtc(UtcNow, timeZone);

		/// <summary>
		/// Gets a DateTime that is set to the current date on this
		/// computer, expressed in the time zone that is passed to this method.
		/// </summary>
		/// <param name="timeZone">The time zone to which the UTC time will be converted.</param>
		public static DateTime GetToday(TimeZoneInfo timeZone) => GetNow(timeZone).Date;

		/// <summary>
		/// Gets a TimeSpan that represents the current time-of-day on this computer,
		/// expressed in the time zone with that is passed to this method.
		/// </summary>
		public static TimeSpan GetTimeOfDay(TimeZoneInfo timeZone)
		{
			var now = GetNow(timeZone);
			return now - now.Date;
		}

		/// <summary>
		/// This method can be used to override the default system clock with a custom service
		/// that provides date and time information in whichever way is suitable for the tests.
		/// 
		/// WARNING - do not call in production code, it will happily throw an exception in this case.
		/// </summary>
		/// <param name="clockService">The clock service which will be used as a clock source for all the methods and properties of the ClockProvider.</param>
		public static void InitializeClockService(IClockService clockService)
		{
#if DEBUG
			_clockService = clockService;
#else
			throw new NotSupportedException("The clock service should not be overriden in the production configuration.");
#endif
		}

		/// <summary>
		/// This method removes the registered IClockService, reverting the behavior of ClockProvider to the standard behavior of using the system clock.
		/// </summary>
		public static void ResetClockService()
		{
#if DEBUG
			_clockService = null;
#else
			//Here the clock service can never be set, so reset is always a no-op
#endif
		}
	}
}
