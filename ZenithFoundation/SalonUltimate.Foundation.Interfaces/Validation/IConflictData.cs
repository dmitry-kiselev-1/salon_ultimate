﻿using System;

namespace SalonUltimate.Foundation.Interfaces.Validation
{
	public interface IConflictData
	{
		DateTime Date { get; }
		TimeSpan FromTime { get; }
		TimeSpan ToTime { get; }

		string EmployeeFullName { get; }
		string ServiceDescription { get; }
		string Notes { get; set; }
		
		int Duration { get; }
		decimal Price { get; }
		TimeSpan? Overlap { get; set; }
	}
}
