﻿using System.Reflection;

namespace SalonUltimate.Foundation.Validation
{
	public class NotEmptyRule : Rule
	{
		public NotEmptyRule(string propertyName, string brokenDescription) : base(propertyName, brokenDescription) { }
		//Note: can only be used with properties that are strings.
		public override bool ValidateRule(object domainObject)
		{
			PropertyInfo pi = domainObject.GetType().GetProperty(this.PropertyName);
			string test = (string)pi.GetValue(domainObject, null);

			return !(test == null || test.Trim().Length == 0);
		}
	}
}
