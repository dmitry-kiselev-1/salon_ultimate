using System.Reflection;
using System.Text.RegularExpressions;

namespace SalonUltimate.Foundation.Validation
{
	public class RegexRule : Rule
	{
		private readonly string _regex;

		/// <summary>
		/// Constructor.
		/// </summary>
		public RegexRule(string propertyName, string description, string regex)
			: base(propertyName, description)
		{
			_regex = regex;
		}


		public override bool ValidateRule(object domainObject)
		{
			PropertyInfo pi = domainObject.GetType().GetProperty(this.PropertyName);
			Match m = Regex.Match(pi.GetValue(domainObject, null).ToString(), (string) _regex);

			return m.Success;
		}
	}
}
