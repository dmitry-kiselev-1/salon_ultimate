﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Linq;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection
{
	public abstract class PropertyValidator
	{
		public abstract bool AppliesToClasses { get; }
		public abstract bool AppliesToCollections { get; }
		public abstract bool AppliesToLeafs { get; }

		public abstract bool NeedsValidation(PropertyInfo property);

		public abstract bool IsValid(PropertyInfo property, object model);

		public void Validate(PropertyInfo property, object model, ModelStateDictionary modelState)
		{
			if (NeedsValidation(property) && !IsValid(property, model))
			{
				modelState?.AddModelError(property.Name, GetErrorMessage(property, model));
			}
		}

		protected virtual string GetErrorMessage(PropertyInfo property, object model) => $"{property.Name} is invalid";

		protected bool TryExtractNullableValueType(Type type, out Type valueType)
		{
			var result = type.IsGenericType && typeof(Nullable<>) == type.GetGenericTypeDefinition();
			valueType = result
				? type.GetGenericArguments().First()
				: null;
			return result;
		}
	}
}
