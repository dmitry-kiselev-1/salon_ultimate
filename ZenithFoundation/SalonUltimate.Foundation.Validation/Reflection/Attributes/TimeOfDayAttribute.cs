﻿using System;

namespace SalonUltimate.Foundation.Validation.Reflection.Attributes
{
	/// <summary>
	/// This is a marker attribute that should decorate TimeSpan, TimeSpan? public properties
	/// on the input models that represent time of day to make sure it is within [0..24h) range.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class TimeOfDayAttribute : Attribute
	{
	}
}
