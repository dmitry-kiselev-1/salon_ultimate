﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection
{
	public class ModelTraverseValidator
	{
		private readonly List<PropertyValidator> _classValidators;
		private readonly List<PropertyValidator> _collectionValidators;
		private readonly List<PropertyValidator> _leafValidators;

		public ModelTraverseValidator(params PropertyValidator[] validators)
		{
			_classValidators = validators.Where(x => x.AppliesToClasses).ToList();
			_collectionValidators = validators.Where(x => x.AppliesToCollections).ToList();
			_leafValidators = validators.Where(x => x.AppliesToLeafs).ToList();
		}

		public ModelStateDictionary GetValidState<TModel>(TModel model, ModelStateDictionary modelState = null)
		{
			modelState = modelState ?? new ModelStateDictionary();

			if (EqualityComparer<TModel>.Default.Equals(model, default))
				return modelState;

			if (typeof(IEnumerable).IsAssignableFrom(model.GetType()))  // If it was like ResponseVM<IEnumerable<T>> by mistake
			{                                                           // it should always be CollectionResponse<T> in that case
				return GetValidState((model as IEnumerable)?.Cast<object>(), modelState);
			}

			var properties = GetPropertiesSafe(model.GetType());
			foreach (var property in properties)
			{
				VisitProperty(property, model, modelState);
			}

			return modelState;
		}

		public ModelStateDictionary GetValidState<TModel>(IEnumerable<TModel> models, ModelStateDictionary modelState = null)
		{
			modelState = modelState ?? new ModelStateDictionary();

			if (models == null)
				return modelState;

			foreach (var model in models)
			{
				GetValidState(model, modelState);
			}

			return modelState;
		}

		#region Validators

		protected void ValidateClass(PropertyInfo property, object model, ModelStateDictionary modelState)
			=> Validate(property, model, modelState, _classValidators);

		protected void ValidateCollection(PropertyInfo property, object model, ModelStateDictionary modelState)
			=> Validate(property, model, modelState, _collectionValidators);

		protected void ValidateLeaf(PropertyInfo property, object model, ModelStateDictionary modelState)
			=> Validate(property, model, modelState, _leafValidators);

		protected void Validate(PropertyInfo property, object model, ModelStateDictionary modelState, List<PropertyValidator> validators)
		{
			foreach (var attributeValidator in validators)
				attributeValidator.Validate(property, model, modelState);
		}

		#endregion

		#region Visitors

		protected void VisitProperty(PropertyInfo property, object model, ModelStateDictionary modelState)
		{
			if (model == null)
				return;

			if (IsModelLeaf(property.PropertyType))
			{
				VisitLeaf(property, model, modelState);
			}
			else if (typeof(IEnumerable).IsAssignableFrom(property.PropertyType))
			{
				VisitCollection(property, model, modelState);
			}
			else if (property.PropertyType.IsClass)
			{
				VisitClass(property, model, modelState);
			}
		}

		protected void VisitClass(PropertyInfo property, object model, ModelStateDictionary modelState)
		{
			if (model == null)
				return;

			ValidateClass(property, model, modelState);

			var properties = GetPropertiesSafe(property.PropertyType);
			foreach (var innerProp in properties)
			{
				var propValue = property.GetValue(model);
				VisitProperty(innerProp, propValue, modelState);
			}
		}

		protected void VisitCollection(PropertyInfo property, object model, ModelStateDictionary modelState)
		{
			if (model == null)
				return;

			var rawCollection = property.GetValue(model);
			var collection = (rawCollection as IEnumerable)?.Cast<object>().ToArray();

			ValidateCollection(property, model, modelState);
			GetValidState(collection, modelState);
		}

		protected void VisitLeaf(PropertyInfo property, object model, ModelStateDictionary modelState)
		{
			if (model == null)
				return;

			if (IsModelLeaf(property.PropertyType))
				ValidateLeaf(property, model, modelState);
		}

		#endregion

		#region Helpers

		/// <summary>
		/// Indicates whether the type is either primitive one, enum or string, so we do not need to enumerate its properties/children
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected bool IsModelLeaf(Type type)
		{
			return type.IsValueType || type == typeof(string);
		}

		protected virtual BindingFlags PropertyBindingFlags => BindingFlags.Public | BindingFlags.Instance;

		/// <summary>
		/// This method will ignore hidden properties
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		protected virtual IEnumerable<PropertyInfo> GetPropertiesSafe(Type type)
		{
			var properties = type
				.GetProperties(PropertyBindingFlags)
				.GroupBy(x => x.Name);

			foreach(var group in properties)
			{
				if (group.Count() == 1)
				{
					yield return group.First();
					continue;
				}

				// else exclude hidden properties since UI has no chance to fill them
				var singleProp = type.GetProperty(group.Key, PropertyBindingFlags | BindingFlags.DeclaredOnly);
				if (singleProp != null)
					yield return singleProp;
			}
		}

		#endregion
	}
}
