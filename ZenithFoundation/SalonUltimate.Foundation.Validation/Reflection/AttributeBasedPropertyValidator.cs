﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection
{
	public abstract class AttributeBasedPropertyValidator : PropertyValidator
	{
		protected Type AttributeType { get; private set; }

		public AttributeBasedPropertyValidator(Type attributeType)
		{
			AttributeType = attributeType;
		}

		public override bool NeedsValidation(PropertyInfo property) => HasAttribute(property);

		protected bool HasAttribute(PropertyInfo property) => GetAttribute(property) != null;
		protected Attribute GetAttribute(PropertyInfo property) => property.GetCustomAttribute(AttributeType, true);
		protected object GetValue(PropertyInfo property, object model) => property.GetValue(model);
	}
}
