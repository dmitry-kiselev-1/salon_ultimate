﻿using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection.Validators
{
	public class RequiredPropertyValidator : AttributeBasedPropertyValidator
	{
		public RequiredPropertyValidator() : base(typeof(RequiredAttribute)) { }

		public override bool AppliesToClasses => true;

		public override bool AppliesToCollections => true;

		public override bool AppliesToLeafs => true;

		public override bool IsValid(PropertyInfo property, object model)
		{
			var value = GetValue(property, model);
			return value != null;
		}

		protected override string GetErrorMessage(PropertyInfo property, object model)
		{
			return $"{property.Name} is required";
		}
	}
}
