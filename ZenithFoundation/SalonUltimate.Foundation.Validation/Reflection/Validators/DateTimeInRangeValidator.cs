﻿using System;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection.Validators
{
	public class DateTimeInRangeValidator : PropertyValidator
	{
		private readonly DateTime _minDate = new DateTime(1800, 1, 1);

		public override bool AppliesToClasses => false;

		public override bool AppliesToCollections => false;

		public override bool AppliesToLeafs => true;

		public override bool IsValid(PropertyInfo property, object model)
		{
			var value = property.GetValue(model);
			return value == null // it is valid unless it has [Required] attribute
				|| ((DateTime)value) >= _minDate;
		}

		public override bool NeedsValidation(PropertyInfo property)
		{
			return property.PropertyType == typeof(DateTime)
				|| property.PropertyType == typeof(DateTime?);
		}

		protected override string GetErrorMessage(PropertyInfo property, object model)
		{
			return $"{property.Name} - date value is too small.";
		}
	}
}
