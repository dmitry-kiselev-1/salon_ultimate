﻿using System;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection.Validators
{
	public class EnumInRangeValidator : PropertyValidator
	{
		protected const int PseudoNull = -1;

		public override bool AppliesToClasses => false;

		public override bool AppliesToCollections => false;

		public override bool AppliesToLeafs => true;

		public override bool IsValid(PropertyInfo property, object model)
		{
			var value = property.GetValue(model);
			var isNullableEnum = TryExtractNullableValueType(property.PropertyType, out var valueType);

			return isNullableEnum && CheckEnum(valueType, value)
				|| !isNullableEnum && CheckEnum(property.PropertyType, value);
		}

		public override bool NeedsValidation(PropertyInfo property)
			=> property.PropertyType.IsEnum 
				|| TryExtractNullableValueType(property.PropertyType, out var valueType) && valueType.IsEnum;

		protected bool CheckEnum(Type enumType, object value)
		{
			if (value == null)
				return true; // null value for nullable enum is valid.

			var nullableEnumType = typeof(Nullable<>).MakeGenericType(enumType);
			if (value.GetType() == nullableEnumType)
			{
				// else convert nullable to not-nullable - and we're safe to "convert" null to non-valid -1 value since we've already made sure it's not null
				value = nullableEnumType
					.GetMethod(nameof(Nullable<int>.GetValueOrDefault))
					.Invoke(value, new object[] { PseudoNull });
			}

			return enumType.IsEnumDefined(value) && !value.Equals(Enum.ToObject(enumType, PseudoNull));
		}
	}
}
