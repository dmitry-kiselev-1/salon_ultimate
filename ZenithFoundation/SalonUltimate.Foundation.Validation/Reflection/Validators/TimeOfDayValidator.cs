﻿using System;
using System.Reflection;
using SalonUltimate.Foundation.Validation.Reflection.Attributes;

namespace SalonUltimate.Foundation.Validation.Reflection.Validators
{
	public class TimeOfDayValidator : AttributeBasedPropertyValidator
	{
		public TimeOfDayValidator() : base(typeof(TimeOfDayAttribute))
		{
		}

		public override bool AppliesToClasses => false;
		public override bool AppliesToCollections => false;
		public override bool AppliesToLeafs => true;

		public override bool IsValid(PropertyInfo property, object model)
		{
			object value = property.GetValue(model);
			if (value == null)
				return true;

			if(property.PropertyType == typeof(TimeSpan?))
			{
				value = typeof(TimeSpan?)
					.GetMethod(nameof(Nullable<TimeSpan>.GetValueOrDefault), new Type[] { })
					.Invoke(value, null);
			}

			TimeSpan converted = (TimeSpan)value;
			return converted >= TimeSpan.Zero && converted < TimeSpan.FromHours(24);
		}

		public override bool NeedsValidation(PropertyInfo property) 
			=> (property.PropertyType == typeof(TimeSpan) || property.PropertyType == typeof(TimeSpan?))
				&& base.NeedsValidation(property);

		protected override string GetErrorMessage(PropertyInfo property, object model) 
			=> $"{property.Name}={property.GetValue(model)} is not a valid time of day";
	}
}
