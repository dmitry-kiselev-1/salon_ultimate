﻿using System.Collections;
using System.Linq;
using System.Reflection;

namespace SalonUltimate.Foundation.Validation.Reflection.Validators
{
	public class CollectionsWithoutNullsValidator : PropertyValidator
	{
		public override bool AppliesToClasses => false;

		public override bool AppliesToCollections => true;

		public override bool AppliesToLeafs => false;

		public override bool IsValid(PropertyInfo property, object model)
		{
			var rawCollection = property.GetValue(model);
			var collection = (rawCollection as IEnumerable)?.Cast<object>();
			return collection?.Any(x => x == null) != true;
		}

		public override bool NeedsValidation(PropertyInfo property)
		{
			return typeof(IEnumerable).IsAssignableFrom(property.PropertyType);
		}

		protected override string GetErrorMessage(PropertyInfo property, object model)
		{
			return $"{property.Name}: collection contains null values";
		}
	}
}
