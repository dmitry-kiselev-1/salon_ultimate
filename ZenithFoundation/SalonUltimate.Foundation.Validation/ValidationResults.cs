﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Exceptions.Validation;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.Foundation.Interfaces.Validation;

namespace SalonUltimate.Foundation.Validation
{
	public class ValidationResult
	{

		public List<ValidationIssueVM> Issues { get => _issues ?? (_issues = new List<ValidationIssueVM>()); set => _issues = value; }
		public bool IsValid => !Errors.Any();

		#region Protected properties

		protected IEnumerable<ValidationIssueVM> Errors => Issues?.Where(x => !x.CanBeSkipped) ?? Enumerable.Empty<ValidationIssueVM>();
		protected IEnumerable<ValidationIssueVM> Warnings => Issues?.Where(x => x.CanBeSkipped) ?? Enumerable.Empty<ValidationIssueVM>();
		protected List<ValidationIssueVM> _issues;

		#endregion

		#region Public properties

		public string ErrorsToString => Errors.Any()
			? $"Errors:{Environment.NewLine}{string.Join(Environment.NewLine, Errors.Select(x => x.Description))}"
			: string.Empty;

		public string WarningsToStrings => Warnings.Any()
			? $"Warnings:{Environment.NewLine}{string.Join(Environment.NewLine, Warnings.Select(x => x.Description))}"
			: string.Empty;

		public string AllIssuesToString => (ErrorsToString + Environment.NewLine + WarningsToStrings).Trim();



		public string SystemErrorsToString => Errors.Any()
			? $"Errors:{Environment.NewLine}{string.Join(Environment.NewLine, Errors.Select(x => x.SystemMessage))}"
			: string.Empty;

		public string SystemWarningsToStrings => Warnings.Any()
			? $"Warnings:{Environment.NewLine}{string.Join(Environment.NewLine, Warnings.Select(x => x.SystemMessage))}"
			: string.Empty;

		public string AllSystemMessagesToString => (SystemErrorsToString + Environment.NewLine + SystemWarningsToStrings).Trim();



		public List<ValidationSystemInfoVM> ErrorsDebugInfo => Errors.Any()
			? Errors.Select(x => new ValidationSystemInfoVM(x.Description, x.SystemInfo)).ToList()
			: null;

		public List<ValidationSystemInfoVM> WarningsDebugInfo => Warnings.Any()
			? Warnings.Select(x => new ValidationSystemInfoVM(x.Description, x.SystemInfo)).ToList()
			: null;

		public List<ValidationSystemInfoVM> AllIssuesDebugInfo => 
			Issues.Select(x => new ValidationSystemInfoVM(x.Description, x.SystemInfo)).ToList();

		#endregion

		#region Constructors

		public ValidationResult(string message, bool canBeSkipped, object conflictData = null, object systemInfo = null) : this(message, message, canBeSkipped, conflictData, systemInfo)
		{
		}

		public ValidationResult(string message, string systemMessage, bool canBeSkipped, object conflictData = null, object systemInfo = null) : base()
		{
			AddIssue(message, systemMessage, canBeSkipped, conflictData, systemInfo);
		}

		public ValidationResult()
		{
			Issues = new List<ValidationIssueVM>();
		}

		#endregion

		#region Public methods

		public void AddResult(ValidationResult result)
		{
			if (result.Issues == null)
				return;

			if (Issues == null)
				Issues = result.Issues;
			else
				Issues.AddRange(result.Issues);
		}

		public void AddError(string errorMessage, object conflictData = null, object systemInfo = null)
		{
			AddError(errorMessage, errorMessage, conflictData, systemInfo);
		}

		public void AddError(string errorMessage, string systemMessage, object conflictData = null, object systemInfo = null)
		{
			AddIssue(errorMessage, systemMessage, false, conflictData, systemInfo);
		}

		public void AddWarning(string warningMessage, object conflictData = null, object systemInfo = null)
		{
			AddWarning(warningMessage, warningMessage, conflictData, systemInfo);
		}

		public void AddWarning(string warningMessage, string systemMessage, object conflictData = null, object systemInfo = null)
		{
			AddIssue(warningMessage, systemMessage, true, conflictData, systemInfo);
		}

		// circular links solution (Validation <-> Exception):
		// throw new ZenithValidationException(this) -> throw new ZenithValidationException(ValidationResultExceptionInfo)
		public void ThrowIfNotValid()
		{
			if (!this.IsValid)
				throw new ZenithValidationException(
					new ValidationResultExceptionInfo()
					{
						Issues = new List<ValidationIssueExceptionInfo>(this.Issues.Select(s =>
							new ValidationIssueExceptionInfo(
								description: s.Description, systemMessage: s.SystemMessage,
								canBeSkipped: s.CanBeSkipped, conflictData: s.ConflictData)))
					});
		}

		#endregion

		protected void AddIssue(string issueMessage, string systemMessage, bool canBeSkipped, object conflictData = null, object systemInfo = null)
		{
			if (Issues == null)
				Issues = new List<ValidationIssueVM>();

			if (systemInfo == null && conflictData != null)
				systemInfo = conflictData;

				Issues.Add(new ValidationIssueVM(issueMessage, systemMessage, canBeSkipped, conflictData as IConflictData, systemInfo));
		}
	}
}
