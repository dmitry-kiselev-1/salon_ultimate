﻿using System;

namespace SalonUltimate.Foundation.Validation
{
	public class MultiFieldSimpleRule: SimpleRule
	{
		public MultiFieldSimpleRule(string description, SimpleRuleDelegate ruleDelegate, params string[] fieldsAffected)
			:base(
				(fieldsAffected!=null && fieldsAffected.Length == 1)? fieldsAffected[0]: string.Empty, 
				description, ruleDelegate
			)
		 {
			if (fieldsAffected == null || fieldsAffected.Length == 0)
				throw new ArgumentException("fieldsAffected");

			if (fieldsAffected.Length > 1)
			{
				PropertyNames = fieldsAffected;
			}
		 }
	}
}
