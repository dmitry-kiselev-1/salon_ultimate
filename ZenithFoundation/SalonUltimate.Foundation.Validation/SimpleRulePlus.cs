﻿using System;

namespace SalonUltimate.Foundation.Validation
{
	public class SimpleRulePlus : SimpleRule
	{
		private readonly Func<string> _brokenDescriptionDelegate;
		public SimpleRulePlus(string propertyName, Func<string> brokenDescriptionDelegate, SimpleRuleDelegate ruleDelegate)
			: base(propertyName, (string) null, ruleDelegate)
		{
			_brokenDescriptionDelegate = brokenDescriptionDelegate;
		}

		public override string Description => _brokenDescriptionDelegate.Invoke();
	}
}
