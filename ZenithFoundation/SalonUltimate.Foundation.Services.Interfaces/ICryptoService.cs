﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Services.Interfaces
{
	public interface ICryptoService
	{
		string Encrypt(string dataToEncrypt);
		string Decrypt(string dataToEncrypt, string errorMsg = "Unable to decrypt invalid string");
	}
}
