﻿namespace SalonUltimate.Foundation.Services.Interfaces
{
	public interface IUuidGeneratingService
	{
		string GenerateUuid(string deploymentUuid, string tableName, string id, string storeId = null);
	}
}