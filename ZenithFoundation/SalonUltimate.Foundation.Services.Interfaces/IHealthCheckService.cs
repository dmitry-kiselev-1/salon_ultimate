﻿using System.Threading.Tasks;

namespace SalonUltimate.Foundation.Services.Interfaces
{
    public interface IHealthCheckService
    {
	    Task WarmUpDbContext();
    }
}
