﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Services.Interfaces
{
    public interface IBackgroundJobsService
    {
        string FireAndForget(Expression<Action> action);
        string FireAndForgetWithDelay(Expression<Action> action, TimeSpan delay);
        JobInfo<T> CheckJobStatus<T>(string jobId);
        void AddRecurring(Expression<Action> action, Func<string> cronExpression);
        void AddRecurring<T>(Expression<Func<T, Task>> action, Func<string> cronExpression);
    }
}
