﻿namespace SalonUltimate.Foundation.Services.Interfaces
{
	public interface ISmartStringSearchService
	{
		bool Similar(string s1, string s2);
		double GetSimilarity(string s1, string s2);
	}
}
