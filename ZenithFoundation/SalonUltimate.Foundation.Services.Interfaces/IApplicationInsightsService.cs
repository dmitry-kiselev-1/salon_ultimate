﻿using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Services.Interfaces
{
    public interface IApplicationInsightsService
    {
	    void TrackEvent(ApplicationInsightsCustomEventVM model);

		void TrackBadDataEvent(ApplicationInsightsCustomEventVM model);

		void TrackException(ApplicationInsightsExceptionEventVM model);

	    void TrackMetric(ApplicationInsightsMetricEventVM model);

	    void Flush();
    }
}
