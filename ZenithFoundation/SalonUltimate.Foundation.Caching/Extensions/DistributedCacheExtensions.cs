﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace SalonUltimate.Foundation.Caching.Extensions
{
	/// <summary>
	/// Extension methods that help to work with objects.
	/// </summary>
	public static class DistributedCacheExtensions
	{
		/// <summary>
		/// Asynchronously sets a serialized object value in the specified cache with the specified key.
		/// </summary>
		/// <typeparam name="TValue">Type of the object to set in the specified cache.</typeparam>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to store the data in.</param>
		/// <param name="value">The data to store in the cache.</param>
		/// <param name="cancellationToken">Optional. A <see cref="CancellationToken" /> to cancel the operation.</param>
		/// <returns>A task that represents the asynchronous set operation.</returns>
		/// <exception cref="System.ArgumentNullException">Thrown when <paramref name="key"/> or <paramref name="value"/> is null.</exception>
		public static Task SetObjectAsync<TValue>(this IDistributedCache cache, string key, TValue value,
			CancellationToken cancellationToken = default)
		{
			if (key == null)
			{
				throw new ArgumentNullException(nameof(key));
			}
			if (value == null)
			{
				throw new ArgumentNullException(nameof(value));
			}

			var stringValue = SerializeObject(value);
			return cache.SetStringAsync(key, stringValue, cancellationToken);
		}

		/// <summary>
		/// Asynchronously sets a serialized object value in the specified cache with the specified key.
		/// </summary>
		/// <typeparam name="TValue">Type of the object to set in the specified cache.</typeparam>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to store the data in.</param>
		/// <param name="value">The data to store in the cache.</param>
		/// <param name="options">The cache options for the entry.</param>
		/// <param name="cancellationToken">Optional. A <see cref="CancellationToken" /> to cancel the operation.</param>
		/// <returns>A task that represents the asynchronous set operation.</returns>
		/// <exception cref="System.ArgumentNullException">Thrown when <paramref name="key"/> or <paramref name="value"/> is null.</exception>
		public static Task SetObjectAsync<TValue>(this IDistributedCache cache, string key, TValue value,
			DistributedCacheEntryOptions options, CancellationToken cancellationToken = default)
		{
			if (key == null)
			{
				throw new ArgumentNullException(nameof(key));
			}
			if (value == null)
			{
				throw new ArgumentNullException(nameof(value));
			}

			var stringValue = SerializeObject(value);
			return cache.SetStringAsync(key, stringValue, options, cancellationToken);
		}

		/// <summary>
		/// Asynchronously gets a deserialized object from the specified cache with the specified key.
		/// </summary>
		/// <typeparam name="TValue"></typeparam>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to store the data in.</param>
		/// <param name="cancellationToken">Optional. A <see cref="CancellationToken" /> to cancel the operation.</param>
		/// <returns>A task that gets the object value from the stored cache key.</returns>
		public static async Task<TValue> GetObjectAsync<TValue>(this IDistributedCache cache, string key,
			CancellationToken cancellationToken = default)
		{
			var stringValue = await cache.GetStringAsync(key, cancellationToken).ConfigureAwait(false);
			if (string.IsNullOrEmpty(stringValue))
			{
				return default;
			}

			var value = DeserializeObject<TValue>(stringValue);
			return value;
		}

		/// <summary>
		/// Sets an object in the specified cache with the specified key.
		/// </summary>
		/// <typeparam name="TValue">Type of the object to set in the specified cache.</typeparam>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to store the data in.</param>
		/// <param name="value">The data to store in the cache.</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown when <paramref name="key"/> or <paramref name="value"/> is null.
		/// </exception>
		public static void SetObject<TValue>(this IDistributedCache cache, string key, TValue value)
		{
			if (key == null)
			{
				throw new ArgumentNullException(nameof(key));
			}
			if (value == null)
			{
				throw new ArgumentNullException(nameof(value));
			}

			var stringValue = SerializeObject(value);
			cache.SetString(key, stringValue);
		}

		/// <summary>
		/// Sets an object in the specified cache with the specified key.
		/// </summary>
		/// <typeparam name="TValue">Type of the object to set in the specified cache.</typeparam>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to store the data in.</param>
		/// <param name="value">The data to store in the cache.</param>
		/// <param name="options">The cache options for the entry.</param>
		/// <exception cref="System.ArgumentNullException">
		/// Thrown when <paramref name="key"/> or <paramref name="value"/> is null.
		/// </exception>
		public static void SetObject<TValue>(this IDistributedCache cache, string key, TValue value,
			DistributedCacheEntryOptions options)
		{
			if (key == null)
			{
				throw new ArgumentNullException(nameof(key));
			}
			if (value == null)
			{
				throw new ArgumentNullException(nameof(value));
			}

			var stringValue = SerializeObject(value);
			cache.SetString(key, stringValue, options);
		}

		/// <summary>
		/// Gets an object from the specified cache with the specified key.
		/// </summary>
		/// <param name="cache">The cache in which to store the data.</param>
		/// <param name="key">The key to get the stored data for.</param>
		/// <returns>The deserialized object from the stored cache key.</returns>
		public static TValue GetObject<TValue>(this IDistributedCache cache, string key)
		{
			var stringValue = cache.GetString(key);
			if (string.IsNullOrEmpty(stringValue))
			{
				return default;
			}

			var value = DeserializeObject<TValue>(stringValue);
			return value;
		}

		private static string SerializeObject(object value)
		{
			return JsonConvert.SerializeObject(value);
		}

		private static TValue DeserializeObject<TValue>(string stringValue)
		{
			return JsonConvert.DeserializeObject<TValue>(stringValue);
		}
	}
}
