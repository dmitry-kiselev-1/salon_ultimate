﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Redis;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace SalonUltimate.Foundation.Caching.Redis.HealthCheck
{
	/// <summary>
	/// Tests Redis distributed cache connection for liveness.
	/// </summary>
	public class RedisConnectionHealthCheck : IHealthCheck
	{
		private readonly RedisCacheOptions _options;
		private ConnectionMultiplexer _redis;
		private readonly SemaphoreSlim _connectionLock = new SemaphoreSlim(initialCount: 1, maxCount: 1);

		/// <summary>
		/// Initializes a new instance of the <see cref="RedisConnectionHealthCheck"/>.
		/// </summary>
		public RedisConnectionHealthCheck(IOptions<RedisCacheOptions> optionsAccessor)
		{
			_options = optionsAccessor?.Value ?? throw new ArgumentNullException(nameof(optionsAccessor));
		}

		/// <inheritdoc />
		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			try
			{
				await ConnectAsync(cancellationToken);
				await _redis.GetDatabase().PingAsync();
				return HealthCheckResult.Healthy();
			}
			catch (Exception ex)
			{
				return new HealthCheckResult(context.Registration.FailureStatus, exception: ex);
			}
		}

		private async Task ConnectAsync(CancellationToken token = default)
		{
			token.ThrowIfCancellationRequested();

			if (_redis != null)
			{
				return;
			}

			await _connectionLock.WaitAsync(token);
			try
			{
				if (_redis == null)
				{
					if (_options.ConfigurationOptions != null)
					{
						_redis = await ConnectionMultiplexer.ConnectAsync(_options.ConfigurationOptions);
					}
					else
					{
						_redis = await ConnectionMultiplexer.ConnectAsync(_options.Configuration);
					}
				}
			}
			finally
			{
				_connectionLock.Release();
			}
		}
	}
}
