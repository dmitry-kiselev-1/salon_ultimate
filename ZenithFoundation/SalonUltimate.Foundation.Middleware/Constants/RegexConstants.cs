﻿namespace SalonUltimate.Foundation.Middleware.Constants
{
	public class RegexConstants
	{
		#region Sensitive data replacement regex

		public const string EmailReplacement = "^.?(.+?)@.+$";
		public const string PhoneReplacement = @"^(\d{0,3})-?(\d{0,3})-?\d*$";

		public const char ReplacementChar = '*';

		#endregion
	}
}
