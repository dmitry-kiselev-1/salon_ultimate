﻿using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Microsoft.ApplicationInsights;

namespace SalonUltimate.Foundation.Middleware.Filters.Hangfire
{
	public class AppInsightsLoggingAttribute : JobFilterAttribute, IServerFilter, IElectStateFilter
	{
		public void OnPerforming(PerformingContext filterContext)
		{

		}

		public void OnPerformed(PerformedContext filterContext)
		{

		}

		public void OnStateElection(ElectStateContext context)
		{
			if (context.CandidateState is FailedState failedState)
			{
				var telemetryClient = new TelemetryClient();
				telemetryClient.TrackException(failedState.Exception);
			}
		}
	}
}
