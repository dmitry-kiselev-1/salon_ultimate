﻿using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SalonUltimate.Foundation.Middleware.Filters.AuthorizationFilters
{
	public class EnableRewindAuthorizationFilter : IAuthorizationFilter
	{
		public void OnAuthorization(AuthorizationFilterContext context)
		{
			context.HttpContext.Request.EnableRewind();
		}
	}
}