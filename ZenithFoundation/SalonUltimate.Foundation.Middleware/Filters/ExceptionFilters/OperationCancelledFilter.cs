﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace SalonUltimate.Foundation.Middleware.Filters.ExceptionFilters
{
    public class OperationCancelledFilter : IAsyncExceptionFilter
    {
        private readonly ILogger<OperationCancelledFilter> _logger;

        public OperationCancelledFilter(ILogger<OperationCancelledFilter> logger)
        {
            _logger = logger;
        }

        public Task OnExceptionAsync(ExceptionContext context)
        {
            if (context.Exception is OperationCanceledException)
            {
                _logger.LogInformation("Request was cancelled");
                context.ExceptionHandled = true;
                context.Result = new StatusCodeResult(499);
            }

            return Task.CompletedTask;
        }
    }
}
