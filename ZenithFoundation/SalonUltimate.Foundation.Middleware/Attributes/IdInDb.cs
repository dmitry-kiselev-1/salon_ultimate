﻿using System;

namespace SalonUltimate.Foundation.Middleware.Attributes
{
	public class IdInDb : Attribute
	{
		public Type Type { get; }
		public bool CheckDeleted { get; set; }
		public bool CheckStoreKey { get; set; }

		public IdInDb(Type type, bool checkDeleted = true, bool checkStoreKey = true)
		{
			Type = type;
			CheckDeleted = checkDeleted;
			CheckStoreKey = checkStoreKey;
		}
	}
}
