﻿using System;
using System.Text.RegularExpressions;

namespace SalonUltimate.Foundation.Middleware.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
	public class SensitiveDataAttribute : Attribute
	{
		public SensitiveDataAttribute()
			: this(".*", '*')
		{ }

		public SensitiveDataAttribute(string regex, char replacementChar)
		{
			ReplacementRegex = new Regex(regex, RegexOptions.IgnoreCase | RegexOptions.Singleline);
			ReplaceByChar = replacementChar;
		}

		public char ReplaceByChar { get; }
		public Regex ReplacementRegex { get; }
	}
}
