﻿using System;

namespace SalonUltimate.Foundation.Middleware.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = true, Inherited = true)]
	public class RequiredPermissionAttribute : Attribute
	{
		protected readonly string _permission;

		public RequiredPermissionAttribute(string permission)
		{
			_permission = permission;
		}

		public string Permission => _permission;
	}
}
