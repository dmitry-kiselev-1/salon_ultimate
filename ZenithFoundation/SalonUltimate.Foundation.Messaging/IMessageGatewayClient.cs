﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SalonUltimate.Foundation.Messaging.Models;

namespace SalonUltimate.Foundation.Messaging
{
	public interface IMessageGatewayClient
	{
		Task<SmsResponseVm> SendSmsAsync(SmsModel model);
		Task<EmailResponseVm> SendEmailAsync(EmailModel model);
	}
}
