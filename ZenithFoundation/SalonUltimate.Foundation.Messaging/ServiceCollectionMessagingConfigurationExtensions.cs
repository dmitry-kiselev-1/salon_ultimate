﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Messaging
{
	public static class ServiceCollectionMessagingConfigurationExtensions
	{
		/// <summary>
		/// Configuring message gateway settings injection and related services
		/// apsettings.json should contain MessageGatewaySettings section
		/// </summary>
		/// <param name="services"></param>
		/// <param name="config"></param>
		public static void ConfigureMessaging(this IServiceCollection services, IConfiguration config)
		{
			services.Configure<MessageGatewaySettings>(config.GetSection("MessageGatewaySettings"));

			services.AddScoped<IMessageGatewayClient, MessageGatewayClient>();
		}
	}
}
