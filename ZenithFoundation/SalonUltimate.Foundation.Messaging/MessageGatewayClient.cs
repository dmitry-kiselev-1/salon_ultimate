﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SalonUltimate.Foundation.Helpers.Rest.Interfaces;
using SalonUltimate.Foundation.Messaging.Models;

namespace SalonUltimate.Foundation.Messaging
{
	public class MessageGatewayClient : IMessageGatewayClient
	{
		private readonly IRestRequestBuilder _restRequestBuilder;
		private readonly MessageGatewaySettings _mgSettings;

		public MessageGatewayClient(IRestRequestBuilder restRequestBuilder, IOptions<MessageGatewaySettings> mgClientSettingsAccessor)
		{
			_restRequestBuilder = restRequestBuilder;
			_mgSettings = mgClientSettingsAccessor.Value;
		}

		public async Task<EmailResponseVm> SendEmailAsync(EmailModel model)
		{
			return await _restRequestBuilder
							.WithIsolatedSettings(_mgSettings.Host)
							.To(@"/api/v1/email")
							.WithPayload(model)
							.PostAsync<EmailResponseVm>();
		}

		public async Task<SmsResponseVm> SendSmsAsync(SmsModel model)
		{
			return await _restRequestBuilder
							.WithIsolatedSettings(_mgSettings.Host)
							.To(@"/api/v1/sms")
							.WithPayload(model)
							.PostAsync<SmsResponseVm>();
		}
	}
}
