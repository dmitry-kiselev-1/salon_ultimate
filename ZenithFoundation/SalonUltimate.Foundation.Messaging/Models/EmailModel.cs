﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Messaging.Models
{
	public class EmailModel : MessageModelBase
	{
		public string Subject { get; set; }
		public string Html { get; set; }
		public string ApiKey { get; set; }
		public List<InlineImageBase64> Images { get; set; }
	}
}
