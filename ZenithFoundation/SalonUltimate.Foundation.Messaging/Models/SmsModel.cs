﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Messaging.Models
{
	public class SmsModel : MessageModelBase
	{
		public string AuthToken { get; set; }
		public string AccountSid { get; set; }
	}
}
