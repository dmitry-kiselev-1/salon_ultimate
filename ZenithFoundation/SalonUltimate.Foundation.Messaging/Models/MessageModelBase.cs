﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Messaging.Models
{
	public class MessageModelBase
	{
		public string Text { get; set; }
		public string To { get; set; } //TODO introduce validation
		public string From { get; set; }
	}
}
