﻿namespace SalonUltimate.Foundation.Messaging.Models
{
	public class InlineImageBase64
	{
		public string Name { get; set; }
		public string Data { get; set; }
	}
}