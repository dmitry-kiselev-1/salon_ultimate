﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.Enums
{
	public enum RedisCacheKey
	{
		RevokedRefreshToken = 1,
		WaitTimeQueueItems = 2,
		WaitTimeStores = 3,
		WaitTimeStoreEmployees = 4,
		WaitTimeStoreServices = 5
	}
}
