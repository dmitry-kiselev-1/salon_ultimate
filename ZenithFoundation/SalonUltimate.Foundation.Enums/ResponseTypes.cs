﻿namespace SalonUltimate.Foundation.Enums
{
	public enum ResponseTypes
	{
		NotImplemented = -1,
		Success = 1,
		NotAuthenticated = 2,
		NoPermission = 3,
		FailedValidation = 4,
		Exception = 5,
		NotFound = 6,
		BadData = 7,
		Conflict = 8,
		InvalidOperation = 9
	}
}
