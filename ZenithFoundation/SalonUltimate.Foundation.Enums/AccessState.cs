﻿namespace SalonUltimate.Foundation.Enums
{
	public enum AccessState
	{
		Allowed = 1,
		AllowedWithRelogin = 2,
		Denied = 3,
	}
}
