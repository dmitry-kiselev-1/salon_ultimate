#!/bin/bash
##exit 0
echo "nuget_packages.sh started..."
SU_CSPROJ=${1}
NEW_VERSION=${2}
SU_CONFIG=${3}
SU_NUGET_URL=${4}
SU_NUGET_API_KEY=${5}
echo "create nuget package for SalonUltimate.Foundation:"
dotnet pack $SU_CSPROJ -c $SU_CONFIG /p:PackageVersion="$NEW_VERSION" /p:version="$NEW_VERSION"
dotnet nuget push SalonUltimate.Foundation/bin/$SU_CONFIG/*.nupkg -s $SU_NUGET_URL -k $SU_NUGET_API_KEY
echo "create nuget packages for each SalonUltimate.Foundation projects:"
declare -a PROJECTS=(
    "Auth" "BasicTypes" "Context" "Controllers" "DTOs"
    "Enums" "Exceptions" "Extensions" "Hangfire"
    "Helpers" "Interfaces" "Messaging" "Middleware" "Models"
    "Services.Implementation" "Services.Interfaces" "Settings" "Storage"
    "Tests" "Time" "Validation" "ViewModels" "FileSystem")
for PROJECT in "${PROJECTS[@]}"
do
	SU_CSPROJ_PATH_TEMPLATE="SalonUltimate.Foundation.PROJECT\SalonUltimate.Foundation.PROJECT.csproj";
    SU_CSPROJ_CURRENT=${SU_CSPROJ_PATH_TEMPLATE//PROJECT/${PROJECT}};
    echo "create nuget package for $SU_CSPROJ_CURRENT";
    dotnet pack $SU_CSPROJ_CURRENT -c $SU_CONFIG /p:PackageVersion="$NEW_VERSION" /p:version="$NEW_VERSION";
    dotnet nuget push SalonUltimate.Foundation.${PROJECT}/bin/$SU_CONFIG/*.nupkg -s $SU_NUGET_URL -k $SU_NUGET_API_KEY;
done;
