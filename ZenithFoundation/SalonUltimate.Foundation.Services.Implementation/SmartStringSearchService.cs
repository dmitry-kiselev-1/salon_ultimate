﻿using F23.StringSimilarity;
using SalonUltimate.Foundation.Services.Interfaces;

namespace SalonUltimate.Foundation.Services.Implementation
{
	public class SmartStringSearchService : ISmartStringSearchService
	{
		private readonly JaroWinkler _jaroWinklerAlgorithm;

		private double Threshold { get; }

		public SmartStringSearchService(double threshold)
		{
			_jaroWinklerAlgorithm = new JaroWinkler();
			Threshold = threshold;
		}

		public bool Similar(string s1, string s2)
		{
			var score = _jaroWinklerAlgorithm.Similarity(s1, s2);

			return score > Threshold;
		}

		public double GetSimilarity(string s1, string s2) => _jaroWinklerAlgorithm.Similarity(s1, s2);
	}
}
