﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Storage.Monitoring;
using Newtonsoft.Json;
using SalonUltimate.Foundation.Models;
using SalonUltimate.Foundation.Services.Interfaces;

namespace SalonUltimate.Foundation.Services.Implementation
{
    public class BackgroundJobsService : IBackgroundJobsService
    {
        public string FireAndForget(Expression<Action> action)
        {
            var jobId = BackgroundJob.Enqueue(action);
            return jobId;
        }

        public string FireAndForgetWithDelay(Expression<Action> action, TimeSpan delay)
        {
            var jobId = BackgroundJob.Schedule(action, delay);
            return jobId;
        }

        public JobInfo<T> CheckJobStatus<T>(string jobId)
        {
            var connection = JobStorage.Current.GetConnection();
            var jobData = connection.GetJobData(jobId);

            if (jobData == null)
            {
                return new JobInfo<T>
                {
                    Status = JobStatus.NotFound
                };
            }

            var stateName = jobData.State;

            switch (stateName)
            {
                case "Enqueued":
                case "Scheduled":
                case "Processing":
                    return new JobInfo<T>
                    {
                        Status = JobStatus.InProgress
                    };

                case "Succeeded":
                    var api = JobStorage.Current.GetMonitoringApi();
                    var succeededJobDto = api.SucceededJobs(0, int.MaxValue).FirstOrDefault(job => job.Key == jobId).Value;
                    if (succeededJobDto != null)
                    {
                        var result = JsonConvert.DeserializeObject<T>(succeededJobDto.Result.ToString());

                        return new JobInfo<T>()
                        {
                            Status = JobStatus.Completed,
                            Result = result
                        };
                    }
                    break;

                case "Failed":
                    return new JobInfo<T>()
                    {
                        Status = JobStatus.Failed
                    };
                //case "Deleted":
                //case "Awaiting":
            }

            return new JobInfo<T>
            {
                Status = JobStatus.NotFound
            };
        }

        public void AddRecurring(Expression<Action> action, Func<string> cronExpression)
        {
            RecurringJob.AddOrUpdate(action, cronExpression);
        }

        public void AddRecurring<T>(Expression<Func<T, Task>> action, Func<string> cronExpression)
        {
            RecurringJob.AddOrUpdate(action, cronExpression);
        }
    }
}
