﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

namespace SalonUltimate.Foundation.Services.Implementation
{
	public abstract class BaseHostedService : IHostedService
	{
		private Task _executingTask;
		private readonly CancellationTokenSource _stoppingCancellationTokenSource = new CancellationTokenSource();
		private readonly TimeSpan _tickTime;

		protected BaseHostedService(TimeSpan tickTime)
		{
			_tickTime = tickTime;
		}

		public virtual Task StartAsync(CancellationToken cancellationToken)
		{
			// Store the task we're executing
			_executingTask = ExecuteAsync(_stoppingCancellationTokenSource.Token);
 
			// If the task is completed then return it,
			// this will bubble cancellation and failure to the caller
			if (_executingTask.IsCompleted)
			{
				return _executingTask;
			}
 
			// Otherwise it's running
			return Task.CompletedTask;
		}
 
		public virtual async Task StopAsync(CancellationToken cancellationToken)
		{
			// Stop called without start
			if (_executingTask == null)
			{
				return;
			}
 
			try
			{
				// Signal cancellation to the executing method
				_stoppingCancellationTokenSource.Cancel();
			}
			finally
			{
				// Wait until the task completes or the stop token triggers
				await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
			}
		}
 
		protected virtual async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			//stoppingToken.Register(() => _logger.LogDebug($"Background task is stopping."));
 
			do
			{
				await Process();
 
				await Task.Delay(_tickTime, stoppingToken); //5 seconds delay
			}
			while (!stoppingToken.IsCancellationRequested);
		}
 
		protected abstract Task Process();
	}
}
