﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Linq;
using SalonUltimate.Foundation.Services.Interfaces;

namespace SalonUltimate.Foundation.Services.Implementation
{

	public class UuidGeneratingService : IUuidGeneratingService
	{
		/// <summary> Store independent tables. </summary>
		private readonly string[] _storeIndependentTables =
		{
			"astbrands",
			"astregiongroups",
			"astregions",
			"aststores",
			"cfgbenchmarks",
			"cfgbmipayrollbonuses",
			"cfgclientattributes",
			"cfgcustomcolumns",
			"cfgdiscountreasons",
			"cfgloyaltylevels",
			"cfgmembershipalternatives",
			"cfgmembershipbenefits",
			"cfgmembershipdetails",
			"cfgmembershippriceexceptions",
			"cfgmembershipprices",
			"cfgmemberships",
			"cfgoccupations",
			"cfgothertimetypes",
			"cfgpackageitems",
			"cfgpackages",
			"cfgpayperiods",
			"cfgpayrolllevelcommissions",
			"cfgpayrolllevels",
			"cfgpromotionitems",
			"cfgpromotions",
			"cfgreferraltypes",
			"cfgregionpackages",
			"cfgregionpromotions",
			"cfgremovalreasontypes",
			"cfgservicetemplates",
			"cfgstorememberships",
			"cfgstorepackages",
			"cfgzipcodes",
			"dtaaddresses",
			"dtaattachments",
			"dtacentralemployees",
			"dtaclientcards",
			"dtaclientforms",
			"dtaclientformstores",
			"dtaclientnotes",
			"dtaclientphones",
			"dtaclientphotogroups",
			"dtaclientphotos",
			"dtaclients",
			"dtacustomcolumnlistitems",
			"dtacustomcolumnvalues",
			"dtaformdocuments",
			"dtaformfields",
			"dtaformulas",
			"dtainternalconversations",
			"dtainternalmailreplies",
			"dtainternalmails",
			"dtainventoryitemtemplates",
			"dtaloyaltyaccounts",
			"dtamailpreferences",
			"dtasoapnotes",
			"dtausers",
			"lkpclientattributeassignments",
			"lkpclientformsservicetemplates",
			"lkpmembershipdetailspromotions",
			"lkprolestasks",
			"lkpstoresregions",
			"txnbenefitaudits",
			"txnbenefitgroupstatuses",
			"txnbenefitperiods",
			"txnbenefitshareaudits",
			"txnbenefitstatuses",
			"txncontractaudits",
			"txncontractbillaudits",
			"txncontractbills",
			"txncontractcharges",
			"txncontractgroups",
			"txncontracts",
			"txncontracttipaudits",
			"txncorporateorderitems",
			"txnfieldresponses",
			"txnformresponses",
			"txngiftcertificates",
			"txnpurchaseordergroups",
			"txnstoretransferitems",
			"txnstoretransfers",
			"utlroles",
			"utltasks",
		};
		
		/// <summary> Tables that may have store_id optionally </summary>
		private readonly string[] _tablesAllowedWithIdAsKey =
		{
			"appusers",
			"cfgsettings",
			"txnclientmergelogs",
			"cfgcustomtemplatetypes",
		};

		public string GenerateUuid(string deploymentUuid, string tableName, string id, string storeId = null)
		{
			var tableKey = _storeIndependentTables.Contains(tableName.ToLower()) ? id : storeId;
			if (tableKey == null)
			{
				if (!_tablesAllowedWithIdAsKey.Contains(tableName.ToLower()))
					throw new ArgumentException($"You should provide storeId for {tableName} table");

				tableKey = id;
			}

			return uuid_v5(deploymentUuid,
				tableName.ToLower() + "|" + (tableKey ?? " - ") + "|" + id);
		}

		private static string UuidFromMd5(string Deployment)
		{
			var md5Hash = CreateMd5(Deployment);
			var result = md5Hash.Substring(0, 8) + "-" + md5Hash.Substring(8, 4) + "-" + md5Hash.Substring(12, 4) +
			             "-" + md5Hash.Substring(16, 4) + "-" + md5Hash.Substring(md5Hash.Length - 12);
			return result;
		}

		public static string CreateMd5(string input)
		{
			using (MD5 md5 = MD5.Create())
			{
				var inputBytes = Encoding.ASCII.GetBytes(input);
				byte[] hashBytes = md5.ComputeHash(inputBytes);

				StringBuilder sb = new StringBuilder();
				foreach (var @byte in hashBytes)
				{
					sb.Append(@byte.ToString("x2"));
				}

				return sb.ToString().ToLower();
			}
		}

		private static string uuid_v5(string ns, string name)
		{
			var nsBin = UuidToBin(ns);
			var nsBinBytes = nsBin.ToByteArray().Reverse();
			if (nsBinBytes.First() == byte.Parse("0"))
			{
				nsBinBytes = nsBinBytes.Skip(1);
			}

			var namesBytes = Encoding.GetEncoding("latin1").GetBytes(name);
			var nsBinConcatName = nsBinBytes.Concat(namesBytes).ToArray();
			string nsBinConcatNameString = System.Text.Encoding.GetEncoding("latin1").GetString(nsBinConcatName);
			var hashedValue = Sha1(nsBinConcatNameString);

			var timeHi = hashedValue.Substring(12, 4);
			var timeHiInt = Convert.ToInt32(timeHi, 16) & 0x0fff;
			timeHiInt = timeHiInt & ~(0xf000);
			timeHiInt = timeHiInt | (5 << 12);

			var clockSeqHi = hashedValue.Substring(16, 2);
			var clockSeqHiInt = Convert.ToInt32(clockSeqHi, 16);
			clockSeqHiInt = clockSeqHiInt & 0x3f;
			clockSeqHiInt = clockSeqHiInt & ~(0xc0);
			clockSeqHiInt = clockSeqHiInt | 0x80;

			var timeLow = hashedValue.Substring(0, 8);
			var timeMid = hashedValue.Substring(8, 4);

			var timeHiAndVersion = timeHiInt.ToString("x").PadLeft(4, '0');
			var clockSeqHiAndReserved = clockSeqHiInt.ToString("x").PadLeft(2, '0');
			var clockSeqLow = hashedValue.Substring(18, 2);
			var node = hashedValue.Substring(20, 12).PadLeft(12, '0');

			var clockSeq = clockSeqHiAndReserved + clockSeqLow;
			var uuidStr = string.Join("-", timeLow, timeMid, timeHiAndVersion, clockSeq, node);

			return uuidStr;
		}


		static string Sha1(string input)
		{
			using (SHA1Managed sha1 = new SHA1Managed())
			{
				var hash = sha1.ComputeHash(Encoding.GetEncoding("latin1").GetBytes(input));
				var sb = new StringBuilder(hash.Length * 2);
				foreach (byte b in hash)
				{
					sb.Append(b.ToString("x2"));
				}
				return sb.ToString();
			}
		}

		private static BigInteger UuidToBin(string s)
		{
			return Unhex(s.Substring(0, 8) + s.Substring(9, 4) + s.Substring(14, 4) + s.Substring(19, 4) +
			             s.Substring(s.Length - 12, 12));
		}

		private static BigInteger Unhex(string hex)
		{	//otherwise bigint decide that it is negative number
			hex = "0" + hex;
			return BigInteger.Parse(
				hex, System.Globalization.NumberStyles.AllowHexSpecifier);
		}
	}
}
