﻿using System.Linq;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Metrics;
using SalonUltimate.Foundation.Services.Interfaces;
using SalonUltimate.Foundation.ViewModels;

namespace SalonUltimate.Foundation.Services.Implementation
{
    public class ApplicationInsightsService : IApplicationInsightsService
	{
		private readonly TelemetryClient _telemetryClient = new TelemetryClient();

		public void TrackEvent(ApplicationInsightsCustomEventVM model)
		{
			_telemetryClient.TrackEvent(model.Name, model.Properties, model.Metrics);
		}

		public void TrackBadDataEvent(ApplicationInsightsCustomEventVM model)
		{
			model.Properties.Add("DataOperationName", model.Name);
			_telemetryClient.TrackEvent("ZenithBadDataSafeguard", model.Properties, model.Metrics);
		}

		public void TrackException(ApplicationInsightsExceptionEventVM model)
		{
			_telemetryClient.TrackException(model.Exception, model.Properties, model.Metrics);
		}

		public void TrackMetric(ApplicationInsightsMetricEventVM model)
		{
			if (model.UsePreAggregation)
			{
				var metricIdentifier = new MetricIdentifier(MetricIdentifier.DefaultMetricNamespace, model.Name, model.Properties?.Keys.ToArray());

				var metric = _telemetryClient.GetMetric(metricIdentifier);

				if (metric.TryGetDataSeries(out var series, true, model.Properties?.Values.ToArray()))
				{
					series.TrackValue(model.Value);
				}
			}
			else
			{
				_telemetryClient.TrackMetric(model.Name, model.Value, model.Properties);
			}
		}

		public void Flush()
		{
			_telemetryClient.Flush();
		}
	}
}
