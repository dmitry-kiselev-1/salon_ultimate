﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Services.Interfaces;

namespace SalonUltimate.Foundation.Services.Implementation
{
	public sealed class CryptoService : ICryptoService
	{
		private const string Password = "sdjfhaldfy2w479rbfayw8o7v6a3784nc7awyvcuibnayuigrbvawiuezxcm";
		private const string Salt = "sdjklfjasljkfhtw3e4u9nhc7q346c5v78o63478qw3bvgc589qabcmhjcvrB";
		private const int Iterations = 1000;
		private const int HashSize = 32;
		private const string IvString = "ab73d32579af23a41948ca5290fa2732";
		private const int BlockSize = 128;
		private const int PaddingBlockSize = 16;
		private const CipherMode Mode = CipherMode.CBC;

		public string Encrypt(string dataToEncrypt)
		{
			var iv = HexStringToBytes(IvString);
			var key = GetPbkdf2Bytes();
			var withPadding = AddPadding(dataToEncrypt);
			var crypt = McryptEncrypt(withPadding, key, iv);
			var result = Convert.ToBase64String(crypt);
			return result;
		}

		public string Decrypt(string dataToDecrypt, string errorMsg)
		{
			try
			{
				var iv = HexStringToBytes(IvString);
				var key = GetPbkdf2Bytes();

				var decoded = Convert.FromBase64String(dataToDecrypt);
				var decrypted = McryptDecrypt(decoded, key, iv);
				var decryptedString = Encoding.ASCII.GetString(decrypted);

				var result = RemovePadding(decryptedString);
				return result;
			}
			catch
			{
				throw new ZenithValidationException(errorMsg);
			}
		}

		private string AddPadding(string str)
		{
			var pad = PaddingBlockSize - (str.Length % PaddingBlockSize);
			string result = str + string.Concat(RepeatString(((char)pad).ToString(), pad));
			return result;
		}

		private string RemovePadding(string decryptedString)
		{
			var strLen = decryptedString.Length;
			var padding = (int)(decryptedString[strLen - 1]);

			if (padding > PaddingBlockSize || padding > strLen)
				return decryptedString;

			var result = decryptedString.Substring(0, strLen - padding);
			return result;
		}

		private IEnumerable<string> RepeatString(string str, int times)
		{
			for (int i = 0; i < times; i++)
			{
				yield return str;
			}
		}

		private byte[] HexStringToBytes(string hexValues)
		{
			if (hexValues.Length % 2 != 0)
				throw new ArgumentException("Hexadecimal string is in incorrect format");

			int arrLen = hexValues.Length / 2;
			byte[] result = new byte[arrLen];

			for (int i = 0; i < arrLen; i++)
			{
				result[i] = Convert.ToByte(hexValues.Substring(i * 2, 2), 16);
			}

			return result;
		}

		private byte[] GetPbkdf2Bytes()
		{
			using (var pbkdf2 = new Rfc2898DeriveBytes(Password, Encoding.ASCII.GetBytes(Salt), Iterations))
			{
				var result = pbkdf2.GetBytes(HashSize);
				return result;
			}
		}

		private byte[] McryptEncrypt(string value, byte[] key, byte[] iv)
		{
			var plainTextBytes = Encoding.ASCII.GetBytes(value);

			using (var rijndael = new RijndaelManaged { Key = key, Mode = Mode, BlockSize = BlockSize, IV = iv })
			{
				var encryptor = rijndael.CreateEncryptor(rijndael.Key, rijndael.IV);
				using (var ms = new MemoryStream())
				using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
				{
					cs.Write(plainTextBytes, 0, plainTextBytes.Length);
					cs.FlushFinalBlock();
					byte[] bytes = ms.ToArray();
					return bytes;
				}
			}
		}

		private byte[] McryptDecrypt(byte[] dataToDecrypt, byte[] key, byte[] iv)
		{
			using (var rijndael = new RijndaelManaged { Key = key, Mode = Mode, BlockSize = BlockSize, IV = iv })
			{
				var decryptor = rijndael.CreateDecryptor(rijndael.Key, rijndael.IV);
				using (var ms = new MemoryStream(dataToDecrypt))
				using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
				using (var decrypted = new MemoryStream())
				{
					int data;
					while ((data = cs.ReadByte()) != -1)
						decrypted.WriteByte((byte)data);

					decrypted.Position = 0;
					var result = decrypted.ToArray();
					return result;
				}
			}
		}
	}
}
