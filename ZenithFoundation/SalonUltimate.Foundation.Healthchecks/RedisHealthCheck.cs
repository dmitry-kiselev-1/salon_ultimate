﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System;
using System.Threading;
using System.Threading.Tasks;
using SalonUltimate.Foundation.Redis;

namespace SalonUltimate.Foundation.Healthchecks
{
	[Obsolete("This healthcheck is obsolete and will be removed (or replaced) in a future version.")]
	public class RedisHealthCheck : IHealthCheck
	{
		private readonly IRedisService _redisService;

		public RedisHealthCheck(IRedisService redisService)
		{
			_redisService = redisService ?? throw new InvalidOperationException("RedisService is not implemented");
		}

		public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default(CancellationToken))
		{
			(var success, var message) = await _redisService.CheckRedisConnection();

			if (success)
				return HealthCheckResult.Healthy(message);

			return HealthCheckResult.Unhealthy(message);
		}
	}
}
