﻿using Microsoft.AspNetCore.Hosting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.Models;

namespace SalonUltimate.Foundation.Tests
{
	public class VersionHelperTests
	{
		[Fact]
		public void GetVersion_DeploymentInfoIsSet()
		{
			var metrics = GetMetrics();
			var versionHelper = new VersionHelper(new Mock<IHostingEnvironment>().Object, metrics);
			var deploymentInfo = versionHelper.GetDeploymentInfo();

			AssertDeploymentInfo(metrics, deploymentInfo);
		}

		protected AppInsightsMetrics GetMetrics()
			=> new AppInsightsMetrics
			{
				Application = "ZenithFoundationTests",
				Environment = "Test",
				Version = "1.0.0.0",
				AdditionalMetrics = new Dictionary<string,string>
				{
					["Branch"] = "develop",
					["BuildDate"] = "2019-01-01",
					["PipelineId"] = "42",
					["JobId"] = "100500"
				}
			};

		protected void AssertDeploymentInfo(AppInsightsMetrics metrics, DeploymentInfo deploymentInfo)
		{
			Assert.NotNull(metrics);
			Assert.NotNull(deploymentInfo);

			Assert.Equal(metrics.Application, deploymentInfo.Application);
			Assert.Equal(metrics.Environment, deploymentInfo.Environment);
			Assert.Equal(metrics.AdditionalMetrics["Branch"], deploymentInfo.Branch);
			Assert.Equal(metrics.AdditionalMetrics["BuildDate"], deploymentInfo.BuildDate);
			Assert.Equal(metrics.AdditionalMetrics["PipelineId"], deploymentInfo.PipelineId);
			Assert.Equal(metrics.AdditionalMetrics["JobId"], deploymentInfo.JobId);
		}
	}
}
