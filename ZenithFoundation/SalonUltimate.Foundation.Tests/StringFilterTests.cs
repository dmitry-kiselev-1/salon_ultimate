﻿using System.Collections.Generic;
using System.Linq;
using Xunit;
using SalonUltimate.Foundation.Helpers.Paginator;

namespace SalonUltimate.Foundation.Tests
{
    public class StringFilterTests
    {
	    private List<SomeObject> objects;

	    [Theory]
		[InlineData("gree black", FilterRules.StartsWith, new[] {2, 3})]
		[InlineData("ous lue", FilterRules.Contains, new[] {3, 4})]
		[InlineData("n k", FilterRules.EndsWith, new[] {2, 3})]
		[InlineData("one two three four", FilterRules.Equals, new[] {1})]
		[InlineData("table", FilterRules.StartsWith, new[] {4})]
		[InlineData("e", FilterRules.Contains, new[] {1, 2, 3, 4})]
	    public void GetSplittedPredicate(string filterValue, FilterRules filterRule, int[] expectedResultIds)
	    {
			this.Seed();
		    var stringFilter = new StringFilter {FilterRule = filterRule, FilterValue = filterValue};
		    var predicate = stringFilter.GetSplittedPredicate<SomeObject>(x => x.Field1, x => x.Field2, x => x.Field3, x => x.Field4);
		    var result = objects.Where(predicate.Compile()).Select(x => x.Id).ToList();
			Assert.Equal(expectedResultIds.Length, result.Count);
		    Assert.Empty(result.Except(expectedResultIds));
	    }

	    private void Seed()
	    {
		    objects = new List<SomeObject>
		    {
				new SomeObject{Id = 1, Field1 = "one", Field2 = "two", Field3 = "three", Field4 = "four"},
				new SomeObject{Id = 2, Field1 = "one", Field2 = "two", Field3 = "green", Field4 = "black"},
				new SomeObject{Id = 3, Field1 = "blue", Field2 = "house", Field3 = "green", Field4 = "black"},
				new SomeObject{Id = 4, Field1 = "blue", Field2 = "house", Field3 = "john", Field4 = "table"},
		    };
	    }

	    private class SomeObject
	    {
		    public int Id { get; set; }
		    public string Field1 { get; set; }
		    public string Field2 { get; set; }
		    public string Field3 { get; set; }
		    public string Field4 { get; set; }
	    }
    }
}
