﻿using System;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Extensions.ApplicationBuilder;

namespace SalonUltimate.Foundation.Tests
{
	/// <summary>
	/// ApplicationBuilderCorsExtensions test
	/// </summary>
	public class ApplicationBuilderCorsExtensionsTests : IDisposable, IClassFixture<ApplicationBuilderCorsExtensionsFixture>
	{
		private readonly ApplicationBuilderCorsExtensionsFixture _fixture;

		/// <summary>
		/// Setup (xUnit.net creates a new instance of the test class for every test)
		/// </summary>
		public ApplicationBuilderCorsExtensionsTests(ApplicationBuilderCorsExtensionsFixture fixture)
		{
			_fixture = fixture;
		}

		[Fact]
		public void GetOrigins_ShouldBeZenithCorsConfigurationMissedOrEmptyExceptionIfCorsConfigurationMissedOrEmpty()
		{
			// Arrange
			var configuration = _fixture.ConfigurationMissedOrEmpty;
			string configurationKey = _fixture.ConfigurationKeyCorrect;

			// Assert 
			Assert.Throws<FoundationCorsConfigurationMissedOrEmptyException>(() =>
			{
				// Act
				var origins = ApplicationBuilderCorsExtensions.GetOrigins(configuration, configurationKey);
			});
		}

		[Fact]
		public void GetOrigins_ShouldBeZenithCorsConfigurationIncorrectExceptionIfCorsConfigurationIncorrect()
		{
			// Arrange
			var configuration = _fixture.ConfigurationJsonFile;
			string configurationKey = _fixture.ConfigurationKeyIncorrect;

			// Assert 
			Assert.Throws<FoundationCorsConfigurationIncorrectException>(() =>
			{
				// Act
				var origins = ApplicationBuilderCorsExtensions.GetOrigins(configuration, configurationKey);
			});
		}

		[Fact]
		public void GetOrigins_ShouldBeStringArrayIfCorsConfigurationCorrect()
		{
			// Arrange
			var configuration = _fixture.ConfigurationJsonFile;
			string configurationKey = _fixture.ConfigurationKeyCorrect;

			// Act
			var origins = ApplicationBuilderCorsExtensions.GetOrigins(configuration, configurationKey);

			// Assert 
			Assert.IsType<string[]>(origins);
		}

		/// <summary>
		/// Clean-up (xUnit.net creates a new instance of the test class for every test)
		/// </summary>
		public void Dispose()
		{
		}
	}

	public class ApplicationBuilderCorsExtensionsFixture : IDisposable
	{
		public string ConfigurationKeyCorrect => "Cors:Origins";
		public string ConfigurationKeyIncorrect => "Cors:OriginsIncorrect";

		public IConfiguration ConfigurationMissedOrEmpty;
		public IConfiguration ConfigurationJsonFile;

		/// <summary>
		/// One-time setup code
		/// </summary>
		public ApplicationBuilderCorsExtensionsFixture()
		{
			this.ConfigurationMissedOrEmpty = new Mock<IConfiguration>().Object;

			this.ConfigurationJsonFile = new ConfigurationBuilder()
				.AddJsonFile("appsettings.json")
				.AddJsonFile("appsettings.local.json", optional: true)
				.Build();
		}

		/// <summary>
		/// One-time clean-up code
		/// </summary>
		public void Dispose()
		{
		}
	}
}
