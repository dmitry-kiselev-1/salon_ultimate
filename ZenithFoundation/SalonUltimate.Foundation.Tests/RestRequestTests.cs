﻿using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using Xunit;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Helpers.Rest;
using SalonUltimate.Foundation.Helpers.Rest.Models;

namespace SalonUltimate.Foundation.Tests
{
	public class TestModel
	{
		public string Hello { get; set; } = "World";
	}

	public class RestRequestTests
	{
		// for MockHttp documentation,
		// see https://github.com/richardszalay/mockhttp

		private const string JsonMediaType = "application/json";
		private const string BaseAddress = "http://my.server";
		private const string Endpoint = "/hello/world";
		private const string FakeJWT = "<some fake jwt>";

		[Fact]
		public async Task RestRequestBuilder_SendsGetRequest_ReceivesAnswer()
		{
			var responseObject = new TestModel();

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(responseObject));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_SendsGetRequestWithAuth_ReceivesAnswerOnlyIfAuthorized()
		{
			var responseObject = new TestModel();

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(responseObject));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var request = requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint);

			await Assert.ThrowsAsync<ZenithExternalResourceException>(() => request.GetAsync<object>());

			request = request
				.WithBearer(FakeJWT);

			var response = await request.GetAsync<TestModel>();
			Assert.Equal(responseObject.Hello, response.Hello);

		}

		[Fact]
		public async Task RestRequestBuilder_SendsProperHeaders_ReceivesHeaderDependentResults()
		{
			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.WithHeaders("CustomHeader", "1")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(new TestModel { Hello = "1" }));

			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.WithHeaders("CustomHeader", "2")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(new TestModel { Hello = "2" }));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var request = requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.WithHeader("CustomHeader", "wrongValue");

			await Assert.ThrowsAsync<ZenithExternalResourceException>(() => request.GetAsync<object>());

			request = requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.WithHeader("CustomHeader", "1");

			var response = await request.GetAsync<TestModel>();
			Assert.Equal("1", response.Hello);

			request = requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.WithHeader("CustomHeader", "2");

			response = await request.GetAsync<TestModel>();
			Assert.Equal("2", response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_SharesHeadersAndAuthorization()
		{
			var responseObject = new TestModel();

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(responseObject));

			// You need to set up the same mock twice for two requests to the same endpoint (that's weird, but...)
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(responseObject));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());

			// first request will save authorization parameters
			var response = await requestBuilder
				.WithSharedClient(BaseAddress)
				.From(Endpoint)
				.WithBearer(FakeJWT)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);

			// test that next clients are the same object
			var client1 = (requestBuilder.WithSharedClient() as ExternalHttpRequest).ShareClient();
			var client2 = (requestBuilder.WithSharedClient(BaseAddress) as ExternalHttpRequest).ShareClient();

			Assert.Same(client1, client2);

			response = await requestBuilder
				.WithSharedClient(BaseAddress)
				.From(Endpoint)
				.WithBearer(FakeJWT)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);

			// try to send unauthorized request

			var request = requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.From(Endpoint);

			await Assert.ThrowsAsync<ZenithExternalResourceException>(() => request.GetAsync<TestModel>());
		}

		[Fact]
		public async Task RestRequestBuilder_MakesPost_ReceivesEcho()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject,
				new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()});

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Post, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.WithContent(serialized)
				.Respond(JsonMediaType, serialized);

			// You need to set up the same mock twice for two requests (that's weird, but...)
			mockHttp
				.Expect(HttpMethod.Post, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.WithContent(serialized)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithBearer(FakeJWT)
				.WithPayload(responseObject)
				.PostAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);

			response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithBearer(FakeJWT)
				.WithSerializedBody(serialized)
				.PostAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_MakesPut_ReceivesEcho()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject,
				new JsonSerializerSettings {ContractResolver = new CamelCasePropertyNamesContractResolver()});

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Put, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.WithContent(serialized)
				.Respond(JsonMediaType, serialized);

			// You need to set up the same mock twice for two requests (that's weird, but...)
			mockHttp
				.Expect(HttpMethod.Put, $"{BaseAddress}{Endpoint}")
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.WithContent(serialized)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithBearer(FakeJWT)
				.WithPayload(responseObject)
				.PutAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);

			response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithBearer(FakeJWT)
				.WithSerializedBody(serialized)
				.PutAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_MakesCallsWithoutBaseAddress()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}")
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings()
				.From(BaseAddress + Endpoint)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_RunSharedRequestInParallel_Succeeeds()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, BaseAddress + Endpoint)
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.Respond(JsonMediaType, serialized);

			mockHttp
				.Expect(HttpMethod.Post, BaseAddress + Endpoint)
				.WithHeaders("Authorization", $"Bearer {FakeJWT}")
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());

			var request1 = requestBuilder
				.WithSharedClient(BaseAddress)
				.From(Endpoint)
				.WithBearer(FakeJWT)
				.GetAsync<TestModel>();

			var request2 = requestBuilder
				.WithSharedClient(BaseAddress)
				.To(Endpoint)
				.WithPayload(responseObject)
				.WithBearer(FakeJWT)
				.PostAsync<TestModel>();

			await Task.WhenAll(request1, request2);

			Assert.Equal(responseObject.Hello, request1.Result.Hello);
			Assert.Equal(responseObject.Hello, request2.Result.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_ForFailedRequests_CanReturnModel()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, BaseAddress + Endpoint)
				.Respond(HttpStatusCode.BadRequest, new StringContent(serialized));
			mockHttp
				.Expect(HttpMethod.Get, BaseAddress + Endpoint)
				.Respond(HttpStatusCode.BadRequest, new StringContent(serialized));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());

			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.ForAnyResponseStatus()
				.From(Endpoint)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);

			// Without "ForAnyResponseStatus" it will throw exception
			var request = requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.From(Endpoint);

			await Assert.ThrowsAsync<ZenithExternalResourceException>(() => request.GetAsync<TestModel>());
		}

		[Fact]
		public async Task RestRequestBuilder_AddsQueryParams_FromSeparateMethods()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var queryParamsEthalon = new KeyValuePair<string, string>[]
			{
				new KeyValuePair<string, string>("hello", "world"),
				new KeyValuePair<string, string>("intParam", "1"),
			};

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, BaseAddress + Endpoint)
				.WithQueryString(queryParamsEthalon)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.WithQueryParameter("hello", "world")
				.WithQueryParameter("intParam", 1)
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_AddsQueryParams_FromObject()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var queryParamsEthalon = new KeyValuePair<string, string>[]
			{
				new KeyValuePair<string, string>("hello", "world"),
				new KeyValuePair<string, string>("intParam", "1"),
			};

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, BaseAddress + Endpoint)
				.WithQueryString(queryParamsEthalon)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.WithQueryParameters(new
				{
					hello = "world",
					intParam = 1
				})
				.GetAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_AddsFormParams_FromSeparateCalls()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var formParamsEthalon = new KeyValuePair<string, string>[]
			{
				new KeyValuePair<string, string>("hello", "world"),
				new KeyValuePair<string, string>("intParam", "1"),
			};

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Post, BaseAddress + Endpoint)
				.WithFormData(formParamsEthalon)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithFormParameter("hello", "world")
				.WithFormParameter("intParam", 1)
				.PostAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_AddsFormParams_FromModel()
		{
			var responseObject = new TestModel();
			var serialized = JsonConvert.SerializeObject(responseObject);

			var formParamsEthalon = new KeyValuePair<string, string>[]
			{
				new KeyValuePair<string, string>("hello", "world"),
				new KeyValuePair<string, string>("intParam", "1"),
			};

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Post, BaseAddress + Endpoint)
				.WithFormData(formParamsEthalon)
				.Respond(JsonMediaType, serialized);

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder
				.WithIsolatedSettings(BaseAddress)
				.To(Endpoint)
				.WithFormParameters(new
				{
					hello = "world",
					intParam = 1
				})
				.PostAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}

		[Fact]
		public async Task RestRequestBuilder_ExecutesDeleteRequest_ReceivesEcho()
		{
			var responseObject = new TestModel();

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Delete, $"{BaseAddress}{Endpoint}")
				.Respond(JsonMediaType, JsonConvert.SerializeObject(responseObject));

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());
			var response = await requestBuilder.WithIsolatedSettings(BaseAddress)
				.From(Endpoint)
				.DeleteAsync<TestModel>();

			Assert.Equal(responseObject.Hello, response.Hello);
		}
	}
}
