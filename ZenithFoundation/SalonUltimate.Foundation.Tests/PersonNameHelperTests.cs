﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.Validation;

namespace SalonUltimate.Foundation.Tests
{
	public class PersonNameHelperTests
	{
		[Theory]
		[InlineData("two  spaces--")]
		[InlineData("not^allowed_characters")]
		[InlineData("two--dashes")]
		public void ValidateNameField_NameIsInvalid_AddsErrorToResult(string name)
		{
			var helper = new PersonNameHelper();
			var validationResult = new ValidationResult();

			helper.ValidateNameField(validationResult, name);

			Assert.False(validationResult.IsValid);
			Assert.Single(validationResult.Issues);
			Assert.False(validationResult.Issues[0].CanBeSkipped);
		}

		[Theory]
		[InlineData("sam's the first-name")]
		[InlineData("my super long surname consisting of 8 words")]
		[InlineData("my-middle-name")]
		public void ValidateNameField_NameIsValid_NothingHappens(string name)
		{
			var helper = new PersonNameHelper();
			var validationResult = new ValidationResult();

			helper.ValidateNameField(validationResult, name);

			Assert.True(validationResult.IsValid);
			Assert.Empty(validationResult.Issues);
		}

		[Theory]
		[InlineData("              sam sam   ", "Sam Sam")]
		[InlineData("smith d'connor   ", "Smith D'Connor")]
		[InlineData(" markovich-rabinovich        ", "Markovich-Rabinovich")]
		public void NormalizePersonName_NameGetsNormalized(string name, string expectedName)
		{
			var helper = new PersonNameHelper();

			var result = helper.NormalizePersonName(name);

			Assert.Equal(expectedName, result);
		}
	}
}
