﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using SalonUltimate.Foundation.Services.Implementation;
using SalonUltimate.Foundation.Services.Interfaces;

namespace SalonUltimate.Foundation.Tests
{
	public class CryptoServiceTests 
	{
		private readonly ICryptoService _cryptoService;

		public CryptoServiceTests()
		{
			_cryptoService = new CryptoService();
		}


		[Fact]
		public void Crypto_TwoWay()
		{
			const string text = "text123";

			var encryptedText = _cryptoService.Encrypt(text);
			var decryptedText = _cryptoService.Decrypt(encryptedText);

			Assert.Equal(text, decryptedText);
		}

		[Fact]
		public void Crypto_Decrypt_from_ZPos_Settings()
		{
			const string encryptedAccount = "oZVLl3mglJP1UGfRxHPwCA==";
			const string encryptedPassword = "C8bDj7ZEgSk1Ka38OKD52BzmdU8QOAyYPkC/vUmVtns=";

			var account = _cryptoService.Decrypt(encryptedAccount);
			var password = _cryptoService.Decrypt(encryptedPassword);
		}
	}
}
