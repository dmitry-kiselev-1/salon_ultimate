﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Helpers.BadData;

namespace SalonUltimate.Foundation.Tests
{
	public class DataSafeguardTests
	{
		// dummy data for distinguishing results and illustraction
		private const int CorrectResponse = 42;
		private const int CatchedResponse = 24;
		private const int DuplicatingKey = 1;

		[Fact]
		public void DataSafeguard_SyncronousMethods_CatchesExceptions()
		{
			// CASE 1. Catch natural dictionary exception.

			var dict = new Dictionary<int, int>() { { DuplicatingKey, 1 } };
			var result = DataSafeguard<int>
				.ForOperation("AddIntToDict")
				.Try(() => // normal flow of getting data
				{
					dict.Add(DuplicatingKey, 2);
					return CorrectResponse;
				})
				.ForBadData(() => // flow for getting substitution for bad data
				{
					dict.Add(2, 2);
					return CatchedResponse;
				})
				.Catch<ArgumentException>()
				.Catch<NullReferenceException>() // an example of catching different types of exceptions.
				.Get();

			Assert.Equal(CatchedResponse, result);
			Assert.Contains(2, dict.Keys);

			// CASE 2. Catch div by 0 exception by using multiple exception types in Catch().

			int zero = 0;
			var result2 = DataSafeguard<int>
				.ForOperation("AddIntToDict")
				.Try(() =>
				{
					dict.Add(DuplicatingKey, 2 / zero);
					return CorrectResponse;
				})
				.ForBadData(() =>
				{
					dict.Add(3, 2);
					return CatchedResponse;
				})
				.Catch(typeof(ArgumentException), typeof(NullReferenceException), typeof(DivideByZeroException))
				.Get();

			Assert.Equal(CatchedResponse, result2);

			// CASE 3. An exception thrown in ForBadData should bubble up.

			Assert.Throws<DivideByZeroException>(() => DataSafeguard<int>
				.ForOperation("AddIntToDict3")
				.Try(() => throw new NullReferenceException())
				.ForBadData(() => 2 / zero)
				.Catch<NullReferenceException>()
				.Get());

			// CASE 4. The exception which type is not specified in Catch() should bubble up as well.

			Assert.Throws<DivideByZeroException>(() => DataSafeguard<int>
				.ForOperation("AddIntToDict3")
				.Try(() => throw new DivideByZeroException())
				.ForBadData(() => CatchedResponse)
				.Catch<NullReferenceException>()
				.Get());

			// CASE 5. For serious cases we should just throw a special BadData exception instead of making some workarounds in the code.

			var exception = Assert.Throws<ZenithBadDataException>(() => DataSafeguard<int>
				.ForOperation("Division")
				.Try(() => throw new DivideByZeroException())
				.ForBadDataThrow()
				.Catch<DivideByZeroException>()
				.Get());

			Assert.Equal("Division", exception.OperationName);
			Assert.NotNull(exception.InnerException);
			Assert.IsType<DivideByZeroException>(exception.InnerException);

			// CASE 6. For serious cases, if the exception wasn't catched, we also should preserve original exception instead of BadData one.

			Assert.Throws<DivideByZeroException>(() => DataSafeguard<int>
				.ForOperation("Division")
				.Try(() => throw new DivideByZeroException())
				.ForBadDataThrow()
				.Catch<NullReferenceException>() // we do not expect DivideByZero exception here, so it will bubble up and ZenithBadData will not be thrown
				.Get());
		}

		[Fact]
		public async Task AsyncDataSafeguard_CatchesExceptions()
		{
			var result = await AsyncDataSafeguard<int>
				.ForOperation("AsyncOperation1")
#pragma warning disable CS1998 // we can't use Task.FromException because of delegate's return type: Task<int>
				.Try(async () => throw new DivideByZeroException())
#pragma warning restore CS1998
				.ForBadData(() => Task.FromResult(CatchedResponse))
				.Catch<DivideByZeroException>()
				.Get();

			Assert.Equal(CatchedResponse, result);

			await Assert.ThrowsAsync<ZenithBadDataException>(() => AsyncDataSafeguard<int>
				.ForOperation("Division")
#pragma warning disable CS1998
				.Try(async () => throw new DivideByZeroException())
#pragma warning restore CS1998
				.ForBadDataThrow()
				.Catch<DivideByZeroException>()
				.Get());

		}
	}
}
