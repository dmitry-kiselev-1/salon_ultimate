﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Xunit;
using SalonUltimate.Foundation.Validation.Reflection;
using SalonUltimate.Foundation.Validation.Reflection.Attributes;
using SalonUltimate.Foundation.Validation.Reflection.Validators;

namespace SalonUltimate.Foundation.Tests
{
	public class ModelTraverseValidatorTests
    {
        [Fact]
        public void ModelTraverseValidator_ValidatesAllRules()
        {
            var validator = new ModelTraverseValidator(
                new CollectionsWithoutNullsValidator(),
                new EnumInRangeValidator(),
                new DateTimeInRangeValidator(),
                new RequiredPropertyValidator());

			var model = new DummyModelWithRequiredFields
			{
				Abc = 5,
				Nested = null,
				Nullable = null,
				Str = null,
				Age = DummyEnum.NULL,
				GoodAge = DummyEnum.SomeValue,
				BadDate = new DateTime(1753, 1, 1),
				GoodDate = new DateTime(1800, 1, 2),
				BadNullableDate = new DateTime(1600, 1, 1),
				GoodNullableDate = null,
                NullList = null,
                NestedList = new List<NestedModelWithRequiredFields> {
                    null,
                    new NestedModelWithRequiredFields() { NestedStr = null },
                    new NestedModelWithRequiredFields { NestedStr = "aaa" }
                },
                GoodList = new List<NestedModelWithRequiredFields>()
            };

            var modelState = validator.GetValidState(model);
            Assert.False(modelState.IsValid);

            Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.Str)));
            Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.Nested)));
            Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.NestedList)));
            Assert.True(modelState.ContainsKey(nameof(NestedModelWithRequiredFields.NestedStr)));
            Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.Age)));
            Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.BadDate)));
			Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.BadNullableDate)));
			Assert.True(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.NullableEnumPseudoNull)));

            Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.Nullable)));
            Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.NullableEnum)));
            Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.GoodList)));
            Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.GoodAge)));
            Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.GoodDate)));
			Assert.False(modelState.ContainsKey(nameof(DummyModelWithRequiredFields.GoodNullableDate)));
		}

        [Fact]
        public void ValidateJObject()
        {
            var model = JObject.Parse("{\"date\":\"2019-04-04T00:00:00\",\"items\":[{\"appointmentId\":763410,\"employeeId\":384,\"roomId\":null,\"roomOrdinal\":null,\"resourceId\":null,\"resourceOrdinal\":null,\"fromTime\":\"12:30:00\",\"toTime\":\"14:15:00\",\"isFirstAvailable\":false}]}");
            var validator = new ModelTraverseValidator(
                    new CollectionsWithoutNullsValidator(),
                    new EnumInRangeValidator(),
                    new DateTimeInRangeValidator(),
                    new RequiredPropertyValidator());

            var modelState = validator.GetValidState(model);
            Assert.True(modelState.IsValid);
        }

		[Fact]
		public void TraverseModel_DoNotValidateHiddenProperties()
		{
			// The idea of this test is to make sure that validator does not take hidden properties into account.
			// Hidden properties are ones that overloaded by NEW keyword: public new DifferentType Property
			// in the case of hidden properties, they stay not set and are treated as missing by Required validator, which is not correct.

			var model = new DummyDerivedType() { Field = DummyEnum.NULL, GoodField = "abc" };

			// Check that validator does not validate hidden string-typed property Field
			var validator = new ModelTraverseValidator(new RequiredPropertyValidator());
			var modelState = validator.GetValidState(model);
			Assert.False(modelState.ContainsKey(nameof(DummyBaseType.Field)));
			Assert.True(modelState.IsValid);

			// Check that validator validates enum-typed property Field instead, if EnumInRangeValidator is specified.
			validator = new ModelTraverseValidator(new RequiredPropertyValidator(), new EnumInRangeValidator());
			modelState = validator.GetValidState(model);
			Assert.True(modelState.ContainsKey(nameof(DummyBaseType.Field)));
			
			Assert.False(modelState.IsValid);
		}

		[Fact]
		public void TimeOfDayValiator_ValidatesTimeSpans()
		{
			var validator = new ModelTraverseValidator(new TimeOfDayValidator());

			var model = new TimeSpanContainer { Time = TimeSpan.FromHours(25), NullableTime = null,  TimeOfDay = TimeSpan.FromHours(13), NullableTimeOfDay = null };

			var modelState = validator.GetValidState(model);
			Assert.True(modelState.IsValid);

			model.TimeOfDay = TimeSpan.FromSeconds(-1);
			model.NullableTimeOfDay = TimeSpan.Parse("24:00:00");

			modelState = validator.GetValidState(model);
			Assert.False(modelState.IsValid);
			Assert.True(modelState.ContainsKey(nameof(TimeSpanContainer.TimeOfDay)));
			Assert.True(modelState.ContainsKey(nameof(TimeSpanContainer.NullableTimeOfDay)));
		}

		[Fact]
		public void TimeOfDayValidator_ValidateTurnAway()
		{
			var validator = new ModelTraverseValidator(new TimeOfDayValidator());

			var model = JsonConvert.DeserializeObject<TurnAway>(@"{
  'date': '2019-07-12T14:16:59.265Z',
  'reasonCode': 1,
  'reason': 'string',
  'myClientId': 67582,
  'isAppointmentBookTurnAway': true,
  'services': [
    {
      'fromTime': '23:45:00',
      'toTime': '24:15:00',
      'myServiceId': 13,
      'myEmployeeId': 2
    },
	{
      'fromTime': '23:40:00',
      'toTime': '24:10:00',
      'myServiceId': 13,
      'myEmployeeId': 2
    }
  ]
}");

			var modelState = validator.GetValidState(model);
			Assert.False(modelState.IsValid);

			Assert.True(modelState.ContainsKey(nameof(TurnAwayServiceInputModel.ToTime)));
		}



		private class TurnAway
		{
			public List<TurnAwayServiceInputModel> Services { get; set; }
		}

		private class TurnAwayServiceInputModel
		{
			[TimeOfDay]
			public TimeSpan? FromTime { get; set; }
			[TimeOfDay]
			public TimeSpan? ToTime { get; set; }

			public int? MyServiceId { get; set; }

			public int? MyEmployeeId { get; set; }
		}
	}



    #region Testable types

    public enum DummyEnum
    {
        NULL = -1,
        SomeValue = 1
    }

    public class DummyModelWithRequiredFields
    {
        [Required]
        public int Abc { get; set; }

        [Required]
        public string Str { get; set; }

        public DummyEnum Age { get; set; }
        public DummyEnum GoodAge { get; set; }
        public DummyEnum? NullableEnum { get; set; } = null;
        public DummyEnum? NullableEnumPseudoNull { get; set; } = DummyEnum.NULL;

        public DateTime BadDate { get; set; }
        public DateTime GoodDate { get; set; }
		public DateTime? BadNullableDate { get; set; }
		public DateTime? GoodNullableDate { get; set; }

        [Required]
        public NestedModelWithRequiredFields Nested { get; set; }

        public List<NestedModelWithRequiredFields> NestedList { get; set; }
        public List<object> NullList { get; set; }

        public List<NestedModelWithRequiredFields> GoodList { get; set; }

        public string Nullable { get; set; }
    }

    public class NestedModelWithRequiredFields
    {
        [Required]
        public string NestedStr { get; set; }
    }

	public class DummyBaseType
	{
		[Required]
		public string Field { get; set; }
		[Required]
		public string GoodField { get; set; }
	}

	public class DummyDerivedType : DummyBaseType
	{
		public new DummyEnum Field { get; set; }
	}

	public class TimeSpanContainer
	{
		[TimeOfDay]
		public TimeSpan TimeOfDay { get; set; }
		[TimeOfDay]
		public TimeSpan? NullableTimeOfDay { get; set; }
		public TimeSpan Time { get; set; }
		public TimeSpan? NullableTime { get; set; }

		[TimeOfDay] // this is wrong usage of this attribute! 
					// It should be put only on TimeSpan, TimeSpan? props
					// So, it will be ignored by TimeOfDayValidator
		public int DummyField { get; set; } = -1;
	}

	

	#endregion
}
