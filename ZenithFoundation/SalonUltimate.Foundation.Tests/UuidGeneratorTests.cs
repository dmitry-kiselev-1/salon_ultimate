﻿using Xunit;
using SalonUltimate.Foundation.Services.Implementation;

namespace SalonUltimate.Foundation.Tests
{

	public class UuidGeneratorTests
	{
		[Fact]
		public void ResultShouldBeEqualToResultFromBase()
		{
			// Arrange
			var generator = new UuidGeneratingService();
			var deploymentId = "e0aed83f-fe78-4978-bbc3-553f4b37be9d";
			var tableName = "tabletest";

			// Act
			var firstResult = generator.GenerateUuid(deploymentId, tableName, "1", "11");
			var secondResult = generator.GenerateUuid(deploymentId, tableName, "2", "12");
			var thirdResult = generator.GenerateUuid(deploymentId, tableName, "3", "13");

			// Assert 
			Assert.Equal("93f1583c-73f4-514f-95a3-90c2bdd3dd39", firstResult);
			Assert.Equal("0aec1643-57a4-5ead-8f29-6ff59faa0f52", secondResult);
			Assert.Equal("17bd9826-0901-5f9b-92f5-0e0e20352a91", thirdResult);
		}

	}
}
