﻿using RichardSzalay.MockHttp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.Foundation.Tests
{
	public class RestApiHelperTests
	{
		[Fact]
		public async Task RestApiHelper_WhenExternalException_ProvidesAllInformation()
		{
			const string errorMessage = "This is an internal error message";

			var mockHttp = new MockHttpMessageHandler();
			mockHttp
				.Expect(HttpMethod.Get, "http://example.com/test")
				.Respond(HttpStatusCode.InternalServerError, "text/plain", errorMessage);

			var client = mockHttp.ToHttpClient();
			var httpClientHandler = new HttpClientHandler();

			var restHelper = new TestRestApiHelper(httpClientHandler, client);

			var ex = await Assert.ThrowsAsync<ZenithExternalResourceException>(() => restHelper.GetStringAsync("http://example.com/test"));
			Assert.Equal("500 Internal Server Error", ex.SystemMessage);
			Assert.Contains(errorMessage, ex.ZenithDebugInfo.ToString());

		}
	}
}
