﻿using Newtonsoft.Json;
using RichardSzalay.MockHttp;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using SalonUltimate.Foundation.Helpers.Rest;

namespace SalonUltimate.Foundation.Tests
{
	public class RestRequestParallelTests
	{
		// for MockHttp documentation,
		// see https://github.com/richardszalay/mockhttp

		private const string JsonMediaType = "application/json";
		private const string BaseAddress = "http://my.server";
		private const string Endpoint = "/hello/world";

		[Fact]
		public async Task ParallelRequest_SharesSomeSettingsAndDistinguishesOthers()
		{
			var responseObjectList = new List<TestModel>
			{
				new TestModel { Hello = "1" },
				new TestModel { Hello = "2" },
				new TestModel { Hello = "3" },
			};

			string customHeaderKey = "X-Custom-Header", customHeaderValue = "Custom-Value";

			var mockHttp = new MockHttpMessageHandler();
			foreach (var mock in responseObjectList)
			{
				mockHttp
					.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}/{mock.Hello}")
					.WithHeaders(customHeaderKey, customHeaderValue)
					.Respond(JsonMediaType, JsonConvert.SerializeObject(mock));
			}

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());

			var results = await requestBuilder.WithIsolatedSettings(BaseAddress)
				.InParallel(r => responseObjectList.Select(x => r.From($"{Endpoint}/{x.Hello}")))
				// Here we're adding a new header to all the requests parallelized in the previous line.
				.WithHeader(customHeaderKey, customHeaderValue)
				.GetAsync<TestModel>();

			Assert.Collection(results, 
				x => { Assert.Equal(responseObjectList[0].Hello, x.Hello); Assert.Equal("1", x.Hello); },
				x => { Assert.Equal(responseObjectList[1].Hello, x.Hello); Assert.Equal("2", x.Hello); },
				x => { Assert.Equal(responseObjectList[2].Hello, x.Hello); Assert.Equal("3", x.Hello); });
		}

		[Fact]
		public async Task ParallelRequest_SomeRequestsFail()
		{
			var responseObjectList = new List<TestModel>
			{
				new TestModel { Hello = "1" },
				new TestModel { Hello = "2" },
				new TestModel { Hello = "3" },
			};

			var mockHttp = new MockHttpMessageHandler();
			foreach (var mock in responseObjectList)
			{
				if (mock.Hello == "2") // one request will be failing
				{
					mockHttp
						.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}/{mock.Hello}")
						.Respond(HttpStatusCode.BadGateway, JsonMediaType, JsonConvert.SerializeObject(new { World = "Hello" }));
				}
				else
				{
					mockHttp
						.Expect(HttpMethod.Get, $"{BaseAddress}{Endpoint}/{mock.Hello}")
						.Respond(JsonMediaType, JsonConvert.SerializeObject(mock));
				}
			}

			var requestBuilder = new RestRequestBuilder(() => mockHttp.ToHttpClient());

			var results = await requestBuilder.WithIsolatedSettings(BaseAddress)
				.ForAnyResponseStatus()
				.InParallel(r => responseObjectList.Select(x => r.From($"{Endpoint}/{x.Hello}")))
				.GetAsync<TestModel>();

			Assert.Equal(3, results.Length);
			Assert.Contains(results, x => x.Hello == "World"); // if could not deserialize an error response, will be default value
			Assert.DoesNotContain(results, x => x.Hello == "2"); // this was an error-responding endpoint
		}

	}
}
