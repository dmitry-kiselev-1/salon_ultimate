﻿using System.Collections.Generic;

namespace SalonUltimate.Foundation.ViewModels
{
	public class ApplicationInsightsCustomEventVM
	{
		public string Name { get; set; }
		public Dictionary<string, string> Properties { get; set; }
		public Dictionary<string, double> Metrics { get; set; }
	}
}
