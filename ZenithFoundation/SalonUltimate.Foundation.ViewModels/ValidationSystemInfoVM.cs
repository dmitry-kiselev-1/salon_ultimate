﻿namespace SalonUltimate.Foundation.ViewModels
{
	public class ValidationSystemInfoVM
	{
		public ValidationSystemInfoVM(string message, object info)
		{
			UserMessage = message;
			SystemInfo = info;
		}

		public string UserMessage { get; set; }
		public object SystemInfo { get; set; }

		public override string ToString()
		{
			return UserMessage;
		}
	}
}
