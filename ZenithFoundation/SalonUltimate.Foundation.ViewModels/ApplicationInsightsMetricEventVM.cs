﻿using System.Collections.Generic;

namespace SalonUltimate.Foundation.ViewModels
{
	public class ApplicationInsightsMetricEventVM
	{
		public string Name { get; set; }

		public double Value { get; set; }

		public Dictionary<string, string> Properties { get; set; }

		public bool UsePreAggregation { get; set; }
	}
}
