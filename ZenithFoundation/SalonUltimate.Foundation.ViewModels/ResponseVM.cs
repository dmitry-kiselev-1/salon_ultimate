﻿using System;
using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.ViewModels
{
	public class ResponseVM<T> : BaseResponseVM
	{
		public ResponseVM() : base() { }
		public ResponseVM(ResponseTypes result, string userMessage) : base(result, userMessage) { }
		public ResponseVM(Exception exception, string userMessage = null) : base(exception, userMessage) { }

		public ResponseVM(T data)
		{
			Response = data;
			Result = data != null ? ResponseTypes.Success : ResponseTypes.NotFound;
		}

		public T Response { get; set; }
	}
}
