﻿namespace SalonUltimate.Foundation.ViewModels
{
	public class MediaFileVM
	{
		public byte[] Content { get; set; }
		public string ContentType { get; set; }
	}
}
