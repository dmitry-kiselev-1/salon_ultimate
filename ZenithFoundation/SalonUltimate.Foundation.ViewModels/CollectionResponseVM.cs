﻿using System;
using System.Collections.Generic;
using System.Linq;
using SalonUltimate.Foundation.Enums;

namespace SalonUltimate.Foundation.ViewModels
{
	public class CollectionResponseVM<T> : ResponseVM<IEnumerable<T>>
	{
		public CollectionResponseVM() : base() { }
		public CollectionResponseVM(ResponseTypes result, string userMessage) : base(result, userMessage) { }
		public CollectionResponseVM(Exception exception, string userMessage = null) : base(exception, userMessage) { }

		public CollectionResponseVM(IEnumerable<T> data) 
			: base(data)
		{
			TotalReturned = data?.Count() ?? 0;
			Result = data != null ? ResponseTypes.Success : ResponseTypes.NotFound;
		}

		public virtual int? Total { get => TotalReturned; set { } }

		public int TotalReturned { get; set; }
	}
}
