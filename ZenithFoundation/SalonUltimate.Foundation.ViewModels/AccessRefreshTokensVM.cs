﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.Foundation.ViewModels
{
    public class AccessRefreshTokensVM
    {
		public string AccessToken { get; set; }
		public string RefreshToken { get; set; }
	}
}
