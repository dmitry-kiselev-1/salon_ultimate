﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.Exceptions;

namespace SalonUltimate.Foundation.ViewModels
{
	public class BaseResponseVM
	{
		public BaseResponseVM()
		{
			GeneratedAt = DateTime.UtcNow;
			Result = ResponseTypes.Success;
		}

		public BaseResponseVM(ResponseTypes result, string userMessage)
			: this()
		{
			Result = result;
			UserMessage = userMessage;
		}

		public BaseResponseVM(Exception exception, string userMessage = null)
			: this()
		{
			if (exception is ZenithBaseException)
			{
				var ex = (exception as ZenithBaseException);
				Result = ex.ResponseType;
				UserMessage = userMessage ?? ex.Message;
				SystemMessage = ex.SystemMessage;
				DebugInfo = ex.ZenithDebugInfo;
			}
			else if (exception is NotImplementedException)
			{
				Result = ResponseTypes.NotImplemented;
				UserMessage = "This API endpoint is not completely implemented so far.";
			}
			else
			{
				Result = ResponseTypes.Exception;
				UnwrapException(exception);
			}
		}

		public DateTime? GeneratedAt { get; set; }

		public ResponseTypes Result { get; set; }
		public string UserMessage { get; set; }
		public string SystemMessage { get; set; }

		public AccessRefreshTokensVM AuthTokens { get; set; }

		// This will probably be used later.
#if !DEBUG
		[JsonIgnore]
#endif
		public object DebugInfo { get; set; }

		public string SystemErrorMessage { get; set; }
		public string SystemErrorType { get; set; }
		public string SystemErrorStack { get; set; }

		private void UnwrapException(Exception e)
		{ 
			var flatten = FlattenException(e);
			foreach(var ie in flatten)
			{
				SystemErrorType += ie.GetType().FullName + Environment.NewLine;
				SystemErrorMessage += ie.Message + Environment.NewLine;
			}
			SystemErrorType = SystemErrorType.TrimEnd();
			SystemErrorMessage = SystemErrorMessage.TrimEnd();
			SystemErrorStack = e.StackTrace;
		}

		private IEnumerable<Exception> FlattenException(Exception e)
		{
			var ae = e as AggregateException;
			if(ae != null && ae.InnerExceptions.Count > 0)
			{
				yield return e;
				foreach (var ie in ae.InnerExceptions)
				{
					var result = FlattenException(ie);
					foreach (var r in result)
						yield return r;
				}
			}
			else
			if (e.InnerException != null)
			{
				yield return e;
				var result = FlattenException(e.InnerException);
				foreach (var r in result)
					yield return r;
			}
			else
				yield return e;
		}
	}
}
