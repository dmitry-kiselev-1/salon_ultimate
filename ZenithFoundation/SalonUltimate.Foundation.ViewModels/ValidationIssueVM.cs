﻿
using SalonUltimate.Foundation.Interfaces.Validation;

namespace SalonUltimate.Foundation.ViewModels
{
	public class ValidationIssueVM
	{
		public IConflictData ConflictData { get; private set; }
		public string Description { get; private set; }
		public string SystemMessage { get; private set; }
		public bool CanBeSkipped { get; private set; }
		public object SystemInfo { get; private set; }

		public ValidationIssueVM(string description, string systemMessage, bool canBeSkipped, IConflictData conflictData, object systemInfo = null)
		{
			ConflictData = conflictData;
			Description = description;
			SystemMessage = systemMessage;
			CanBeSkipped = canBeSkipped;
			SystemInfo = systemInfo;
		}
	}
}
