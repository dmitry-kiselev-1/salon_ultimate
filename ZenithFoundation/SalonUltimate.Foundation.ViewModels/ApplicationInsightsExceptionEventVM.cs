﻿using System;
using System.Collections.Generic;

namespace SalonUltimate.Foundation.ViewModels
{
	public class ApplicationInsightsExceptionEventVM
	{
		public Dictionary<string, string> Properties { get; set; }

		public Dictionary<string, double> Metrics { get; set; }

		public Exception Exception { get; set; }
	}
}