﻿using System;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using SalonUltimate.Foundation.Exceptions;
using SalonUltimate.Foundation.Time;

namespace SalonUltimate.Foundation.Auth
{
	public class JwtAuthService
	{
		private AuthSettings _jwtSettings;
		private IClockService _clockService;
		private TokenValidationParameters _validationParams = null;

		public const int TOKEN_LIFETIME_YEARS = 1;

		public JwtAuthService(IOptions<AuthSettings> jwtSettingsAccessor, IClockService clockService)
		{
			_jwtSettings = jwtSettingsAccessor.Value;
			_clockService = clockService;
		}

		public string Key => _jwtSettings.SecretKey;
		public SecurityKey SecurityKey => new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));

		public TokenValidationParameters TokenParameters
		{
			get
			{
				if (_validationParams == null)
					return (_validationParams = new TokenValidationParameters
					{
						IssuerSigningKey = SecurityKey,
						RequireExpirationTime = false,
						ValidateAudience = false,
						ValidateIssuer = false
					});
				return _validationParams;
			}
		}

		public ClaimsPrincipal DecodeWithHs256(string encodedJwt, out SecurityToken securityToken, TokenValidationParameters tokenParameters = null)
		{
			try
			{
				var principal = new JwtSecurityTokenHandler().ValidateToken(encodedJwt, 
					tokenParameters ?? TokenParameters, out securityToken);
				return principal;
			}
			catch (SecurityTokenExpiredException) { throw new ZenithNotAuthenticatedException("Token is expired"); }
			catch (SecurityTokenInvalidIssuerException) { throw new ZenithNotAuthenticatedException("Token is signed by an invalid issuer"); }
			catch (SecurityTokenInvalidAudienceException) { throw new ZenithNotAuthenticatedException("Token is signed for a different audience"); }
			catch (SecurityTokenInvalidSignatureException) { throw new ZenithNotAuthenticatedException("Token signature is invalid"); }
			catch (SecurityTokenNotYetValidException) { throw new ZenithNotAuthenticatedException("Token is signed for future use and is not valid yet"); }
			catch (SecurityTokenInvalidLifetimeException) { throw new ZenithNotAuthenticatedException("Token has invalid lifetime options"); }
			catch (Exception e) { throw new ZenithValidationException("Error occurred while validating token", e.Message, e.ToString()); }
		}

		public string CreateAndSignToken(ClaimsIdentity identity)
		{
			var handler = new JwtSecurityTokenHandler();
			var now = _clockService.UtcNow;

			var token = handler.CreateEncodedJwt(
				issuer: null,
				audience: null,
				subject: identity,
				notBefore: now,
				issuedAt: now, 
				expires: now.AddYears(TOKEN_LIFETIME_YEARS),
				signingCredentials: new SigningCredentials(SecurityKey, SecurityAlgorithms.HmacSha256));
			return token;
		}
	}
}
