﻿using System;
using System.Collections.Generic;
using System.Text;
using SalonUltimate.Foundation.Storage.UnitOfWork;

namespace SalonUltimate.MediaGateway.ORM
{
	/// <inheritdoc/>
	public class ApplicationUnitOfWork : EfUnitOfWork<ApplicationDbContext>
	{
		/// <inheritdoc/>
		public ApplicationUnitOfWork(ApplicationDbContext dbContext) : base(dbContext)
		{
		}
	}
}
