﻿using Microsoft.EntityFrameworkCore;
using SalonUltimate.BookedBy.Domain.Entities;
using SalonUltimate.MediaGateway.Domain.Entities;

namespace SalonUltimate.MediaGateway.ORM
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Storage>();
            modelBuilder.Entity<StorageFile>();
        }
    }
}
