﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.MediaGateway.ORM.Repositories;
using SalonUltimate.Foundation.Storage.UnitOfWork;
using SalonUltimate.MediaGateway.Domain;

namespace SalonUltimate.MediaGateway.ORM.DI
{
	public class RepositoryDIConfigurator : IDependencyInjectionConfigurator
	{
		public void RegisterServices(Action<Type, Type, ServiceLifetime> action)
		{
			action(typeof(IGenericRepository<,>), typeof(GenericRepository<,>), ServiceLifetime.Scoped);
			action(typeof(IUnitOfWork), typeof(ApplicationUnitOfWork), ServiceLifetime.Scoped);
		}
	}
}
