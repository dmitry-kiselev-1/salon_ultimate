﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace SalonUltimate.MediaGateway.ORM
{
    public class DesignTimeContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var projectPath = AppDomain.CurrentDomain.BaseDirectory.Split(new[] { @"bin\" }, StringSplitOptions.None)[0];
            var configuration = new ConfigurationBuilder()
                .SetBasePath(projectPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.local.json", true)
                .Build();

            var connectionString = configuration["ConnectionStrings:PostgreSqlConnectionString"];

            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UsePostgreSql(connectionString);

            // Devart driver will create database if not exists (https://forums.devart.com/viewtopic.php?t=34590):
            var config = Devart.Data.PostgreSql.Entity.Configuration.PgSqlEntityProviderConfig.Instance;
            config.DatabaseScript.Schema.DeleteDatabaseBehaviour = Devart.Data.PostgreSql.Entity.Configuration.DeleteDatabaseBehaviour.Database;

            return new ApplicationDbContext(builder.Options);
        }
    }
}
