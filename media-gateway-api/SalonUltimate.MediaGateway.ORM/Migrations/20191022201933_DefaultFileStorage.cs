﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SalonUltimate.MediaGateway.ORM.Migrations
{
	public partial class DefaultFile : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.InsertData(
				table: "Storage",
				columns: new[] { "Id", "Name", "Description" },
				values: new object[] { new Guid("00000000-0000-0000-0000-000000000000"), "AWSS3", "Default Storage, AWSS3 key in appsettings.json" });
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DeleteData(
				table: "Storage", "Id", Guid.Parse("00000000-0000-0000-0000-000000000001"));
		}
	}
}
