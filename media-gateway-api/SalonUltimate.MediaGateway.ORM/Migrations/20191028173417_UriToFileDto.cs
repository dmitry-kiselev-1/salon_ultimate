﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SalonUltimate.MediaGateway.ORM.Migrations
{
    public partial class UriToFileDto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "StorageFile",
                maxLength: 2048,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "StorageFile");
        }
    }
}
