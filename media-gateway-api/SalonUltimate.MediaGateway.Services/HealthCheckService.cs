﻿using System.Composition;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using Microsoft.EntityFrameworkCore;
using SalonUltimate.Foundation.Services.Interfaces;
using SalonUltimate.MediaGateway.Domain.Entities;
using SalonUltimate.MediaGateway.ORM;

namespace SalonUltimate.MediaGateway.Services
{
	[Export(typeof(IHealthCheckService))]
	public class HealthCheckService : IHealthCheckService
	{
		private readonly ApplicationDbContext _applicationDbContext;

		public HealthCheckService(ApplicationDbContext applicationDbContext)
		{
			_applicationDbContext = applicationDbContext;
		}

		public async Task WarmUpDbContext()
		{
			var storageCount = await _applicationDbContext.Set<Storage>().CountAsync();
		}
	}
}
