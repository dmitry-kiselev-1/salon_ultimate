﻿using AutoMapper;
using SalonUltimate.BookedBy.Domain.Entities;
using SalonUltimate.MediaGateway.Dtos;

namespace SalonUltimate.MediaGateway.Services.Mappings
{
	public class OrmMappingProfile : Profile
	{
		public OrmMappingProfile()
		{
			CreateMap<StorageFile, FileDto>().ReverseMap();
		}
	}
}
