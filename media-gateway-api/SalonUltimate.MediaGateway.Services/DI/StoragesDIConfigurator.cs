﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.MediaGateway.ORM.Repositories;
using SalonUltimate.Foundation.Storage.UnitOfWork;
using SalonUltimate.MediaGateway.Domain;
using SalonUltimate.MediaGateway.Services.Interfaces;
using SalonUltimate.MediaGateway.Services.Storages;

namespace SalonUltimate.MediaGateway.ORM.DI
{
	public class StoragesDIConfigurator : IDependencyInjectionConfigurator
	{
		public void RegisterServices(Action<Type, Type, ServiceLifetime> action)
		{
			action(typeof(IS3FileService), typeof(S3FileService), ServiceLifetime.Scoped);
			action(typeof(ISqlFileService), typeof(SqlFileService), ServiceLifetime.Scoped);
		}
	}
}
