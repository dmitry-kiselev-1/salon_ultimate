﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using SalonUltimate.BookedBy.Domain.Entities;
using SalonUltimate.MediaGateway.Dtos;

namespace SalonUltimate.MediaGateway.Services.Interfaces
{
	/// <inheritdoc />
	public interface ISqlFileService : IFileService
	{
		/// <summary>
		/// Return file info from sql database
		/// </summary>
		/// <param name="id">file id (guid)</param>
		/// <param name="cancellationToken"></param>
		/// <returns>StorageFileDto</returns>
		Task<FileDto> GetFileInfoAsync(Guid id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Upload file info into sql database
		/// </summary>
		/// <param name="storageFileDto">StorageFileDto</param>
		/// <param name="id">file id (guid)</param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		Task UploadAsync(FileDto storageFileDto, Guid id, CancellationToken cancellationToken = default);
	}
}
