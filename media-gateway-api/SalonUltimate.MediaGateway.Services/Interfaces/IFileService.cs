﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SalonUltimate.MediaGateway.Services.Interfaces
{
	/// <summary>
	/// Represents a file storage.
	/// </summary>
	public interface IFileService
	{
		/// <summary>
		/// Uploads the file to the storage.
		/// </summary>
		/// <param name="stream">Stream for upload, content of the file will be read from this stream.</param>
		/// <param name="id">Unique file guid.</param>
		/// <param name="cancellationToken"> A <see cref="CancellationToken"/> to propagate a notification
		/// that asynchronous operation should be cancelled. </param>
		/// <returns>A task that represents the asynchronous operation.</returns>
		Task UploadAsync(Stream stream, string id,
			CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Delete file from storage
		/// </summary>
		/// <param name="id">file id</param>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		Task DeleteAsync(string id, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Builds a full-qualified URI of the file to be accessed outside.
		/// Can be a direct link to AWS, BB, NW or whatever. This address must return the file.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		string BuildFileUrl(string id);

		/// <summary>
		/// Creates <see cref="Stream"/> for reading the file.
		/// </summary>
		/// <param name="id">Unique file guid.</param>
		/// <param name="cancellationToken"> A <see cref="CancellationToken"/> to propagate a notification
		/// that asynchronous operation should be cancelled. </param>
		/// <returns>A <see cref="Task"/> that on completion provides the created stream./></returns>
		Task<Stream> CreateReadStreamAsync(string id,
			CancellationToken cancellationToken = default(CancellationToken));
	}
}
