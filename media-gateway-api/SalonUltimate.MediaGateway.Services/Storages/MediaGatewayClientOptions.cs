﻿namespace SalonUltimate.MediaGateway.Services.Storages
{
	/// <summary>
	/// Options for MediaGatewayClient.
	/// </summary>
	public class MediaGatewayClientOptions
	{
		/// <summary>
		/// MediaGatewayApi BaseAddress.
		/// </summary>
		public string BaseAddress { get; set; }

	}
}
