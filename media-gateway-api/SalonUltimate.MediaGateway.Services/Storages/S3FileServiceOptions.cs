﻿namespace SalonUltimate.MediaGateway.Services.Storages
{
	/// <summary>
	/// Options for S3 file storage.
	/// </summary>
	public class S3FileServiceOptions
	{
		/// <summary>
		/// Authentication key.
		/// </summary>
		public string AwsKey { get; set; }

		/// <summary>
		/// Authentication token.
		/// </summary>
		public string AwsToken { get; set; }

		/// <summary>
		/// System name of the S3 region.
		/// </summary>
		public string AwsRegion { get; set; }

		/// <summary>
		/// The name of bucket.
		/// </summary>
		public string BucketName { get; set; }
	}
}
