﻿using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.Extensions.Options;
using SalonUltimate.MediaGateway.Services.Interfaces;

namespace SalonUltimate.MediaGateway.Services.Storages
{
	/// <summary>
	/// Amazon S3 file storage implementation.
	/// </summary>
	public class S3FileService : IS3FileService
	{
		private const string FileUrlTemplate = "https://{0}.s3.{1}.amazonaws.com/{2}";

		private readonly S3FileServiceOptions _options;
		private readonly BasicAWSCredentials _awsCredentials;
		private readonly RegionEndpoint _awsRegion;

		/// <summary>
		/// Initializes a new instance of <see cref="S3FileService"/>.
		/// </summary>
		/// <param name="optionsAccessor">S3 file storage options.</param>
		public S3FileService(IOptions<S3FileServiceOptions> optionsAccessor)
		{
			_options = optionsAccessor.Value;
			_awsCredentials = new BasicAWSCredentials(_options.AwsKey, _options.AwsToken);
			_awsRegion = RegionEndpoint.GetBySystemName(_options.AwsRegion);
		}

		/// <inheritdoc />
		public async Task UploadAsync(Stream stream, string id,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var uploadStreamBufferSize = 1024 * 16;
			BufferedStream bufferedStream = new BufferedStream(stream, uploadStreamBufferSize);

			var putObjectRequest = new PutObjectRequest
			{
				BucketName = _options.BucketName,
				InputStream = bufferedStream,
				Key = id,
				CannedACL = S3CannedACL.PublicRead
			};

			using (var awsClient = new AmazonS3Client(_awsCredentials, _awsRegion))
			{
				var result = await awsClient.PutObjectAsync(putObjectRequest, cancellationToken);	
			}
		}

		/// <inheritdoc />
		public async Task DeleteAsync(string subpath, 
			CancellationToken cancellationToken = default(CancellationToken))
		{
			//if (await this.ExistsAsync(id, cancellationToken))
			using (var awsClient = new AmazonS3Client(_awsCredentials, _awsRegion))
			{
				var result = await awsClient.DeleteObjectAsync(_options.BucketName, subpath, cancellationToken);
			}
		}

		/// <inheritdoc />
		public string BuildFileUrl(string subpath)
		{
			return string.Format(FileUrlTemplate, _options.BucketName, _options.AwsRegion, subpath);
		}

		/// <inheritdoc />
		public async Task<Stream> CreateReadStreamAsync(string subpath,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var downloadStreamBufferSize = 1024 * 16;

			using (var awsClient = new AmazonS3Client(_awsCredentials, _awsRegion))
			{
				var objectResponse = await awsClient.GetObjectAsync(_options.BucketName, subpath, cancellationToken);
				BufferedStream bufferStream = new BufferedStream(objectResponse.ResponseStream, downloadStreamBufferSize);
				return bufferStream;
			}
		}

		#region private
		/// <summary>
		/// Check file exists in storage without download operation.
		/// </summary>
		/// <param name="subpath">Virtual FS relative file path.</param>
		/// <param name="cancellationToken"> A <see cref="CancellationToken"/> to propagate a notification
		/// that asynchronous operation should be cancelled. </param>
		/// <returns>A task that represents the asynchronous operation.</returns>
		private async Task<bool> ExistsAsync(string subpath, 
			CancellationToken cancellationToken = default(CancellationToken))
		{
			// Amazon.S3.IO (S3FileInfo) not supported in .Net Core
			// https://forums.aws.amazon.com/thread.jspa?messageID=756075&tstart=0

			using (var awsClient = new AmazonS3Client(_awsCredentials, _awsRegion))
			{
				var file = await awsClient.ListObjectsAsync(bucketName: _options.BucketName, prefix: subpath, cancellationToken);
				return file.S3Objects.Any();
			}
		}
		#endregion
	}
}
