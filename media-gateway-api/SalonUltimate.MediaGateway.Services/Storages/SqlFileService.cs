﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using AutoMapper;
using Microsoft.Extensions.Options;
using SalonUltimate.BookedBy.Domain.Entities;
using SalonUltimate.MediaGateway.Dtos;
using SalonUltimate.MediaGateway.ORM;
using SalonUltimate.MediaGateway.Services.Interfaces;

namespace SalonUltimate.MediaGateway.Services.Storages
{
	/// <summary>
	/// Amazon S3 file storage implementation.
	/// </summary>
	public class SqlFileService : ISqlFileService
	{
		private readonly IMapper _mapper;
		private readonly ApplicationDbContext _applicationDbContext;
		//private readonly ApplicationUnitOfWork _applicationUnitOfWork;

		/// <summary>
		/// Initializes a new instance of <see cref="SqlFileService"/>.
		/// </summary>
		public SqlFileService(IMapper mapper, ApplicationDbContext applicationDbContext)
		{
			_mapper = mapper;
			_applicationDbContext = applicationDbContext;
			//_applicationUnitOfWork = new ApplicationUnitOfWork(applicationDbContext);
		}

		/// <inheritdoc />
		public async Task UploadAsync(Stream stream, string id,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			await Task.Run(() => null, cancellationToken);
		}

		/// <inheritdoc />
		public async Task UploadAsync(FileDto storageFileDto, Guid id, 
			CancellationToken cancellationToken = default)
		{
			var storageFile = _mapper.Map<StorageFile>(storageFileDto);
			await this.CreateOrUpdate(id, storageFile, cancellationToken);
		}

		/// <inheritdoc />
		public async Task DeleteAsync(string id, 
			CancellationToken cancellationToken = default(CancellationToken))
		{
			// select id from sql database:
			var file = await _applicationDbContext.FindAsync<StorageFile>(keyValues: new object[]{Guid.Parse(id)}, cancellationToken: cancellationToken);

			if (file != null)
			{
				// delete from sql database:
				_applicationDbContext.Set<StorageFile>().Remove(file);
				await _applicationDbContext.SaveChangesAsync(cancellationToken);
			}
		}

		/// <inheritdoc />
		public string BuildFileUrl(string id)
		{
			return id;
		}

		/// <inheritdoc />
		public async Task<Stream> CreateReadStreamAsync(string id,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			return await Task.Run(() => Stream.Null, cancellationToken);
		}

		/// <inheritdoc />
		public async Task<FileDto> GetFileInfoAsync(Guid id, CancellationToken cancellationToken)
		{
			// select id from sql database:
			var file = await _applicationDbContext.FindAsync<StorageFile>(keyValues: new object[]{id}, cancellationToken: cancellationToken);
			var storageFileDto = _mapper.Map<FileDto>(file);
			return storageFileDto;
		}

		#region private
		private async Task CreateOrUpdate(Guid id, StorageFile newFile, CancellationToken cancellationToken)
		{
			// find file id in sql database:
			var existingFile = await _applicationDbContext.FindAsync<StorageFile>(keyValues: new object[]{id}, cancellationToken: cancellationToken);

			if (existingFile != null) {
				// update file in sql database:
				_applicationDbContext.Set<StorageFile>().Remove(existingFile);
				newFile.Id = id;
				await _applicationDbContext.Set<StorageFile>().AddAsync(newFile, cancellationToken);
			} else {
				// insert file in sql database:
				newFile.Id = id;
				await _applicationDbContext.Set<StorageFile>().AddAsync(newFile, cancellationToken);
			}
			await _applicationDbContext.SaveChangesAsync(cancellationToken);
		}
		#endregion
	}
}
