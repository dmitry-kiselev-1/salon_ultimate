﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using SalonUltimate.MediaGateway.Domain.Entities;

namespace SalonUltimate.BookedBy.Domain.Entities
{
	/// <summary>
	/// Represents a file sent with the HttpRequest.
	/// </summary>
	public class StorageFile : Entity
	{
		/// <summary>
		/// StorageFile type ("pdf", "jpeg", "xls", etc.),
		/// "application/octet-stream" by default.
		/// </summary>
		[StringLength(128)]
		[Column(Order = 3)]
		public String ContentType { get; set; } = "application/octet-stream";

		/// <summary>
		/// Gets the file length in bytes.</summary>
		[Column(Order = 4)]
		public long Length { get; set; }

		/// <summary>
		/// StorageFile stream 
		/// (not for store in EF database, just for sending to file-storage).
		/// </summary>
		[NotMapped]
		public Stream Stream { get; set; }

		/// <summary>
		/// Absolute Url to File
		/// </summary>
		[Required]
		[StringLength(2048)]
		[Column(Order = 5)]
		public String Url { get; set; }

		/// <summary>
		/// Navigation property Storage
		/// </summary>
		[Required]
		[ForeignKey(nameof(Storage))]
		[Column(Order = 6)]
		public Guid StorageId { get; set; }
		public virtual Storage Storage { get; set; }
	}
}
