﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Extensions.FileProviders;
using SalonUltimate.BookedBy.Domain.Entities;

namespace SalonUltimate.MediaGateway.Domain.Entities
{
	/// <summary>
	/// Represents a file storage (AWS, Azure, etc).
	/// </summary>
	public class Storage : Entity
	{
		/// <summary>
		/// Navigation property FileList
		/// </summary>
		[InverseProperty(nameof(StorageFile.Storage))]
		public virtual IList<StorageFile> FileList { get; set; }
	}
}
