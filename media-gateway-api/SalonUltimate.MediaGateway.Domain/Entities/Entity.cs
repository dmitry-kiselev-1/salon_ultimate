﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SalonUltimate.MediaGateway.Domain.Infrastructure;

namespace SalonUltimate.MediaGateway.Domain.Entities
{
	public abstract class Entity: BaseEntity<Guid>
	{
		/*
		/// <summary>
		/// Entity unique id.
		/// </summary>
		[Key]
		[Required]
		[Column(Order = 0)]
		public new Guid Id { get; set; }
		*/

		/// <summary>
		/// Entity name (FileName, StorageName, etc).
		/// </summary>
		[Required]
		[StringLength(512)]
		[Column(Order = 1)]
		public string Name { get; set; }

		/// <summary>
		/// Any notes
		/// </summary>
		[StringLength(1024)]
		[Column(Order = 100)]
		public string Description { get; set; }
	}
}
