﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.MediaGateway.Domain.Infrastructure
{
	public interface IBaseEntity<TKey> : IIdentity<TKey> where TKey : struct
	{
		bool IsDeleted { get; set; }
	}

	public interface IBaseEntity : IBaseEntity<Guid> { }
}
