﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalonUltimate.MediaGateway.Domain.Infrastructure
{
	public class Identity<TKey> : IIdentity<TKey> where TKey : struct
	{
		/// <summary>
		/// Entity unique id.
		/// </summary>
		[Key]
		[Required]
		[Column(Order = 0)]
		public TKey Id { get; set; }
	}
}
