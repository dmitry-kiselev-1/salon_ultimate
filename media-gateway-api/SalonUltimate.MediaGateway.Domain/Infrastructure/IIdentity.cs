﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalonUltimate.MediaGateway.Domain.Infrastructure
{
	public interface IIdentity<TKey> where TKey : struct
	{
		TKey Id { get; set; }
	}
}
