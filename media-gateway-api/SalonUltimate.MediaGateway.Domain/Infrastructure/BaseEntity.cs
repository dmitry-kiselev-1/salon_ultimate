﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SalonUltimate.MediaGateway.Domain.Infrastructure
{
	public class BaseEntity<TKey> : Identity<TKey>, IBaseEntity<TKey>
		where TKey : struct
	{
		[NotMapped]
		public bool IsDeleted { get; set; }
	}

	public class BaseEntity : BaseEntity<Guid>, IBaseEntity { }
}
