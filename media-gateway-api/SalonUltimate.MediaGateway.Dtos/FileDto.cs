﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace SalonUltimate.MediaGateway.Dtos
{
	/// <summary>
	/// Represents a file sent with the HttpRequest.
	/// </summary>
	public class FileDto : BaseDto
	{
		/// <summary>
		/// Entity name (FileName, StorageName, etc).
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// StorageFile extensions as string ("pdf", "jpeg", "xls", etc.),
		/// "application/octet-stream" by default.
		/// </summary>
		public string ContentType { get; set; } = "application/octet-stream";

		/// <summary>
		/// Gets the file length in bytes.</summary>
		public long Length { get; set; }

		/// <summary>
		/// StorageId
		/// </summary>
		public Guid StorageId { get; set; }

		/// <summary>
		/// Any notes
		/// </summary>
		public string Description { get; set; } = "";

		/// <summary>
		/// Absolute Url to File
		/// </summary>
		public String Url { get; set; }

	}
}
