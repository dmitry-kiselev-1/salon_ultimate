﻿using System;

namespace SalonUltimate.MediaGateway.Dtos
{
	public class BaseDto
	{
		/// <summary>
		/// Entity unique id.
		/// </summary>
		public new Guid Id { get; set; }
	}
}
