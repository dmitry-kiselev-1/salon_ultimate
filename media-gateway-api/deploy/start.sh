#!/bin/bash

#### TEST
# SU_PGSQL_BASE64="eyJTZXJ2ZXIiOiJ0ZXN0LXBzcWwxMDRyMS1mb3Item5ldy1kZXBsb3ltZW50LmN1Y21tYmpoOHA2ay51cy1lYXN0LTEucmRzLmFtYXpvbmF3cy5jb20iLCJEYXRhYmFzZSI6Im1lZGlhX2dhdGV3YXkiLCJ1c2VyIjoicm9vdCIsIlB3ZCI6Im91cmxvZ29pc2JsdWUiLCJQb3J0IjoiNTQzMiIsIkxpY2Vuc2UgS2V5IjoiMnpwMXEyeUdBQnBQSVFXOWc1dFpLMTNzbFR4NVJHVWZYUHZXYjlJRWRuU0lzUGxub1pMbjh2cXJkenBSV3B0SFpQTDU2RFA3TWJMZXlWQy9wcXFlZ0lIL1Vua25qSWkzV20wekJoK0xBc2lGNzNlY1VCN2NlVFg3QjQwZ0RyVWdwWXFZZ2ozOTlZcjVkckYvUmVSTEk1SGg5WEJTTnZYU0lEaGc2Z1RKZDUxRUs0K2pmWjBLNkpubmsvZnBNSWYrcVdGNFFsdTczOFpubEJZQ2ptT0MzL253bzcvMWZWTUx1UlRTaURueEFLcHk2MHdnYXNrUnBzMHpKL3B1dCt1ei9WcGlMRnhFbGtJWHEwdTQrVy9GNU54QVh6KzNkNTcxczU1WXU5UFU0UGM9In0="
#### TEST
. $(dirname "$0")/variables-processing.sh

export SU_DB_CONNECTION_STRING_PGSQL=`echo $SU_PGSQL | jq "to_entries | map(.key + \"=\" + .value) | join(\"; \")" -r`

cat ./appsettings.json \
    | jq ".ConnectionStrings.PostgreSqlConnectionString=\"$SU_DB_CONNECTION_STRING_PGSQL\"" \
    | jq ".AWSS3.AwsKey=\"$SU_AWSS3_KEY\"" \
    | jq ".AWSS3.AwsToken=\"$SU_AWSS3_TOKEN\"" \
    | jq ".AWSS3.AwsRegion=\"$SU_AWSS3_REGION\"" \
    | jq ".AWSS3.BucketName=\"$SU_AWSS3_BUCKET\"" \
    | jq ".ApplicationInsights.InstrumentationKey=\"$SU_APPINSIGHTS_IKEY\"" \
    | jq ".AppInfo.Environment=\"$SU_ENVIRONMENT_NAME\"" \
    > ./appsettings.json.tmp

cp ./appsettings.json.tmp ./appsettings.json
rm ./appsettings.json.tmp
cat ./appsettings.json

# peform database migration and application start:
dotnet SalonUltimate.MediaGateway.Api.dll --migrate=up && dotnet SalonUltimate.MediaGateway.Api.dll
