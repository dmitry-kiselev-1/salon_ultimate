#!/bin/sh
#set -x

if [ "$1" = "local" ]; then
	export SU_SERVICE_URL=http://localhost:56063
	export SU_STORE_KEY=1
fi

check_results() {
	RESULT_CHECKER=$1; shift;

	echo $SU_QUERY_RESPONSE
	export SU_QUERY_RESULT_VALUE=`echo $SU_QUERY_RESPONSE | jq "$RESULT_CHECKER"`
	echo "response = $SU_QUERY_RESULT_VALUE"

	if [ "$SU_QUERY_RESULT_VALUE" != 1 ] ; then
	    echo "fail"
	    exit -1
	else
	    echo "passed"
	fi
}

test_cookie() {
	URI=$1; shift;

	echo "Will Check $SU_SERVICE_URL$URI"

	export SU_QUERY_RESPONSE=`curl -q -X GET --header 'Accept: application/json' --cookie "store_key=$SU_STORE_KEY" $SU_SERVICE_URL$URI`
	check_results $@
}


test_header() {
	URI=$1; shift;

	echo "Will Check $SU_SERVICE_URL$URI"

	export SU_QUERY_RESPONSE=`curl -q -X GET --header 'Accept: application/json' --header "X-SU-store-key: $SU_STORE_KEY" $SU_SERVICE_URL$URI`
	check_results $@
}

test_cookie /api/v1/HealthCheck/warm-up-db .result
test_cookie /api/v1/HealthCheck/is-up .result

#test_cookie /api/Queue .result
#test_cookie /api/Employees?filterRule=none .result
#test_header /api/Employees?filterRule=none .result
true
