#!/bin/bash

# hack for <nil>
VARIABLES=`env | grep \<nil\> | sed s/\<nil\>//g`
export $VARIABLES

# decode base64 variables
for var in ${!SU_@}; do
    V=`echo $var | grep -oP '(.*?)(?=_BASE64$)'`
    if [ "$V" ]; then
        export $V="`echo ${!var} | base64 -d`"
        unset ${var}
    fi
done
