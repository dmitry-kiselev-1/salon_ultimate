#!/bin/sh
set -e
APP_SETTINGS_FILE=${1:-./appsettings.json}

### Tests
# cp ./Zenith.Webend.API/appsettings.json $APP_SETTINGS_FILE
# CI_COMMIT_REF_NAME=`git name-rev HEAD --name-only`
# CI_COMMIT_SHA=`git rev-list HEAD -1`
# CI_JOB_ID=`date +%M%H%S`
# CI_PIPELINE_IID=`date +%H%M%S`
### Tests

cat $APP_SETTINGS_FILE \
    | jq ".BuildInfo.BranchName=\"$CI_COMMIT_REF_NAME\"" \
    | jq ".BuildInfo.CommitHash=\"$CI_COMMIT_SHA\"" \
    | jq ".BuildInfo.BuildDate=\"$(date -Iseconds -u)\"" \
    | jq ".BuildInfo.PipelineId=\"$CI_PIPELINE_IID\"" \
    | jq ".BuildInfo.JobId=\"$CI_JOB_ID\"" \
    > $APP_SETTINGS_FILE.tmp

diff -w $APP_SETTINGS_FILE $APP_SETTINGS_FILE.tmp || true
cp $APP_SETTINGS_FILE.tmp $APP_SETTINGS_FILE
rm $APP_SETTINGS_FILE.tmp
cat $APP_SETTINGS_FILE
