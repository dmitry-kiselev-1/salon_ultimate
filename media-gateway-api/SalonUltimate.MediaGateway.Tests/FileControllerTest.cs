using System;
using System.IO;
using SalonUltimate.MediaGateway.Tests.Infrastructure;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using SalonUltimate.MediaGateway.Controllers;
using SalonUltimate.MediaGateway.Dtos;
using SalonUltimate.MediaGateway.Services.Interfaces;
using Xunit;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Moq;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.MediaGateway.ORM;
using SalonUltimate.MediaGateway.Services.Mappings;
using SalonUltimate.MediaGateway.Services.Storages;

namespace SalonUltimate.MediaGateway.Tests
{
	/// <summary>
	/// Tests Fixture
	/// </summary>
	public class FileControllerTestFixture: IDisposable
	{
		public IConfiguration Configuration;
		public IOptions<S3FileServiceOptions> S3FileServiceOptions;
		public IMapper Mapper;

		public IHttpContextAccessor _httpContextAccessor;
		public HttpContext _httpContext;
		public ISqlFileService _sqlFileService;
		public IS3FileService _s3FileService;

		public Guid fileId = Guid.Parse("00000000-0000-0000-0000-000000000001");
		public Guid fileIdForClear = Guid.Parse("00000000-0000-0000-0000-000000000001");
		public string fileName = "appsettings.json";
		public string contentType = "text/json";

		//public IConfiguration ConfigurationJsonFile;

		/// <summary>
		/// One-time setup code
		/// </summary>
		public FileControllerTestFixture()
		{
			//_sqlFileService = (ISqlFileService)base.ServiceProvider.GetService(typeof(ISqlFileService));
			//_s3FileService = (IS3FileService)base.ServiceProvider.GetService(typeof(IS3FileService));

			_httpContext = new DefaultHttpContext();
			_httpContextAccessor = new HttpContextAccessor();
			//_httpContextAccessor = new Mock<IHttpContextAccessor>().Object;

			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.AddJsonFile("appsettings.local.json", optional: true)
				.Build();

			this.Configuration = configuration;
			
			this.S3FileServiceOptions = Microsoft.Extensions.Options.Options.Create(new S3FileServiceOptions()
			{
				AwsKey = configuration.GetSection("AWSS3:AwsKey").Value,
				AwsToken = configuration.GetSection("AWSS3:AwsToken").Value,
				AwsRegion = configuration.GetSection("AWSS3:AwsRegion").Value,
				BucketName = configuration.GetSection("AWSS3:BucketName").Value
			});
			_s3FileService = new S3FileService(this.S3FileServiceOptions);
			
			var ormMappingProfile = new OrmMappingProfile();
			var mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile(ormMappingProfile));
			this.Mapper = new Mapper(mapperConfiguration);

			var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
			builder.UsePostgreSql(Configuration.GetConnectionString("PostgreSqlConnectionString"));
			var applicationDbContext = new ApplicationDbContext(builder.Options);

			_sqlFileService = new SqlFileService(this.Mapper, applicationDbContext);
		}

		/// <summary>
		/// One-time clean-up code
		/// </summary>
		public new void Dispose()
		{
			//_serviceScope?.Dispose();
		}
	}

	/// <summary>
	/// Tests
	/// </summary>
	public class FileControllerTest: IDisposable, IClassFixture<FileControllerTestFixture>
	{
		private readonly FileControllerTestFixture _fixture;

		public FileControllerTest(FileControllerTestFixture fixture)
		{
			_fixture = fixture;
			_fixture._httpContextAccessor.HttpContext = _fixture._httpContext;
		}

		/// <summary>
		/// Unit-test for GetUrl
		/// </summary>
		[Fact]
		public async Task GetUrl_ShouldReturnOkAndValidAbsoluteUrl()
		{
			// Arrange
			var controller = new FileController(_fixture._httpContextAccessor, _fixture._s3FileService, _fixture._sqlFileService);

			// Act
			var result = await controller.GetUrl(Guid.Empty);

			Assert.IsType<OkObjectResult>(result);
			var content = ((ObjectResult) result).Value;
			bool isValidAbsoluteUrl = Uri.TryCreate(content.ToString(), UriKind.Absolute, out _);

			// Assert 
			Assert.True(isValidAbsoluteUrl);
		}

		/// <summary>
		/// Integration test for CreateOrUpdateFileAsForm
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task CreateOrUpdateFileAsForm_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var controller = new FileController(_fixture._httpContextAccessor, _fixture._s3FileService, _fixture._sqlFileService);

			// Act
			using (var fileStream = File.OpenRead(_fixture.fileName))
			{
				var formFile = new FormFile(fileStream, 0, fileStream.Length, null, Path.GetFileName(fileStream.Name))
				{
					Headers = new HeaderDictionary(),
					ContentType = _fixture.contentType
				};

				var result = await controller.CreateOrUpdateFileAsForm(_fixture.fileId, formFile);

				// Assert
				Assert.IsType<OkObjectResult>(result);

				var content = ((ObjectResult) result).Value;
				Assert.IsType<ResponseVM<FileDto>>(content);

				// Clean-up (by Dispose)
				_fixture.fileIdForClear = ((ResponseVM<FileDto>) content).Response.Id;
			}

		}

		/// <summary>
		/// Integration test for CreateFileAsForm
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task CreateFileAsForm_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var controller = new FileController(_fixture._httpContextAccessor, _fixture._s3FileService, _fixture._sqlFileService);

			// Act
			using (var fileStream = File.OpenRead(_fixture.fileName))
			{
				var formFile = new FormFile(fileStream, 0, fileStream.Length, null, Path.GetFileName(fileStream.Name))
				{
					Headers = new HeaderDictionary(),
					ContentType = _fixture.contentType
				};

				var result = await controller.CreateFileAsForm(formFile);

				// Assert
				Assert.IsType<OkObjectResult>(result);

				var content = ((ObjectResult) result).Value;
				Assert.IsType<ResponseVM<FileDto>>(content);

				// Clean-up (by Dispose)
				_fixture.fileIdForClear = ((ResponseVM<FileDto>) content).Response.Id;
			}
		}

		/// <summary>
		/// Integration test for Delete
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task Delete_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var controller = new FileController(_fixture._httpContextAccessor, _fixture._s3FileService, _fixture._sqlFileService);

			// Act
			var result = await controller.Delete(_fixture.fileId);

			// Assert
			Assert.IsType<OkObjectResult>(result);

			var content = ((ObjectResult) result).Value;
			Assert.IsType<ResponseVM<FileDto>>(content);
		}

		/// <summary>
		/// Clean-up (xUnit.net creates a new instance of the test class for every test)
		/// </summary>
		public void Dispose()
		{
			_fixture._httpContextAccessor.HttpContext = _fixture._httpContext;
			var controller = new FileController(_fixture._httpContextAccessor, _fixture._s3FileService, _fixture._sqlFileService);
			try
			{
				var result1 = controller.Delete(_fixture.fileId).Result;
				var result2 = controller.Delete(_fixture.fileIdForClear).Result;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}
	}
}
