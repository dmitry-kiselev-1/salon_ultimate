using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Moq;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.MediaGateway.Client;
using SalonUltimate.MediaGateway.Controllers;
using SalonUltimate.MediaGateway.Dtos;
using SalonUltimate.MediaGateway.Services.Interfaces;
using SalonUltimate.MediaGateway.Services.Storages;
using Xunit;

namespace SalonUltimate.MediaGateway.Tests
{
	/// <summary>
	/// Tests Fixture
	/// </summary>
	public class MediaGatewayClientTestFixture: IDisposable
	{
		public IConfiguration Configuration;
		public IOptions<MediaGatewayClientOptions> MediaGatewayClientOptions;

		public string FileName = "appsettings.json";
		public Guid FileId = Guid.Parse("00000000-0000-0000-0000-000000000001");
		public string FileUrl = Uri.EscapeDataString("https://k8s-mediagw.s3.us-east-1.amazonaws.com/00000000-0000-0000-0000-000000000001");

		/// <summary>
		/// One-time setup code
		/// </summary>
		public MediaGatewayClientTestFixture()
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json")
				.AddJsonFile("appsettings.local.json", optional: true)
				.Build();

			this.Configuration = configuration;
			
			this.MediaGatewayClientOptions = Options.Create(new MediaGatewayClientOptions()
				{BaseAddress = configuration.GetSection("MediaGatewayApi:BaseAddress").Value});
		}

		/// <summary>
		/// One-time clean-up code
		/// </summary>
		public void Dispose()
		{}
	}


	/// <summary>
	/// Tests
	/// </summary>
	public class MediaGatewayClientTest: IDisposable, IClassFixture<MediaGatewayClientTestFixture>
	{
		private readonly MediaGatewayClientTestFixture _fixture;

		public MediaGatewayClientTest(MediaGatewayClientTestFixture fixture)
		{
			_fixture = fixture;
		}

		/// <summary>
		/// Unit-test for GetUrl
		/// </summary>
		[Fact]
		public async Task GetUrl_ShouldReturnsValidAbsoluteUrl()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			var result = await client.GetUrlAsync(Guid.Empty);
			bool isValidAbsoluteUrl = Uri.TryCreate(result, UriKind.Absolute, out _);

			// Assert 
			Assert.True(isValidAbsoluteUrl);
		}

		/// <summary>
		/// Integration test for Delete
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task Delete_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			var result = await client.DeleteAsync(_fixture.FileId);

			// Assert
			Assert.IsType<ResponseVM<FileDto>>(result);
			Assert.True(result.Response == null);
		}

		/// <summary>
		/// Integration test for DeleteByUrl
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task DeleteByUrl_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			var result = await client.DeleteAsync(_fixture.FileUrl);

			// Assert
			Assert.IsType<ResponseVM<FileDto>>(result);
			Assert.True(result.Response == null);
		}

		/// <summary>
		/// Integration test for CreateOrUpdateFileAsForm
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task CreateOrUpdateFileAsForm_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			using (var fileStream = File.OpenRead(_fixture.FileName))
			{
				IFormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "uploadedFile", _fixture.FileName);
				var result = await client.CreateOrUpdateFileAsFormAsync(_fixture.FileId, formFile);

				// Assert
				Assert.IsType<ResponseVM<FileDto>>(result);
				Assert.IsType<FileDto>(result.Response);

				// Clean-up (by Dispose)
				_fixture.FileId = result.Response.Id;
			}
		}

		/// <summary>
		/// Integration test for CreateFileAsForm
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task CreateFileAsForm_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			using (var fileStream = File.OpenRead(_fixture.FileName))
			{
				IFormFile formFile = new FormFile(fileStream, 0, fileStream.Length, "uploadedFile", _fixture.FileName);
				var result = await client.CreateFileAsFormAsync(formFile);

				// Assert
				Assert.IsType<ResponseVM<FileDto>>(result);
				Assert.IsType<FileDto>(result.Response);

				// Clean-up (by Dispose)
				_fixture.FileId = result.Response.Id;
			}
		}

		/// <summary>
		/// Integration test for CreateFileAsStream
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task CreateFileAsStream_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			using (var fileStream = File.OpenRead(_fixture.FileName))
			{
				var fileDto = new FileDto()
				{
					Name = Path.GetFileName(fileStream.Name)
				};
				var result = await client.CreateFileAsStreamAsync(fileDto, fileStream);

				// Assert
				Assert.IsType<ResponseVM<FileDto>>(result);
				Assert.IsType<FileDto>(result.Response);

				// Clean-up (by Dispose)
				_fixture.FileId = result.Response.Id;
			}
		}

		/// <summary>
		/// Integration test for GetFileDto
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task GetFileDto_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			var result = await client.GetFileDtoAsync(_fixture.FileId);

			// Assert
			Assert.IsType<ResponseVM<FileDto>>(result);
			Assert.True(result.Response == null || result.Response.GetType() == typeof(FileDto));
		}

		/// <summary>
		/// Integration test for GetFileDtoByUrl
		/// (not supported our build-environment, uncomment for local run only)
		/// </summary>
		[Fact]
		public async Task GetFileDtoByUrl_ShouldReturnOkAndResponseVM()
		{
			// Arrange
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);

			// Act
			var result = await client.GetFileDtoAsync(_fixture.FileUrl);

			// Assert
			Assert.IsType<ResponseVM<FileDto>>(result);
			Assert.True(result.Response == null || result.Response.GetType() == typeof(FileDto));
		}

		/// <summary>
		/// Clean-up (xUnit.net creates a new instance of the test class for every test)
		/// </summary>
		public void Dispose()
		{
			var client = new MediaGatewayHttpClient(new HttpClient(), _fixture.MediaGatewayClientOptions);
			try
			{
				var result = client.DeleteAsync(_fixture.FileId).Result;
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}
	}
}
