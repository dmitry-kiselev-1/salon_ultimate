﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SalonUltimate.MediaGateway.ORM;
using SalonUltimate.MediaGateway.Tests.Infrastructure.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SalonUltimate.MediaGateway.Tests.Infrastructure
{
	/// <summary>
	/// Inherit your test class from this if you need to run tests against a fake database.
	/// </summary>
	public abstract class DatabaseTestBase : TestBase
	{
		protected DatabaseTestBase()
		{
			DbContext = ServiceProvider.GetRequiredService<ApplicationDbContext>();
		}

		protected ApplicationDbContext DbContext { get; }

		public override async Task InitializeAsync()
		{
			await SeedAsync();
		}

		public override async Task DisposeAsync()
		{
			if(DbContext.Database.IsSqlite())
			{
				DbContext.Database.CloseConnection();
				await DbContext.Database.EnsureDeletedAsync();
			}
		}

		protected abstract Task SeedAsync();

		protected Task AddAndSaveAsync<T>(T entity) where T : class 
			=> DbContext.AddAndSaveAsync(entity);
		protected Task AddRangeAndSaveAsync<T>(IEnumerable<T> entities) where T : class 
			=> DbContext.AddRangeAndSaveAsync(entities);
	}
}
