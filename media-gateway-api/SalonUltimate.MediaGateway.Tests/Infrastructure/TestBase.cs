﻿using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using SalonUltimate.Foundation.Helpers;

namespace SalonUltimate.MediaGateway.Tests.Infrastructure
{
	/// <summary>
	/// Inherit from this class if you don't need database in your test.
	/// </summary>
	public abstract class TestBase : IAsyncLifetime, IDisposable
	{
		private static readonly Lazy<IServiceProvider> ContainerInitializer =
			new Lazy<IServiceProvider>(CreateContainer, LazyThreadSafetyMode.ExecutionAndPublication);
		protected readonly IServiceScope _serviceScope;

		protected TestBase()
		{
			_serviceScope = ContainerInitializer.Value.CreateScope();
		}

		static TestBase()
		{
			ZenithLogger.LoggerFactory = new LoggerFactory();
		}

		protected IServiceProvider ServiceProvider => _serviceScope.ServiceProvider;

		public void Dispose()
		{
			_serviceScope?.Dispose();
		}

		public virtual Task DisposeAsync() => Task.CompletedTask;

		public virtual Task InitializeAsync() => Task.CompletedTask;

		private static IServiceProvider CreateContainer()
		{
			var services = new ServiceCollection();

			// Register controllers.
			var partManager = new ApplicationPartManager();
			partManager.FeatureProviders.Add(new ControllerFeatureProvider());
			partManager.ApplicationParts.Add(new AssemblyPart(typeof(SalonUltimate.MediaGateway.Startup).Assembly));
			var controllersFeature = new ControllerFeature();
			partManager.PopulateFeature(controllersFeature);
			foreach (var controllerType in controllersFeature.Controllers.Select(t => t.AsType()))
			{
				services.TryAddTransient(controllerType);
			}

			Startup.ConfigureServices(services);
			return services.BuildServiceProvider();
		}
	}
}
