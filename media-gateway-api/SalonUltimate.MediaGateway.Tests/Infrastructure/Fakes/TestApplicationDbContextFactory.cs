﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace SalonUltimate.MediaGateway.Tests.Infrastructure.Fakes
{
	/// <summary>
	/// A helper which simplifies test DBContext creation
	/// </summary>
	public class TestApplicationDbContextFactory
	{
		public static Action<DbContextOptionsBuilder> GetTestOptionsBuilderConfigurator(ILoggerFactory loggerFactory = null)
			=> optionsBuilder =>
			{
				optionsBuilder.EnableSensitiveDataLogging();
				// this connection is not shared: https://www.sqlite.org/inmemorydb.html
				var connectionString = new SqliteConnectionStringBuilder { Mode = SqliteOpenMode.Memory }.ConnectionString;
				optionsBuilder.UseSqlite(connectionString);

				if (loggerFactory != null && Debugger.IsAttached)
					optionsBuilder.UseLoggerFactory(loggerFactory);
			};
	}
}
