﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Extensions.DependencyInjection.Extensions;
using System.Threading;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.MediaGateway.Api.Extensions;
using SalonUltimate.MediaGateway.ORM.DI;
using SalonUltimate.MediaGateway.Services.DI;
using SalonUltimate.MediaGateway.Tests.Infrastructure.Fakes;
using SalonUltimate.MediaGateway.ORM;
using SalonUltimate.MediaGateway.Services.Interfaces;
using SalonUltimate.MediaGateway.Services.Storages;

namespace SalonUltimate.MediaGateway.Tests.Infrastructure
{
	public class Startup
	{
		private static Lazy<IConfiguration> _configuration = new Lazy<IConfiguration>(() => new ConfigurationBuilder()
			.AddJsonFile("appsettings.json")
			.AddJsonFile("appsettings.local.json", optional: true)
			.Build(), LazyThreadSafetyMode.ExecutionAndPublication);
		private static IConfiguration Configuration => _configuration.Value;

		public static void ConfigureServices(IServiceCollection services)
		{
			services.TryAddSingleton(Configuration);
			//services.AddMemoryCache();
			//services.AddAutoMapper(typeof(OrmMappingProfile), typeof(DtoMappingProfile));

			services.AddAutoMapper();

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.EnableSensitiveDataLogging();
				options.UsePostgreSql(Configuration.GetConnectionString("PostgreSqlConnectionString"));
			});

			services.RegisterDependencies(new RepositoryDIConfigurator());
			services.RegisterDependencies(new ServicesDIConfiguration());
			
			services.Configure<S3FileServiceOptions>(Configuration.GetSection("AWSS3"));
			services.RegisterDependencies(new StoragesDIConfigurator());

			OverrideDependencies(services);
		}


		private static void OverrideDependencies(IServiceCollection services)
		{
			// TODO: here you can unregister services if you need to replace them with fake ones
			// Example:
			// services.RemoveAll<IDateTimeService>();
			// services.AddScoped<IDateTimeService, TestDateTimeService>();
		}
	}
}
