﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SalonUltimate.BookedBy.Domain.Entities;
using SalonUltimate.Foundation.Controllers;
using SalonUltimate.Foundation.Enums;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.MediaGateway.Dtos;
using SalonUltimate.MediaGateway.Services.Interfaces;

namespace SalonUltimate.MediaGateway.Controllers
{
	/// <summary>
	/// StorageFile Storage CRUD operations
	/// </summary>
	[ApiVersion("1.0")]
	[Route("api/v{version:apiVersion}/[controller]")]
	public class FileController : BaseController
	{
		private readonly IHttpContextAccessor _httpContextAccessor;
		private readonly ISqlFileService _sqlFileService;
		private readonly IS3FileService _s3FileService;

		/// <inheritdoc />
		public FileController(
			IHttpContextAccessor httpContextAccessor,
			IS3FileService s3FileService, 
			ISqlFileService sqlFileService)
		{
			_httpContextAccessor = httpContextAccessor;
			_sqlFileService = sqlFileService;
			_s3FileService = s3FileService;
		}

		/// <summary>
		/// Return file Url
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <returns>Url to file</returns>
		/// <response code="200">OK</response>
		[HttpGet("Url/{id}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(string))]
		public async Task<IActionResult> GetUrl(Guid id)
		{
			return Ok(await Task.Run(() =>
				_s3FileService.BuildFileUrl(id.ToString())));
		}

		/// <summary>
		/// Redirect to file Url
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <returns>Redirect</returns>
		/// <response code="302">Redirect</response>
		[HttpGet("Redirect/{id}")]
		[ProducesResponseType((int)HttpStatusCode.Redirect)]
		[ApiExplorerSettings(IgnoreApi = true)]
		public async Task<IActionResult> GetRedirect(Guid id)
		{
			return Redirect(await Task.Run(() =>
				_s3FileService.BuildFileUrl(id.ToString())));
		}

		/// <summary>
		/// Return full information about file without content from storage
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		[HttpGet("Info/{id}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		public async Task<IActionResult> GetFileDto(Guid id, CancellationToken cancellationToken = default)
		{
			var file = await _sqlFileService.GetFileInfoAsync(id, cancellationToken);

			if (file == null)
				return Ok(new ResponseVM<FileDto>(ResponseTypes.NotFound, $"File not found by id {id}"));

			return Ok(new ResponseVM<FileDto>(file));
		}

		/// <summary>
		/// Return full information about file without content from storage by unique url
		/// </summary>
		/// <param name="url">unique file url, example: https://k8s-mediagw.s3.us-east-1.amazonaws.com/00000000-0000-0000-0000-000000000001 </param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		[HttpGet("Info/ByUrl/{url}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		public async Task<IActionResult> GetFileDtoByUrl(string url, CancellationToken cancellationToken = default)
		{
			string urlUnescaped = Uri.UnescapeDataString(url);

			if (Uri.TryCreate(urlUnescaped, UriKind.Absolute, out Uri urlUri))
			{
				string idString = urlUri.Segments.Last();

				if (Guid.TryParse(idString, out Guid id))
					return Ok(await GetFileDto(id, cancellationToken));

				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, $"Url incorrect, id must be Guid: {idString}"));
			}
			return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, $"Url incorrect, must be Absolute Url: {url}"));
		}

		/// <summary>
		/// Upload file to Storage (create or update if exist).
		/// Use this method if Body 'Content-Type' is 'multipart/form-data'
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="uploadedFile">file posted in form</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		/// <remarks>
		/// BODY: form expected
		/// </remarks>
		[HttpPut("AsForm/{id}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		[RequestSizeLimit(100000000)] // 100 Mb
		public async Task<IActionResult> CreateOrUpdateFileAsForm(Guid id, [FromForm] IFormFile uploadedFile, CancellationToken cancellationToken = default)
		{
			if (uploadedFile == null && (Request.Form.Files.Count == 0))
				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, "FormFile collection is empty"));

			if (uploadedFile == null) 
				uploadedFile = Request.Form.Files?[0];

			var storageFileDto = new FileDto() 
			{
				Id = id,
				ContentType = uploadedFile.ContentType,
				Name = uploadedFile.FileName,
				Length = uploadedFile.Length,
				StorageId = Guid.Empty,
				Description = "",
				Url = _s3FileService.BuildFileUrl(id.ToString())
			};

			// send file to file storage:
			await _s3FileService.UploadAsync(uploadedFile.OpenReadStream(), id.ToString(), cancellationToken);

			// send information about file to sql database:
			await _sqlFileService.UploadAsync(storageFileDto: storageFileDto, id: id, cancellationToken: cancellationToken);

			return Ok(new ResponseVM<FileDto>(storageFileDto));
		}

		/// <summary>
		/// Upload file to Storage (create or update if exist).
		/// Use this method if Body 'Content-Type' is 'application/octet-stream'
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		/// <remarks>
		/// BODY: stream expected
		/// </remarks>
		[HttpPut("AsStream/{id}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		[RequestSizeLimit(100000000)] // 100 Mb
		public async Task<IActionResult> CreateOrUpdateFileAsStream(Guid id, CancellationToken cancellationToken = default)
		{
			Stream fileStream = _httpContextAccessor.HttpContext.Request.Body;

			if (fileStream == null)
				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, "Body stream is empty"));

			string fileName;
			try
			{
				fileName = Path.GetFileName(_httpContextAccessor.HttpContext.Request.Headers.ToList()
					           .Find(h => h.Key == "Content-Disposition").Value.ToString().Split('=')[1]) ?? "EmptyName";
			}
			catch (Exception e)
			{
				fileName = "NameError";
			}

			// read stream to end
			using (var memoryStream = new MemoryStream())
			{
				fileStream.CopyTo(memoryStream);
			}

			var storageFileDto = new FileDto() 
			{
				Id = id,
				ContentType = "application/octet-stream",
				Name = fileName,
				Length = fileStream.Length,
				StorageId = Guid.Empty,
				Description = "",
				Url = _s3FileService.BuildFileUrl(id.ToString())
			};

			// send file to file storage:
			await _s3FileService.UploadAsync(fileStream, id.ToString(), cancellationToken);

			// send information about file to sql database:
			await _sqlFileService.UploadAsync(storageFileDto: storageFileDto, id: id, cancellationToken: cancellationToken);

			return Ok(new ResponseVM<FileDto>(storageFileDto));
		}

		/// <summary>
		/// Upload file to Storage (create new only).
		/// Use this method if Body 'Content-Type' is 'multipart/form-data'
		/// </summary>
		/// <param name="uploadedFile">file posted in form</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		/// <remarks>
		/// BODY: form expected
		/// </remarks>
		[HttpPost("AsForm")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		[RequestSizeLimit(100000000)] // 100 Mb
		public async Task<IActionResult> CreateFileAsForm([FromForm] IFormFile uploadedFile, CancellationToken cancellationToken = default)
		{
			if (uploadedFile == null && (Request.Form.Files.Count == 0))
				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, "StorageFile from Form is empty"));

			if (uploadedFile == null) 
				uploadedFile = Request.Form.Files?[0];

			// sequential GUID for clustered index:
			Guid id = RT.Comb.Provider.PostgreSql.Create();

			var storageFileDto = new FileDto() 
			{
				Id = id,
				ContentType = uploadedFile.ContentType,
				Name = uploadedFile.FileName,
				Length = uploadedFile.Length,
				StorageId = Guid.Empty,
				Description = "",
				Url = _s3FileService.BuildFileUrl(id.ToString())
			};

			// send file to file storage:
			await _s3FileService.UploadAsync(uploadedFile.OpenReadStream(), id.ToString(), cancellationToken);

			// send information about file to sql database:
			await _sqlFileService.UploadAsync(storageFileDto: storageFileDto, id: id, cancellationToken: cancellationToken);

			return Ok(new ResponseVM<FileDto>(storageFileDto));
		}

		/// <summary>
		/// Upload file to Storage (create new only).
		/// Use this method if Body 'Content-Type' is 'application/octet-stream'
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		/// <remarks>
		/// BODY: stream expected
		/// </remarks>
		[HttpPost("AsStream")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		[RequestSizeLimit(100000000)] // 100 Mb
		public async Task<IActionResult> CreateFileAsStream(CancellationToken cancellationToken = default)
		{
			Stream fileStream = _httpContextAccessor.HttpContext.Request.Body;

			if (fileStream == null)
				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, "Body stream is empty"));

			var fileName = Path.GetFileName(_httpContextAccessor.HttpContext.Request.Headers.ToList()
				.Find(h => h.Key == "Content-Disposition").Value.ToString().Split('=')[1]) ?? "";

			// sequential GUID for clustered index:
			Guid id = RT.Comb.Provider.PostgreSql.Create();

			// read stream to end
			using (var memoryStream = new MemoryStream())
			{
				fileStream.CopyTo(memoryStream);
			}

			var storageFileDto = new FileDto() 
			{
				Id = id,
				ContentType = "application/octet-stream",
				Name = fileName,
				Length = fileStream.Length,
				StorageId = Guid.Empty,
				Description = "",
				Url = _s3FileService.BuildFileUrl(id.ToString())
			};

			// send file to file storage:
			await _s3FileService.UploadAsync(fileStream, id.ToString(), cancellationToken);

			// send information about file to sql database:
			await _sqlFileService.UploadAsync(storageFileDto: storageFileDto, id: id, cancellationToken: cancellationToken);

			return Ok(new ResponseVM<FileDto>(storageFileDto));
		}

		/// <summary>
		/// Delete file from Storage by unique id (guid)
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		[HttpDelete("{id}")]
		[ProducesResponseType((int)HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken = default)
		{
			// delete from file storage:
			await _s3FileService.DeleteAsync(id.ToString(), cancellationToken);

			// delete information about file from sql database:
			await _sqlFileService.DeleteAsync(id.ToString(), cancellationToken);
			
			return Ok(new ResponseVM<FileDto>(ResponseTypes.Success, $"File {id} has been deleted"));
		}

		/// <summary>
		/// Delete file from Storage by unique url
		/// </summary>
		/// <param name="url">unique file url, example: https://k8s-mediagw.s3.us-east-1.amazonaws.com/00000000-0000-0000-0000-000000000001 </param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		/// <response code="200">OK</response>
		[HttpDelete("ByUrl/{url}")]
		[ProducesResponseType((int) HttpStatusCode.OK)]
		[Produces(typeof(ResponseVM<FileDto>))]
		public async Task<IActionResult> DeleteByUrl(string url, CancellationToken cancellationToken = default)
		{
			string urlUnescaped = Uri.UnescapeDataString(url);

			if (Uri.TryCreate(urlUnescaped, UriKind.Absolute, out Uri urlUri))
			{
				string idString = urlUri.Segments.Last();

				if (Guid.TryParse(idString, out Guid id))
					return await Delete(id, cancellationToken);

				return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, $"Url incorrect, id must be Guid: {idString}"));
			}
			return Ok(new ResponseVM<FileDto>(ResponseTypes.BadData, $"Url incorrect, must be Absolute Url: {url}"));
		}
	}
}
