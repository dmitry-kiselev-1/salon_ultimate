﻿using System.Diagnostics;
using System.IO;
using AutoMapper;
using Marvin.Cache.Headers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using SalonUltimate.MediaGateway.Middleware;
using SalonUltimate.MediaGateway.ORM;
using SalonUltimate.MediaGateway.ORM.DI;
using SalonUltimate.MediaGateway.Services.DI;
using SalonUltimate.Foundation.DTOs;
using SalonUltimate.Foundation.Extensions.ApplicationBuilder;
using SalonUltimate.Foundation.Extensions.ServiceCollection;
using SalonUltimate.Foundation.Helpers;
using SalonUltimate.Foundation.Middleware.Filters.AuthorizationFilters;
using SalonUltimate.Foundation.Middleware.Filters.ExceptionFilters;
using SalonUltimate.MediaGateway.Api.Extensions;
using SalonUltimate.MediaGateway.Api.Swagger;
using SalonUltimate.MediaGateway.Services;
using SalonUltimate.Foundation.Services.Interfaces;
using SalonUltimate.MediaGateway.Services.Storages;

[assembly: ApiController]
namespace SalonUltimate.MediaGateway
{
	public class Startup
	{
		private readonly ILoggerFactory _loggerFactory;

		private IConfiguration Configuration { get; }

		public Startup(IConfiguration configuration, ILoggerFactory loggerFactory)
		{
			Configuration = configuration;
			_loggerFactory = loggerFactory;
			ZenithLogger.LoggerFactory = loggerFactory;

			// migration applied in deploy/start.sh
			//ApplicationLifetimeExtensions.ApplyMigration(configuration);
		}

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddTransient<IHealthCheckService, HealthCheckService>();

			services.ConfigureAppInsights();

			services.AddCors();
			services.AddMemoryCache();

			services.AddDbContext<ApplicationDbContext>(options =>
			{
				options.EnableSensitiveDataLogging();
				options.UsePostgreSql(Configuration.GetConnectionString("PostgreSqlConnectionString"));

				if (Debugger.IsAttached)
					options.UseLoggerFactory(_loggerFactory);
			});

			services.AddMvcCore()
				.SetCompatibilityVersion(CompatibilityVersion.Latest);
			services.AddVersionedApiExplorer("'v'VVV", true, "VVV");

			services.AddMvc(options =>
			{
				options.Filters.Add<OperationCancelledFilter>();
				options.Filters.Add<EnableRewindAuthorizationFilter>();
				options.AllowEmptyInputInBodyModelBinding = true;
			})
			.AddControllersAsServices();

			services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
			services.ConfigureApiVersioning();

			services.AddAutoMapper();

			services.ConfigureSwaggerGen(new InfoApiVersionDTO
			{
				TitleTemplate = "SalonUltimate.MediaGateway API {0}",
				VersionTemplate = "{0}",
				Description = "SalonUltimate.MediaGateway server",
				ContactName = "Some Guy",
				ContactEmail = "some.guy@somewhere.com",
				TermsOfService = "Shareware",
				LicenseName = "MIT",
				LicenseUrl = "https://opensource.org/licenses/MIT",
				DescriptionPartWhenDepricated = " This API version has been deprecated.",
				XmlCommentsFilePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath,
					$"{PlatformServices.Default.Application.ApplicationName}.xml")
			});

			services.ConfigureSwaggerGen(options =>
			{
				options.OperationFilter<FileUploadOperation>();
			});

			services.AddSuHttpCacheHeaders(new ExpirationModelOptions { NoStore = true });

			services.RegisterDependencies(new RepositoryDIConfigurator());
			services.RegisterDependencies(new ServicesDIConfiguration());
			
			services.Configure<S3FileServiceOptions>(Configuration.GetSection("AWSS3"));
			services.Configure<MediaGatewayClientOptions>(Configuration.GetSection("MediaGatewayApi"));
			services.RegisterDependencies(new StoragesDIConfigurator());
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env,
			ApiVersionDescriptionProviderWrapper provider, AutoMapper.IConfigurationProvider autoMapper)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSuHttpCacheHeaders(); // this must be the first middleware (the outer one), because it closes response stream.
			app.UseGlobalExceptionHandler();

			app.UseSwaggerWithUI(provider);
			
			// Swagger UI need access to file storage:
			app.UseCors(configuration: Configuration, configurationKey: "Cors:Origins");

			app.UseMvc();

			autoMapper.AssertConfigurationIsValid();
		}
	}
}
