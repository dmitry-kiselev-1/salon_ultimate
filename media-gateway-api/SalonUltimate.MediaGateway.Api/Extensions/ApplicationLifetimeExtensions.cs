﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SalonUltimate.MediaGateway.ORM;

namespace SalonUltimate.MediaGateway.Api.Extensions
{
	public static class ApplicationLifetimeExtensions
	{
		public static void RegisterMigrateArgumentHandler(this IApplicationLifetime appLifetime, IConfiguration configuration)
		{
			appLifetime.ApplicationStarted.Register(async () =>
			{
				var migrateArg = configuration["migrate"];

				if (migrateArg == "up")
				{
					ApplicationLifetimeExtensions.ApplyMigration(configuration);
					appLifetime.StopApplication();
				}
				else if (migrateArg != null)
				{
					Console.WriteLine("\t\nInvalid value for 'migrate' argument. Valid values: up.\n");
				}
			});
		}

		internal static void ApplyMigration(IConfiguration configuration)
		{
			var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
			var connectionString = configuration.GetConnectionString("PostgreSqlConnectionString");
			builder.UsePostgreSql(connectionString);
			using (var dbContext = new ApplicationDbContext(builder.Options))
			{
				// Devart driver will create database if not exists (https://forums.devart.com/viewtopic.php?t=34590):
				var config = Devart.Data.PostgreSql.Entity.Configuration.PgSqlEntityProviderConfig.Instance;
				config.DatabaseScript.Schema.DeleteDatabaseBehaviour = Devart.Data.PostgreSql.Entity.Configuration.DeleteDatabaseBehaviour.Database;

				dbContext.Database.Migrate();
			}
		}

	}
}
