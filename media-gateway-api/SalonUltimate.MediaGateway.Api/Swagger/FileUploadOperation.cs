﻿using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace SalonUltimate.MediaGateway.Api.Swagger
{
	/// <summary>
	/// Upload file via Swagger in ASP.NET Core Web API
	/// </summary>
	public class FileUploadOperation : IOperationFilter
	{
		public void Apply(Operation operation, OperationFilterContext context)
		{
			if (operation.OperationId == "CreateOrUpdateFileAsForm")
			{
				var id = operation.Parameters[0];
				operation.Parameters.Clear();
				operation.Parameters.Add(id);
				operation.Parameters.Add(new NonBodyParameter
				{
					Name = "uploadedFile",
					In = "formData",
					Description = "Upload file to Storage (create or update if exist).",
					Required = true,
					Type = "file"
				});
				operation.Consumes.Add("multipart/form-data");
			} else if (operation.OperationId == "CreateFileAsForm")
			{
				var id = operation.Parameters[0];
				operation.Parameters.Clear();
				operation.Parameters.Add(new NonBodyParameter
				{
					Name = "uploadedFile",
					In = "formData",
					Description = "Upload file to Storage (create or update if exist).",
					Required = true,
					Type = "file"
				});
				operation.Consumes.Add("multipart/form-data");
			}

		}
	}
}
