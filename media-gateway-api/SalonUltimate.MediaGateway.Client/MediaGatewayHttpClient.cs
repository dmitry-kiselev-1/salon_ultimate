﻿using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.MediaGateway.Client;
using SalonUltimate.MediaGateway.Dtos;
using SalonUltimate.MediaGateway.Services.Storages;

namespace SalonUltimate.MediaGateway.Client
{
	/// <summary>
	/// <inheritdoc cref="IMediaGatewayHttpClient"/>
	/// </summary>
	public class MediaGatewayHttpClient : IMediaGatewayHttpClient
	{
		private const string GetFileUrlAddress = "/api/v1/File/Url/";
		private const string GetFileInfoAddress = "/api/v1/File/Info/";
		private const string GetFileInfoByUrlAddress = "/api/v1/File/Info/ByUrl/";
		private const string PutCreateOrUpdateFileAsFormAddress = "/api/v1/File/AsForm/";
		private const string PutCreateOrUpdateFileAsStreamAddress = "/api/v1/File/AsStream/";
		private const string PostCreateFileAsFormAddress = "/api/v1/File/AsForm";
		private const string PostCreateFileAsStreamAddress = "/api/v1/File/AsStream";
		private const string DeleteFileAddress = "/api/v1/File/";
		private const string DeleteFileByUrlAddress = "/api/v1/File/ByUrl/";

		private readonly HttpClient _client;
 
		/// <summary>
		/// Constructor for HttpClient and IOptions injection
		/// </summary>
		/// <param name="httpClient">HttpClient expected for HttpClientFactory</param>
		/// <param name="options">IOptions for MediaGatewayApi:BaseAddress in appsettings.json</param>
		public MediaGatewayHttpClient(HttpClient httpClient, IOptions<MediaGatewayClientOptions> options)
		{
			var baseAddress = options.Value.BaseAddress;
			httpClient.BaseAddress = new Uri(baseAddress);
			_client = httpClient;
		}
 
		/// <summary> <inheritdoc/> </summary>
		public async Task<string> GetUrlAsync(Guid id)
		{
			return await _client.GetStringAsync($"{GetFileUrlAddress}{id}");
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> GetFileDtoAsync(Guid id, CancellationToken cancellationToken = default)
		{
			var content = await _client.GetStringAsync($"{GetFileInfoAddress}{id}");
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> GetFileDtoAsync(string url, CancellationToken cancellationToken = default)
		{
			var content = await _client.GetStringAsync($"{GetFileInfoByUrlAddress}{url}");
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> CreateOrUpdateFileAsFormAsync(Guid id, IFormFile uploadedFile, CancellationToken cancellationToken = default)
		{
			var httpContent = new MultipartFormDataContent
			{
				{ new StreamContent(uploadedFile.OpenReadStream()), "uploadedFile", uploadedFile.FileName }
			};

			var httpResponseMessage = await _client.PutAsync($"{PutCreateOrUpdateFileAsFormAddress}{id}", httpContent, cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> CreateOrUpdateFileAsStreamAsync(Guid id, FileDto fileDto, Stream fileStream, CancellationToken cancellationToken = default)
		{
			var httpContent = new StreamContent(fileStream);
			httpContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
				{FileName = fileDto.Name};

			var httpResponseMessage = await _client.PutAsync($"{PutCreateOrUpdateFileAsStreamAddress}{id}", httpContent, cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> CreateFileAsFormAsync(IFormFile uploadedFile, CancellationToken cancellationToken = default)
		{
			var httpContent = new MultipartFormDataContent();
			httpContent.Add(new StreamContent(uploadedFile.OpenReadStream()), "uploadedFile", uploadedFile.FileName);

			var httpResponseMessage = await _client.PostAsync($"{PostCreateFileAsFormAddress}", httpContent, cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> CreateFileAsStreamAsync(FileDto fileDto, Stream fileStream, CancellationToken cancellationToken = default)
		{
			var httpContent = new StreamContent(fileStream);
			httpContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
				{FileName = fileDto.Name};

			var httpResponseMessage = await _client.PostAsync($"{PostCreateFileAsStreamAddress}", httpContent, cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> DeleteAsync(Guid id, CancellationToken cancellationToken = default)
		{
			var httpResponseMessage = await _client.DeleteAsync($"{DeleteFileAddress}{id}", cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}

		/// <summary> <inheritdoc/> </summary>
		public async Task<ResponseVM<FileDto>> DeleteAsync(string uri, CancellationToken cancellationToken = default)
		{
			var httpResponseMessage = await _client.DeleteAsync($"{DeleteFileByUrlAddress}{uri}", cancellationToken);
			var content = await httpResponseMessage.Content.ReadAsStringAsync();
			var storageFileDto = JsonConvert.DeserializeObject<ResponseVM<FileDto>>(content);

			return storageFileDto;
		}
	}
}
