﻿using System;
using System.IO;
using System.Net.Http;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SalonUltimate.Foundation.ViewModels;
using SalonUltimate.MediaGateway.Dtos;

namespace SalonUltimate.MediaGateway.Client
{
	/// <summary>
	/// It's HttpClientFactory pattern for SalonUltimate.MediaGateway.Api
	/// Register implementation as a typed client into Startup -> ConfigureServices
	/// Example:
	/// <![CDATA[ services.AddHttpClient<IMediaGatewayHttpClient, MediaGatewayHttpClient>(); ]]>
	/// and inject IMediaGatewayHttpClient to some controllers or services there you need it.
	/// Please, note:
	/// MediaGatewayApi:BaseAddress (like "https://media.dev.sg.salondev.net") in appsettings.json expected
	/// </summary>
	public interface IMediaGatewayHttpClient
	{
		/// <summary>
		/// Return file Url
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <returns>Url to file</returns>
		Task<string> GetUrlAsync(Guid id);

		/// <summary>
		/// Return full information about file without content from storage
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> GetFileDtoAsync(Guid id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Return full information about file without content from storage by unique url
		/// </summary>
		/// <param name="url">unique file url, example: https://k8s-mediagw.s3.us-east-1.amazonaws.com/00000000-0000-0000-0000-000000000001 </param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> GetFileDtoAsync(string url, CancellationToken cancellationToken = default);

		/// <summary>
		/// Upload file to Storage (create or update if exist).
		/// Use this method if Body 'Content-Type' is 'multipart/form-data'
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="uploadedFile">file posted in form</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> CreateOrUpdateFileAsFormAsync(Guid id, IFormFile uploadedFile, CancellationToken cancellationToken = default);

		/// <summary>
		/// Upload file to Storage (create or update if exist).
		/// Use this method if Body 'Content-Type' is 'application/octet-stream'
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> CreateOrUpdateFileAsStreamAsync(Guid id, FileDto fileDto, Stream fileStream, CancellationToken cancellationToken = default);

		/// <summary>
		/// Upload file to Storage (create new only).
		/// Use this method if Body 'Content-Type' is 'multipart/form-data'
		/// </summary>
		/// <param name="uploadedFile">file posted in form</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> CreateFileAsFormAsync(IFormFile uploadedFile, CancellationToken cancellationToken = default);

		/// <summary>
		/// Upload file to Storage (create new only).
		/// Use this method if Body 'Content-Type' is 'application/octet-stream'
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> CreateFileAsStreamAsync(FileDto fileDto, Stream fileStream, CancellationToken cancellationToken = default);

		/// <summary>
		/// Delete file from Storage by unique id (guid)
		/// </summary>
		/// <param name="id">unique file guid, example: {00000000-0000-0000-0000-000000000001}</param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>> DeleteAsync(Guid id, CancellationToken cancellationToken = default);

		/// <summary>
		/// Delete file from Storage by unique url
		/// </summary>
		/// <param name="url">unique file url, example: https://k8s-mediagw.s3.us-east-1.amazonaws.com/00000000-0000-0000-0000-000000000001 </param>
		/// <param name="cancellationToken"></param>
		/// <returns><![CDATA[ ResponseVM<FileDto> ]]></returns>
		Task<ResponseVM<FileDto>>  DeleteAsync(string uri, CancellationToken cancellationToken = default);
	}
}
